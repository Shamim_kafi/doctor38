<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Doctor\Doctor;
use App\DoctorType\DoctorType;
use App\Message\Message;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    Utility::redirect("../index.php");
}

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

$objDoctor = new Doctor();
$allDoctors = $objDoctor->allDoctorInfoWithDesignation();

$patientLists ='';
if (isset($_POST['search_appointment']))
{
    //Utility::dd($_POST);
    $doctorId = $_POST['doctor_id'];
    $chamberId = $_POST['chamber_location'];
    $date = $_POST['input_date'];
    $patientLists = $objDoctor->allSingleDoctorPatientList($doctorId,$chamberId,$date);

    //Utility::dd($patientLists);
}



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Appointment List</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />

    <link href='http://eoffice.codeslab.net/assets/plugin/select2/select2.min.css' rel='stylesheet' media='screen'>

    <!--Select2 use-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../dashboard.php"><b>DOCTOR38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Admin Name: <a href="#" class="btn btn-info"><b>DOCTOR38</b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../dashboard.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li>
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Doctor</b><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="../doctor_type/doctor_type.php"><b><i class="fa fa-sign-out"></i> Doctor Type</b></a>
                            <a href="../doctor/doctor.php"><b><i class="fa fa-sign-out"></i> Doctors</b></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="../user/user_info.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;User</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-history"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-edit"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../profile/admin_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-history"></i>Doctor Appointment</b></h2>
                </div>

                <!--for datatable-->
                <div class="col-md-12">
                    <form action="" method="post" autocomplete="off">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Date Select</div>
                                    <input type="text" id="datepicker1" name="input_date" class="form-control" placeholder="Select Date" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Doctor</div>
                                <select class="select2 form-control" id="doctor_id" name="doctor_id" required>
                                    <option value="0" selected disabled>Select Doctor</option>
                                    <?php
                                    foreach ($allDoctors as $doctor){
                                        echo "
                                            <option value='$doctor->doctor_info_id'>$doctor->doctor_info</option>
                                    ";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Location</div>
                                <select class="select2 form-control" id="location" name="chamber_location" required>
                                    <option value="0" selected disabled>Select Location</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <input type="submit" name="search_appointment" class="btn btn-primary" value="Search">
                        </div>
                    </form>
                </div>

                <!--for excel-->
                <div class="col-md-12">
                    <form action="appoint_excel.php" method="post" autocomplete="off">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Date Select</div>
                                    <input type="text" id="datepicker_excel" name="input_date" class="form-control" placeholder="Select Date" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Doctor</div>
                                <select class="select2 form-control" id="doctor_id_excel" name="doctor_id" required>
                                    <option value="0" selected disabled>Select Doctor</option>
                                    <?php
                                    foreach ($allDoctors as $doctor){
                                        echo "
                                            <option value='$doctor->doctor_info_id'>$doctor->doctor_info</option>
                                    ";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Location</div>
                                <select class="select2 form-control" id="location_excel" name="chamber_location" required>
                                    <option value="0" selected disabled>Select Location</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <input type="submit" name="search_appointment" class="btn btn-warning" value="Excel">
                        </div>
                    </form>
                </div>


                <!--for email-->
                <div class="col-md-12">
                    <form action="send_mail_doctor_appoint_list.php" method="post" autocomplete="off">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Date Select</div>
                                    <input type="text" id="datepicker_email" name="input_date" class="form-control" placeholder="Select Date" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Doctor</div>
                                <select class="select2 form-control" id="doctor_id_email" name="doctor_id" required>
                                    <option value="0" selected disabled>Select Doctor</option>
                                    <?php
                                    foreach ($allDoctors as $doctor){
                                        echo "
                                            <option value='$doctor->doctor_info_id'>$doctor->doctor_info</option>
                                    ";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-addon">Location</div>
                                <select class="select2 form-control" id="location_email" name="chamber_location" required>
                                    <option value="0" selected disabled>Select Location</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <input type="submit" name="search_appointment" class="btn btn-info" value="Email">
                        </div>
                    </form>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr/>

            <?php if (!empty($patientLists)){?>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Advanced Tables -->
                        <!--<div>
                            <a href="appoint_excel.php?date=" class="btn btn-primary"><span class="fa fa-download"></span> Excel</a>
                            <a href="" class="btn btn-warning"><span class="fa fa-envelope"></span> Email</a>
                        </div>
                        <br>-->
                        <div class="panel panel-default btn-default" style="font-family: monospace;">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-condensed table-hover text-center" id="dataTables">
                                        <thead>
                                        <tr class="btn-info text-center">
                                            <td><b>INVOICE ID#</b></td>
                                            <td><b>SERIAL NUMBER</b></td>
                                            <td><b>USER</b></td>
                                            <td><b>PAYMENT</b></td>
                                            <td><b>PAYMENT STATUS</b></td>
                                            <td><b>APPOINT STATUS</b></td>
                                            <td><b>ACTION</b></td>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                         foreach ($patientLists as $patientList){
                                             if ($patientList->appoint_payment_method_id_ref == 1){
                                                 $paymentMethod = 'Cash';
                                             }else{
                                                 $paymentMethod = 'bKash';
                                             }

                                             if ($patientList->appoint_isDeleted == 1){
                                                 $appointStatus = '<b><span class=\'label label-danger\'>Cancel</span></b>';;
                                             }

                                             else if ($patientList->appoint_isDeleted == 0 && $patientList->appoint_isverify == 1){
                                                 $appointStatus = '<b><span class=\'label label-success\'>Complete</span></b>';
                                             }

                                             else if ($patientList->appoint_isDeleted == 0 && $patientList->appoint_isverify == 0){
                                                 $appointStatus = '<b><span class=\'label label-warning\'>Pending</span></b>';
                                             }

                                             echo "
                                                <tr>

                                                    <td><b>$patientList->appoint_invoice_id</b></td>
                                                    <td><b>$patientList->appoint_serial_num </b></td>
                                                    <td><b>Name : $patientList->user_name <br>Phone : $patientList->user_phone <br>Blood : $patientList->user_blood_group</b></td>
                                                    <td><b>$paymentMethod <br> $patientList->appoint_transaction_id</b></td>
                                                    ";
                                             if ($patientList->appoint_isverify == 1){
                                                 echo "
                                                    <td><b><span class='label label-info'>Verified</span></b></td>
                                                 ";
                                             } else{
                                                 echo "
                                                    <td><b><span class='label label-danger'>Unverified</span></b></td>
                                                 ";
                                             }

                                             echo "
                                   
                                                    <td>$appointStatus</td>
                                                    <td>
                                                     
                                                       <b><a class='btn btn-danger btn-sm' href='cancel_appointment.php?appoint_id=$patientList->appoint_id'><span class='fa fa-remove'></span> Delete</a></b>
                                                    </td>
                                                </tr>
                                             ";
                                         }
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr class="btn-info text-center">

                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <h2 class="text-center"><b>Total Patient : <?php echo count($patientLists)?></b></h2>
                            </div>
                        </div>
                        <!--End Advanced Tables -->
                    </div>
                </div>

                <!-- /. ROW  -->
                <hr />
                <div class="row">
                    <div class="col-md-12">
                        <span class="btn pull-left"><b>Developed by @ Doctor38 Ltd.</b></span>
                        <span class="btn pull-right"><b>Copyright @ 2018</b></span>
                    </div>
                </div>
            <?php }?>



        </div>
    </div>
    <!-- /. PAGE INNER  -->
</div>

<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../../../resources/js/admin/jquery.dataTables.js"></script>
<script src="../../../resources/js/admin/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables').dataTable();
    });
</script>


<!--Select2 use-->
<script src='http://eoffice.codeslab.net/assets/plugin/select2/select2.full.min.js'></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<!--Select2 use end-->
<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>

<!-- Calendar -->
<link rel="stylesheet" href="../../../resources/dateinput/jquery-ui.css" />
<script src="../../../resources/dateinput/jquery-ui.js"></script>
<script>
    $(document).ready(function () {

        $("#datepicker1").datepicker({

            showAnim :'drop',
            numberOfMonths : 2,
            dateFormat : 'yy-mm-dd'
        });

        $("#datepicker_excel").datepicker({

            showAnim :'drop',
            numberOfMonths : 2,
            dateFormat : 'yy-mm-dd'
        });

        $("#datepicker_email").datepicker({

            showAnim :'drop',
            numberOfMonths : 2,
            dateFormat : 'yy-mm-dd'
        });

    });
</script>
<!-- //Calendar -->


<!--Load-->
<script>
    $(document).ready(function(){
        $('#doctor_id').change(function(){
            var doctor_id = $(this).val();

            $.ajax({
                url:"load_doctor_chamber_location.php",
                method:"POST",
                data:{doctor_id:doctor_id},
                success:function(data){
                    $('#location').html(data);
                }
            });
        });

        $('#doctor_id_excel').change(function(){
            var doctor_id = $(this).val();

            $.ajax({
                url:"load_doctor_chamber_location.php",
                method:"POST",
                data:{doctor_id:doctor_id},
                success:function(data){
                    $('#location_excel').html(data);
                }
            });
        });

        $('#doctor_id_email').change(function(){
            var doctor_id = $(this).val();

            $.ajax({
                url:"load_doctor_chamber_location.php",
                method:"POST",
                data:{doctor_id:doctor_id},
                success:function(data){
                    $('#location_email').html(data);
                }
            });
        });
    });
</script>

</body>
</html>

