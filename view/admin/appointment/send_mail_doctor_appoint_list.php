<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Doctor\Doctor;
use App\Message\Message;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

//for get doctor type list
$objDoctor = new Doctor();
$doctorId = $_POST['doctor_id'];
$chamberId = $_POST['chamber_location'];
$date = $_POST['input_date'];

$singleDoctorDetails = $objDoctor->singleDoctorDetails($doctorId);

$patientLists = $objDoctor->allSingleDoctorPatientList($doctorId,$chamberId,$date);



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | <?php echo $singleDoctorDetails->doctor_info_name ?> Appointment List</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />

    <link href='http://eoffice.codeslab.net/assets/plugin/select2/select2.min.css' rel='stylesheet' media='screen'>

    <!--Select2 use-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>tinymce.init({
            selector: 'textarea',  // change this value according to your HTML

            menu: {
                table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
                tools: {title: 'Tools', items: 'spellchecker code'}

            }
        });
    </script>

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../dashboard.php"><b>DOCTOR38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Admin Name: <a href="#" class="btn btn-info"><b>DOCTOR38</b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../dashboard.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li>
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Doctor</b><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="../doctor_type/doctor_type.php"><b><i class="fa fa-sign-out"></i> Doctor Type</b></a>
                            <a href="../doctor/doctor.php"><b><i class="fa fa-sign-out"></i> Doctors</b></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="../user/user_info.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;User</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-history"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-edit"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../profile/admin_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-send"></i> Send Email</b></h2>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default btn-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form  role="form" method="post" action="mail_send.php">
                                        <div class="form-group">
                                            <label for="Name">Name:</label>
                                            <input type="text"  name="name"  class="form-control" value="<?php echo $singleDoctorDetails->doctor_info_name ?>"  id="name" >
                                            <label for="Email">Email Address:</label>
                                            <input type="text"  name="doctor_email"  class="form-control" value="<?php echo $singleDoctorDetails->doctor_info_email?>" id="email" >

                                            <label for="Subject">Subject:</label>
                                            <input type="text"  name="email_subject"  class="form-control" id="subject" value="Patient Serial List">

                                            <label for="body">Body:</label>
                                            <textarea  rows="14" cols="160"  name="msg_body" >
                                                <span>Assalamu Alaikum Dr. <?php echo $singleDoctorDetails->doctor_info_name ?>,
                                                </span>
                                                <table class="table table-striped table-bordered table-hover text-center">
                                                <thead>
                                                     <tr class="btn-danger">
                                                         <td><b>INVOICE ID#</b></td>
                                                         <td><b>SERIAL NUMBER</b></td>
                                                         <td><b>USER</b></td>
                                                         <td><b>PAYMENT</b></td>
                                                         <td><b>STATUS</b></td>
                                                     </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($patientLists as $patientList){
                                                        if ($patientList->appoint_payment_method_id_ref == 1){
                                                            $paymentMethod = 'Cash';
                                                        }else{
                                                            $paymentMethod = 'bKash';
                                                        }

                                                        echo "
                                                        <tr>
                                                            <td><b>$patientList->appoint_invoice_id</b></td>
                                                            <td><b>$patientList->appoint_serial_num </b></td>
                                                            <td><b>Name : $patientList->user_name <br>Phone : $patientList->user_phone <br>Blood : $patientList->user_blood_group</b></td>
                                                            <td><b>$paymentMethod <br> $patientList->appoint_transaction_id</b></td>
                                                        ";
                                                        if ($patientList->appoint_isverify == 1){
                                                            echo "
                                                            <td><b><span class='text-info'>Verified</span></b></td>
                                                        ";
                                                        }
                                                        else{
                                                            echo "
                                                            <td><b><span class='text-danger'>Unverified</span></b></td>
                                                        ";}
                                                        echo "
                                                        </tr>
                                                        ";
                                                    }
                                                    ?>
                                                </tbody>

                                                <tfoot>
                                                <tr>
                                                    <td colspan="11">
                                                        <h3><b class="text-primary">Total Patient : <?php echo count($patientLists)?></b></h3>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                            </textarea>
                                        </div>
                                        <input class="btn-lg btn-primary" type="submit" value="Send Email">

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /. PAGE INNER  -->
</div>

<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>

<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>


</body>
</html>

