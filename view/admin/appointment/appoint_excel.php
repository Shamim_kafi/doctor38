<?php
require_once ('../../../vendor/autoload.php');

use App\Utility\Utility;
use App\Doctor\Doctor;
use App\Message\Message;

/*if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    Utility::redirect("../index.php");
}*/

//for get doctor type list
$objDoctor = new Doctor();
$doctorId = $_POST['doctor_id'];
$chamberId = $_POST['chamber_location'];
$date = $_POST['input_date'];
$patientLists = $objDoctor->allSingleDoctorPatientList($doctorId,$chamberId,$date);

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');

/** Include PHPExcel */


require_once('../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Doctor38")
    ->setLastModifiedBy("Doctor38")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'INVOICE ID')
    ->setCellValue('B1', 'SL NUMBER')
    ->setCellValue('C1', 'USER NAME')
    ->setCellValue('D1', 'USER PHONE')
    ->setCellValue('E1', 'USER BLOOD GROUP')
    ->setCellValue('F1', 'TRANSACTION ID');

$sl=0;
$counter=1;
foreach($patientLists as $patientList) {
    $sl++;
    $counter++;
// Miscellaneous glyphs, UTF-8
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $counter, $patientList->appoint_invoice_id)
        ->setCellValue('B' . $counter, $patientList->appoint_serial_num)
        ->setCellValue('C' . $counter, $patientList->user_name)
        ->setCellValue('D' . $counter, $patientList->user_phone)
        ->setCellValue('E' . $counter, $patientList->user_blood_group)
        ->setCellValue('F' . $counter, $patientList->appoint_transaction_id);

// Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('PATIENT INFORMATION');

}
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a clientâ€™s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="patient_list.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;