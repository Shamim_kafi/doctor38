<?php
session_start();
include_once('../../../vendor/autoload.php');

use App\Message\Message;
use App\Utility\Utility;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

require '../../../vendor/phpmailer/phpmailer/src/Exception.php';
require '../../../vendor/phpmailer/PHPMailer/src/PHPMailer.php';
require '../../../vendor/phpmailer/PHPMailer/src/SMTP.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication

    $mail->Username = 'pro.event38@gmail.com';                 // SMTP username
    $mail->Password = 'projectevent38';
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom($_POST['doctor_email']);
    $mail->addAddress($_POST['doctor_email']);     // Add a recipient

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $_POST['email_subject'];
    $mail->Body    = $_POST['msg_body'];

    $mail->send();
    //echo 'Message has been sent';
    Utility::redirect('appointment_list.php');
}

catch (Exception $e) {
    //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}




