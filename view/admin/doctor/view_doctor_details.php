<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;
use App\DoctorChamber\DoctorChamber;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

$objDoctor = new Doctor;
$singleDoctorInfo = $objDoctor->singleDoctorInfo($_GET['doctor_info_id']);

//Utility::dd($singleDoctorInfo);

//for chamber
$objDoctorChamber = new DoctorChamber();
$allSingleDoctorChamberInfo = $objDoctorChamber->allSingleDoctorChamberInfo($_GET['doctor_info_id']);


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Single Doctor Details</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <link href="../../../resources/css/admin/button_style.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="../../../resources/plugin/timepicker/jquery.timepicker.min.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../dashboard.php"><b>DOCTOR38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b><a href="#" class="btn btn-info"><b>Dr. <?php echo $singleDoctorInfo->doctor_info_name ?></b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../dashboard.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li>
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Doctor</b><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="../doctor_type/doctor_type.php"><b><i class="fa fa-sign-out"></i> Doctor Type</b></a>
                            <a href="../doctor/doctor.php"><b><i class="fa fa-sign-out"></i> Doctors</b></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="../user/user_info.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;User</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-history"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-edit"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../profile/admin_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">

            <div class="row">
               <div class="col-md-12">
                   <!--  Message insert-->
                   <?php

                   $msg =Message::message();
                   if ($msg){
                       echo "
                           $msg
                           ";
                   }
                   ?>
                   <!-- Message insert end-->
               </div>
            </div>
            <!-- /. ROW  -->
            <div class="row" >
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-default btn-default">
                        <div class="panel-body">
                            <h1 class="text-center text-primary"><b>Dr. <?php echo $singleDoctorInfo->doctor_info_name ?> <br><?php echo $singleDoctorInfo->doctor_info_degree ?> </b></h1>

                            <?php
                            if ($singleDoctorInfo->doctor_info_status==1 && $singleDoctorInfo->doctor_info_admin ==1){

                                echo "
                                
                                    <div class=\"row\">
                                        <div class=\"col-md-6 col-sm-12\">
                                            <a href=\"\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModalDoctorChamber\">Add Chamber</a>
                                        </div>
                                    </div>
        
                                    <br>
                                
                                ";
                            }
                            ?>

                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-condensed table-striped">
                                        <thead>
                                        <tr class="btn-info">
                                            <th colspan="4">
                                                <h2 style="color: whitesmoke;"><i class="fa fa-user"></i> <b>Dr. <?php echo $singleDoctorInfo->doctor_info_name ?> Info</b>
                                                    <span class="pull-right">
                                                        <span class="badge"><b class="text-success">Reg No. : <?php echo $singleDoctorInfo->doctor_info_reg_id ?></b></span>
                                                    </span>
                                                </h2>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <table class="table table-bordered table-condensed table-striped table-hover">
                                                    <tbody>
                                                    <tr>
                                                        <td class="text-right"><b>Doctor Name :</b></td>
                                                        <td><b>Dr. <?php echo $singleDoctorInfo->doctor_info_name ?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><b>Contact Number :</b></td>
                                                        <td><b><?php echo $singleDoctorInfo->doctor_info_phone ?></b></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="text-right"><b>Doctor Email :</b></td>
                                                        <td><b><?php echo $singleDoctorInfo->doctor_info_email ?></b></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="text-right"><b>Specialist In :</b></td>
                                                        <td><b><?php echo $singleDoctorInfo->doctor_type_name ?></b></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <table class="table table-condensed table-striped table-hover">
                                                    <tbody>
                                                    <tr>
                                                        <td class="text-right"><b>Reg Date Time :</b></td>
                                                        <td><b><?php echo $singleDoctorInfo->doctor_info_reg_datetime ?></b></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="text-right"><b>Doctor Degree :</b></td>
                                                        <td><b><?php echo $singleDoctorInfo->doctor_info_degree ?></b></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><b>Doctor Details :</b></td>
                                                        <td>
                                                            <?php echo substr("$singleDoctorInfo->doctor_info_details",0,50)." ...." ?>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover text-center">
                                    <thead>
                                    <tr class="btn-info">
                                        <td><b>SL#</b></td>
                                        <td><b>CHAMBER LOCATION</b></td>
                                        <td><b>CHAMBER ADDRESS</b></td>
                                        <td><b>DAY & VISITING TIME</b></td>
                                        <td><b>CONSULTANCY FEE</b></td>
                                        <td><b>SERIAL CONTACT</b></td>
                                        <td><b>ACTION</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $sl = 1;
                                    foreach ($allSingleDoctorChamberInfo as $singleDoctorChamberInfo){
                                        echo "
                                    <tr>

                                        <td><b>$sl</b></td>
                                        <td><b>$singleDoctorChamberInfo->doctor_chamber_loc_title</b></td>
                                        <td><b>$singleDoctorChamberInfo->doctor_chamber_loc_details</b></td>
                                        <td><b>$singleDoctorChamberInfo->doctor_chamber_visiting_day <br>
                                            $singleDoctorChamberInfo->doctor_chamber_visiting_time_start -- $singleDoctorChamberInfo->doctor_chamber_visiting_time_end</b>
                                        </td>                                
                                        <td><b>$singleDoctorChamberInfo->doctor_chamber_visiting_fee/= <br>
                                            Daily Patient View : $singleDoctorChamberInfo->doctor_chamber_total_patient</b>
                                        </td>
                                        <td><b>$singleDoctorChamberInfo->doctor_chamber_contact_serial_num</b></td>
                                        ";
                                            if ($singleDoctorChamberInfo->doctor_chamber_verify == 0){
                                                echo "
                                                <td>
                                       
                                                       <a href='doctor_chamber_accept.php?doctor_chamber_id=$singleDoctorChamberInfo->doctor_chamber_id&chamber_phone=$singleDoctorChamberInfo->doctor_chamber_contact_serial_num' class='btn btn-success'>Accept</a>
                                                       <a href='doctor_chamber_edit.php?doctor_chamber_id=$singleDoctorChamberInfo->doctor_chamber_id' class='btn btn-primary'>Edit</a>
                                                       <a href='doctor_chamber_delete.php?doctor_chamber_id=$singleDoctorChamberInfo->doctor_chamber_id' class='btn btn-danger'>Delete</a>
                               
                                                    </td>
                                                </tr>   
                                                ";
                                            }else{
                                                echo "
                                                <td>
                                                  
                                                       <a href='doctor_chamber_edit.php?doctor_chamber_id=$singleDoctorChamberInfo->doctor_chamber_id' class='btn btn-primary'>Edit</a>
                                                       <a href='doctor_chamber_delete.php?doctor_chamber_id=$singleDoctorChamberInfo->doctor_chamber_id' class='btn btn-danger'>Delete</a>
                               
                                                    </td>
                                                </tr>   
                                                ";

                                            }
                                        $sl++;
                                    }

                                    ?>

                                    <tr>
                                        <td colspan="11" class="text-right">
                                            <a href="doctor.php" class="btn btn-danger pull-left"><b><i class="fa fa-reply-all"></i> Back</b></a>

                                        </td>
                                    </tr>
                                    </tbody>

                                    <tfoot>

                                    <tr class="btn-info">
                                        <td colspan="7"></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <span class="btn pull-left"><b>Developed by @ Doctor38 Ltd.</b></span>
                    <span class="btn pull-right"><b>Copyright @ 2018</b></span>
                </div>
            </div>
        </div>
    </div>
    <!-- /. PAGE INNER  -->

    <!-- Modal add chamber -->
    <div class="modal fade" id="myModalDoctorChamber" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Chamber</h4>
                </div>

                <form id="form" action="doctor_chamber_store.php" method="post" autocomplete="off" >
                    <div class="modal-body">

                        <table class="table table-bordered table-condensed text-left">
                            <thead>
                                <tr class="btn-info text-center">
                                    <td colspan="2"><h3><b>Enter Right Information Below</b></h3></td>
                                </tr>
                            </thead>
                            <tbody>
                            <input type="hidden" name="doctor_ref_id" value="<?php echo $_GET['doctor_info_id']?>">
                            <tr>
                                <td class="text-right text-info"><h5><b>Chamber Address Title :</b></h5></td>
                                <td><b>
                                        <select class="form-control select2" style="width: 100%" name="location_head_name" required>
                                            <option selected >Please Select Head Name...</option>
                                            <option value="GEC">GEC</option>
                                            <option value="Agrabad">Agrabad</option>
                                            <option value="Chawkbazar">Chawkbazar</option>
                                            <option value="Provortok">Provortok</option>
                                            <option value="2 Number Gate">2 Number Gate</option>
                                            <option value="Muradpur">Muradpur</option>
                                            <option value="Andorkilla">Andorkilla</option>
                                            <option value="Medical Gate">Medical Gate</option>
                                            <option value="Kumira">Kumira</option>
                                        </select>
                                    </b>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Chamber Location Lat :</b></h5></td>
                                <td><input class="form-control" type="text" name="location_lat"  required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Chamber Location Long :</b></h5></td>
                                <td><input class="form-control" type="text" name="location_long" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Chamber Address Details:</b></h5></td>
                                <td>
                                    <textarea class="form-control" name="chamber_address" id="" cols="30" rows="5" placeholder="Address..."></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Day :</b></h5></td>
                                <td><b>
                                        <select class="form-control select2" style="width: 100%" name="visiting_day" required>
                                            <option selected >Please Select Day...</option>
                                            <option value="Saturday">Saturday</option>
                                            <option value="Sunday">Sunday</option>
                                            <option value="Monday">Monday</option>
                                            <option value="Thursday">Thursday</option>
                                            <option value="Wednesday">Wednesday</option>
                                            <option value="Tuesday">Tuesday</option>
                                            <option value="Friday">Friday</option>

                                        </select>
                                    </b>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Visiting Time Start:</b></h5></td>
                                <td><input class="form-control" type="text" name="visiting_time_start" id="startTime" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Visiting Time End:</b></h5></td>
                                <td><input class="form-control" type="text" name="visiting_time_end" id="endTime"  required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Consultancy Fee :</b></h5></td>
                                <td><input class="form-control" type="number" name="consultancy_fee" placeholder="Enter Consultancy Fee" min="50" max="1000000" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Total Patient View Daily :</b></h5></td>
                                <td><input class="form-control" type="number" name="total_patient_view_daily" placeholder="Enter amount of daily patient view" min="10" max="300" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Serial Number Contact :</b></h5></td>
                                <td><input class="form-control" type="text" name="serial_number_contact" placeholder="Enter Serial Number Contact" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Is Active :</b></h5></td>
                                <td><b>
                                        <select class="form-control" style="width: 100%" name="is_active" required>
                                            <option value="1" >Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </b>
                                </td>
                            </tr>

                            </tbody>
                        </table>

                        <!-- End Form Elements -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit"  value="Save Chamber" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../../../resources/js/admin/jquery.dataTables.js"></script>
<script src="../../../resources/js/admin/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
    });
</script>
<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>
<!-- bootstrap time picker -->
<script src="../../../resources/plugin/timepicker/jquery.timepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#startTime').timepicker();
    });

    $(document).ready(function(){
        $('#endTime').timepicker();
    });
</script>

<!--For message-->
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
</script>

</body>
</html>

