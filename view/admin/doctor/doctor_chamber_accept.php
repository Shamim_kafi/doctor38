<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\DoctorChamber\DoctorChamber;
use App\Message\Message;

$doctorPhone = $_GET['chamber_phone'];

$objChamber = new DoctorChamber();
$objChamber->chamberAccept($_GET['doctor_chamber_id']);

/*Mobile msg*/
$token = "289ded62c68135bfe5c7ca1c0fbe7f57";

$message = "Dear Sir/Mam- Congratulation.Now Chamber has been accepted";

$url = "http://sms.greenweb.com.bd/api.php";


$data= array(
    'to'=>"$doctorPhone",
    'message'=>"$message",
    'token'=>"$token"
); // Add parameters in key value
$ch = curl_init(); // Initialize cURL
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$smsresult = curl_exec($ch);
/*Mobile msg end*/

return Utility::redirect($_SERVER['HTTP_REFERER']);