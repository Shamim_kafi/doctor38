<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\DoctorChamber\DoctorChamber;
use App\Message\Message;

//Utility::dd($_POST);

$objDoctorChamber = new DoctorChamber();
$objDoctorChamber->deactivateChamber($_GET['doctor_chamber_id']);

return Utility::redirect($_SERVER['HTTP_REFERER']);