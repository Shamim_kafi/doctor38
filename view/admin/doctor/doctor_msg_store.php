<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\MessageBox\MessageBox;
use App\Message\Message;


$objMessageBox = new MessageBox();

//Utility::dd($_POST);

$objMessageBox->setMsgData($_POST);
$objMessageBox->storeMsg();
Utility::redirect("doctor.php");

