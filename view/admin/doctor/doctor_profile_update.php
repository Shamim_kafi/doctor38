<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;

$objDoctorInfo = new Doctor();
$objDoctorInfo->setDoctorData($_POST);
$objDoctorInfo->singleDoctorInfoUpdate();

return Utility::redirect($_SERVER['HTTP_REFERER']);