<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;
use App\DoctorType\DoctorType;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

$objDoctorInfo = new Doctor();
$doctorDetails = $objDoctorInfo->singleDoctorDetails($_GET['doctor_id']);


?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Dashboard</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="../../../resources/css/admin/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    <!--Select2 use-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../dashboard.php"><b>DOCTOR38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Admin Name: <a href="#" class="btn btn-info"><b>DOCTOR38</b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>

    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../dashboard.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li>
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Doctor</b><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="../doctor_type/doctor_type.php"><b><i class="fa fa-sign-out"></i> Doctor Type</b></a>
                            <a href="../doctor/doctor.php"><b><i class="fa fa-sign-out"></i> Doctors</b></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="../user/user_info.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;User</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-history"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-edit"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../profile/admin_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <!--  Message insert-->
                <?php

                $msg =Message::message();
                if ($msg){
                    echo "
                           $msg
                           ";
                }
                ?>
                <!-- Message insert end-->

                <div class="col-md-12">
                    <form action="doctor_profile_update.php" method="post">

                        <input type="hidden" name="doctor_id" value="<?php echo $doctorDetails->doctor_info_id?>">

                        <table class="table table-bordered table-condensed text-left">
                            <thead>
                            <tr class="btn-info text-center">
                                <td colspan="2"><h3><b>Dr. <?php echo $doctorDetails->doctor_info_name?> Information</b></h3></td>
                            </tr>

                            </thead>
                            <tbody>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Unique Id :</b></h5></td>
                                <td><input class="form-control" type="text" name="doctor_unique_id" value="<?php echo $doctorDetails->doctor_info_unique_id?>" readonly disabled/></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Reg Number :</b></h5></td>
                                <td><input class="form-control" type="text" name="doctor_reg_id" value="<?php echo $doctorDetails->doctor_info_reg_id?>"/></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Name :</b></h5></td>
                                <td><input class="form-control" type="text" name="doctor_name" value="<?php echo $doctorDetails->doctor_info_name?>"  /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Password :</b></h5></td>
                                <td><input class="form-control" type="password" name="doctor_pass" placeholder="Enter Password" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Email :</b></h5></td>
                                <td><input class="form-control" type="email" name="doctor_email" value="<?php echo $doctorDetails->doctor_info_email?>" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Specialities:</b></h5></td>
                                <td><b>
                                        <select class="form-control select2" style="width: 100%" name="doctor_type_ref" required>
                                            <option selected >Please Select Specialities...</option>
                                            <?php
                                            foreach ($allDoctorType as $doctorType){ ?>

                                                    <option value='<?php echo $doctorType->doctor_type_id?>' <?php if ($doctorDetails->doctor_info_type_ref == $doctorType->doctor_type_id) echo "selected "?>><?php echo $doctorType->doctor_type_name?></option>

                                            <?php }
                                             ?>

                                        </select>
                                    </b>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Degree :</b></h5></td>
                                <td>
                                    <textarea class="form-control" name="doctor_degree" id="" cols="30" rows="2" ><?php echo $doctorDetails->doctor_info_degree?></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Details :</b></h5></td>
                                <td>
                                    <textarea class="form-control" name="doctor_details" id="" cols="30" rows="5" ><?php echo $doctorDetails->doctor_info_details?></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Doctor Mobile :</b></h5></td>
                                <td><input class="form-control" type="text" name="doctor_phone" value="<?php echo $doctorDetails->doctor_info_phone?>" max="13" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Presentation Link :</b></h5></td>
                                <td>
                                    <textarea class="form-control" name="doctor_presentation_link" id="" cols="30" rows="2"><?php echo $doctorDetails->doctor_info_presentation_link?></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Active Status:</b></h5></td>
                                <td>
                                    <select class="form-control select2"  name="doctor_status" required>
                                        <option value="1" <?php if ($doctorDetails->doctor_info_status == 1) echo "selected"?>>Active</option>
                                        <option value="0" <?php if ($doctorDetails->doctor_info_status == 0) echo "selected"?>>Deactive</option>
                                    </select>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" class="text-right">
                                    <a href="doctor.php" class="btn btn-danger pull-left"><b><i class="fa fa-reply-all"></i> Back</b></a>
                                    <button type="submit" class="btn btn-primary"><b><i class="fa fa-edit"></i> update</b></button>
                                </td>
                            </tr>

                            </tbody>

                            <tfoot>
                            <tr class="btn-danger text-center">
                                <td colspan="2"><b></b></td>
                            </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>

        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="../../../resources/js/admin/raphael-2.1.0.min.js"></script>
<script src="../../../resources/js/admin/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>

<!--Select2 use-->
<script src='http://eoffice.codeslab.net/assets/plugin/select2/select2.full.min.js'></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<!--Select2 use end-->

<!--For message-->
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
</script>
</body>
</html>
