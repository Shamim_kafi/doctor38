<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Doctor\Doctor;
use App\Message\Message;


$objDoctor = new Doctor();

$imgDoctorName = time().$_FILES['doctor_img']['name'];
$sourceImgOne = $_FILES['doctor_img']['tmp_name'];
$destinationImgOne = "../../../resources/images/doctor/".$imgDoctorName;

move_uploaded_file($sourceImgOne,$destinationImgOne);

$_POST['doctor_image_info'] =  $imgDoctorName;


$eventUniquePin=generatePIN();
$_POST['doctor_unique_id'] =  $eventUniquePin;

//Utility::dd($_POST);

$objDoctor->setDoctorData($_POST);
$objDoctor->storeDoctor();
Utility::redirect("doctor.php");


function generatePIN($digits = 6){
    $i = 0; //counter
    $tpin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $tpin .= mt_rand(0, 9);
        $i++;
    }
    return 'DI'.$tpin;
}