<?php
session_start();
require_once("../../vendor/autoload.php");

use App\Doctor\Doctor;
use App\Utility\Utility;
use App\Appointment\Appointment;
use App\MessageBox\MessageBox;


if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    Utility::redirect("index.php");
}


//for latest 3 doctors join
$objDoctor = new Doctor();
$latestDoctorJoinList = $objDoctor->latestDoctorJoin();

$objDoctor = new Doctor();
$currentDate  = date("Y-m-d");
$patientCount = count($objDoctor->allDoctorPatientList($currentDate));

//for new msg count
$objMsgBox = new MessageBox();
$newMsgCount=count($objMsgBox->unSeenMsgCount());


?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Dashboard</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="../../resources/css/admin/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="dashboard.php"><b>Doctor38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Admin Name: <a href="" class="btn btn-info"><b>DOCTOR38</b></a></b>&nbsp;
            <a href="logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="dashboard.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li>
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Doctor</b><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="doctor_type/doctor_type.php"><b><i class="fa fa-sign-out"></i> Doctor Type</b></a>
                            <a href="doctor/doctor.php"><b><i class="fa fa-sign-out"></i> Doctors</b></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="user/user_info.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;User</b></a>
                </li>

                <li>
                    <a href="appointment/appointment_list.php"><span class="fa fa-user"></span>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment
                            <?php if ($patientCount>0){?>
                                <sup style="background: red;color: white;padding:.3em .8em .3em;border-radius:.25em;border-top-left-radius: 30%;border-top-right-radius: 30%;border-bottom-right-radius: 30%;border-bottom-left-radius: 30%;margin-left: 5px;"><?php echo $patientCount?></sup>
                            <?php }?>
                        </b></a>
                </li>

                <li>
                    <a href="transaction/transaction_history.php"><span class="fa fa-history"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="message/message.php"><span class="fa fa-envelope"></span>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;Message
                            <?php if ($newMsgCount>0){?>
                                <sup style="background: red;color: white;padding:.3em .8em .3em;border-radius:.25em;border-top-left-radius: 30%;border-top-right-radius: 30%;border-bottom-right-radius: 30%;border-bottom-left-radius: 30%;margin-left: 5px;"><?php echo $newMsgCount?></sup>
                            <?php }?>
                        </b>
                    </a>
                </li>

                <li>
                    <a href="blog/blog.php"><span class="fa fa-edit"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="Comments/comments_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Comments</b></a>
                </li>

                <li>
                    <a href="profile/admin_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2 style="font-family: Showcard Gothic;"><b><i class="fa fa-dashboard fa-2x"></i> Dashboard</b></h2>
                    <h5><b style="color: #3C8DBC;">Welcome SHAMIM	 , Love to see you back</h5>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover text-center">
                    <thead>
                    <tr class="btn-info" rowspan="2">
                        <td colspan="5">
                            <h3><b>Latest Doctor Join</b></h3>
                        </td>
                    </tr>
                    <tr class="btn-info">
                        <td><b>SL#</b></td>
                        <td><b>DOCTOR REG NUMBER</b></td>
                        <td><b>DOCTOR NAME</b></td>
                        <td><b>DOCTOR MOBILE</b></td>
                        <td><b>ACTION</b></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $sl = 1;
                    foreach ($latestDoctorJoinList as $doctor){

                        echo "
                                    <tr>

                                        <td><b>$sl</b></td>
                                        <td><b>$doctor->doctor_info_reg_id </b></td>
                                        <td><b>$doctor->doctor_info_unique_id <br>$doctor->doctor_info_name</b></td>
                                        <td><b>$doctor->doctor_info_phone</b></td>
                                        <td>
                                           <b><a class='btn btn-primary btn-sm' href='doctor/view_doctor_details.php?doctor_info_id=$doctor->doctor_info_id'><span class='fa fa-eye'></span> View</a></b>                                        
                                           <b><a class='btn btn-info btn-sm' href='doctor/doctor_profile_edit.php?doctor_id=$doctor->doctor_info_id'><span class='fa fa-edit'></span> Edit</a></b>                                                                           
                                        </td>
                                    </tr>    
                                        ";
                        $sl++;
                    }

                    ?>
                    </tbody>

                    <tfoot>

                    <tr class="btn-info">
                        <td colspan="5"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../resources/js/user/jquery.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="../../resources/js/admin/raphael-2.1.0.min.js"></script>
<script src="../../resources/js/admin/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../../resources/js/admin/custom.js"></script>
</body>
</html>
