<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Auth\AdminAuth;

$auth= new AdminAuth();
$auth->setData($_POST);

$check=$auth->adminLogin();
//Utility::dd($check);

if ($check==false){
    Message::message("<div style='color: red' class='eula'>
                                  <strong>Sorry!</strong> Your information is incorrect!
                              </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

else{
    $_SESSION['admin_id']= $check;
    Utility::redirect('../dashboard.php');
}