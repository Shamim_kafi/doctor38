<?php
session_start();
require_once ('../../../vendor/autoload.php');

use App\Utility\Utility;
use App\User\User;
use App\Message\Message;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

$objUser = new User();
$allUserList = $objUser->allUserInfo();

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');

/** Include PHPExcel */


require_once('../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Doctor38")
    ->setLastModifiedBy("Doctor38")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'SL#')
    ->setCellValue('B1', 'USER UNIQUE ID')
    ->setCellValue('C1', 'USER NAME')
    ->setCellValue('D1', 'USER PHONE')
    ->setCellValue('E1', 'USER BLOOD GROUP')
    ->setCellValue('F1', 'USER EMAIL')
    ->setCellValue('G1', 'USER ADDRESS')
    ->setCellValue('H1', 'USER JOIN DATETIME');

$sl=0;
$counter=1;
foreach($allUserList as $user) {
    $sl++;
    $counter++;
// Miscellaneous glyphs, UTF-8
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $counter, $sl)
        ->setCellValue('B' . $counter, $user->user_unique_id)
        ->setCellValue('C' . $counter, $user->user_name)
        ->setCellValue('D' . $counter, $user->user_phone)
        ->setCellValue('E' . $counter, $user->user_blood_group)
        ->setCellValue('F' . $counter, $user->user_email)
        ->setCellValue('G' . $counter, $user->user_address)
        ->setCellValue('H' . $counter, $user->user_insert_datetime);

// Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('USER INFORMATION');

}
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a clientâ€™s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="user_list.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;