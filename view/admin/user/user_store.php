<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\User\User;
use App\Message\Message;


$objUser = new User();

$userUniquePin=generatePIN();
$_POST['user_unique_id'] =  $userUniquePin;

//Utility::dd($_POST);

$objUser->setUserData($_POST);
$objUser->storeUserInfo();
Utility::redirect("user_info.php");


function generatePIN($digits = 6){
    $i = 0; //counter
    $tpin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $tpin .= mt_rand(0, 9);
        $i++;
    }
    return 'UI'.$tpin;
}