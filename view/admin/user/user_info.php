<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\User\User;
use App\Message\Message;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    Utility::redirect("../index.php");
}


$objUser = new User();
$allUserList = $objUser->allUserInfo();



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | User Info</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />

    <link href='http://eoffice.codeslab.net/assets/plugin/select2/select2.min.css' rel='stylesheet' media='screen'>
</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../dashboard.php"><b>DOCTOR38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Admin Name: <a href="#" class="btn btn-info"><b>DOCTOR38</b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../dashboard.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li>
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Doctor</b><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="../doctor_type/doctor_type.php"><b><i class="fa fa-sign-out"></i> Doctor Type</b></a>
                            <a href="../doctor/doctor.php"><b><i class="fa fa-sign-out"></i> Doctors</b></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="user_info.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;User</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-history"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-edit"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../profile/admin_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-history"></i> User Info</b></h2>
                </div>
                <div class="col-md-12">
                    <a class="btn btn-info" data-toggle="modal" data-target="#myModalDoctor">Add User</a>
                    <a href="user_excel.php" class="btn btn-success" >Download As Excel</a>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default btn-default" style="font-family: monospace;">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed table-hover text-center" id="dataTables-example">
                                    <thead>
                                    <tr class="btn-info text-center">
                                        <td><b>SL#</b></td>
                                        <td><b>USER UNIQUE ID</b></td>
                                        <td><b>USER NAME</b></td>
                                        <td><b>USER MOBILE</b></td>
                                        <td><b>USER BLOOD GROUP</b></td>
                                        <td><b>LAST DONATION DATE</b></td>
                                        <td><b>USER EMAIL</b></td>
                                        <td><b>USER ADDRESS</b></td>
                                        <td><b>USER JOIN DATETIME</b></td>
                                        <td><b>STATUS</b></td>
                                        <td><b>ACTION</b></td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    $sl = 1;
                                    foreach ($allUserList as $user){

                                        if ($user->user_status == 1){
                                            $status = '<span class="label label-success">Success</span>';
                                        }else{
                                            $status = '<span class="label label-danger">Delete</span>';
                                        }

                                        echo "
                                    <tr>

                                        <td><b>$sl</b></td>
                                        <td><b>$user->user_unique_id</b></td>
                                        <td><b>$user->user_name</b></td>
                                        <td><b>$user->user_phone</b></td>
                                        <td><b>$user->user_blood_group</b></td>
                                        <td><b>$user->user_last_donation_date</b></td>
                                        <td><b>$user->user_email</b></td>
                                        <td><b>$user->user_address</b></td>
                                        <td><b>$user->user_insert_datetime</b></td>
                                        <td>$status</td>
                                        <td>                                 
                                           <b><a class='btn btn-danger btn-sm' href='user_delete.php?user_id=$user->user_id'><span class='fa fa-remove'></span> Delete</a></b>                                        
                                        </td>
                                    </tr>    
                                        ";
                                        $sl++;
                                    }

                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="btn-info text-center">

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <h2 class="text-center"><b>Total User : <?php echo count($allUserList)?></b></h2>
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <span class="btn pull-left"><b>Developed by @ Doctor38 Ltd.</b></span>
                    <span class="btn pull-right"><b>Copyright @ 2018</b></span>
                </div>
            </div>
        </div>
    </div>
    <!-- /. PAGE INNER  -->

    <!-- Modal add doctor info -->
    <div class="modal fade" id="myModalDoctor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add User</h4>
                </div>

                <form id="form" action="user_store.php" method="post" autocomplete="off" >
                    <div class="modal-body">

                        <table class="table table-bordered table-condensed text-left">
                            <thead>
                            <tr class="btn-info text-center">
                                <td colspan="2"><h3><b>Enter Right Information Below</b></h3></td>
                            </tr>

                            </thead>
                            <tbody>

                            <tr>
                                <td class="text-right text-info"><h5><b>User Name :</b></h5></td>
                                <td><input class="form-control" type="text" name="user_name" placeholder="Enter User Name" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>User Phone Number :</b></h5></td>
                                <td><input class="form-control" type="text" name="phone_num" placeholder="Enter User Name" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Blood Group :</b></h5></td>
                                <td><b>
                                        <select class="form-control select2" style="width: 100%" name="blood_group" required>

                                            <option selected >Please Select Blood Group</option>
                                            <option value='A+'>A+</option>
                                            <option value='A-'>A-</option>
                                            <option value='AB+'>AB+</option>
                                            <option value='AB-'>AB-</option>
                                            <option value='B+'>B+</option>
                                            <option value='B-'>B-</option>
                                            <option value='O+'>O+</option>
                                            <option value='O-'>O-</option>

                                        </select>
                                    </b>
                                </td>
                            </tr>


                            <tr>
                                <td class="text-right text-info"><h5><b>User Email :</b></h5></td>
                                <td><input class="form-control" type="text" name="user_email" placeholder="Enter User Email" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>User Key Point (Address) :</b></h5></td>
                                <td>
                                    <select class="form-control" style="width: 100%" name="user_address" required>
                                        <option selected >Please Select Key Point</option>
                                        <option value="GEC">GEC</option>
                                        <option value="Agrabad">Agrabad</option>
                                        <option value="Chawkbazar">Chawkbazar</option>
                                        <option value="Provortok">Provortok</option>
                                        <option value="2 Number Gate">2 Number Gate</option>
                                        <option value="Muradpur">Muradpur</option>
                                        <option value="Andorkilla">Andorkilla</option>
                                        <option value="Medical Gate">Medical Gate</option>
                                        <option value="Kumira">Kumira</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><b>Last Donation Date :</b></td>
                                <td>
                                    <input type="text" id="datepicker1" name="last_donation_date" class="form-control" placeholder="Select Last Donation Date" required>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>User Password :</b></h5></td>
                                <td><input class="form-control" type="password" name="user_pass" placeholder="Enter Password" required="" /></td>
                            </tr>

                            </tbody>
                        </table>

                        <!-- End Form Elements -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit"  value="Save changes" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<!--Select2 use-->
<script src='http://eoffice.codeslab.net/assets/plugin/select2/select2.full.min.js'></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<!--Select2 use end-->
<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../../../resources/js/admin/jquery.dataTables.js"></script>
<script src="../../../resources/js/admin/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
    });
</script>
<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>

<!-- Calendar -->
<link rel="stylesheet" href="../../../resources/dateinput/jquery-ui.css" />
<script src="../../../resources/dateinput/jquery-ui.js"></script>

<script>
    $(document).ready(function () {

        $("#datepicker1").datepicker({

            showAnim :'drop',
            numberOfMonths : 2,
            dateFormat : 'dd-mm-yy'
        });
    });
</script>
<!-- //Calendar -->


</body>
</html>

