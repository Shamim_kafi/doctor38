<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\MessageBox\MessageBox;

//Utility::dd($_POST);

//for message history
$objMessageBox = new MessageBox();
$objMessageBox->msgIsDelete($_GET['msg_id']);

return Utility::redirect($_SERVER['HTTP_REFERER']);


