<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\MessageBox\MessageBox;
use App\Message\Message;
use App\Doctor\Doctor;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    Utility::redirect("../index.php");
}


//for message history
$objMessageBox = new MessageBox();
$allMessageList = $objMessageBox->allMessageHistory();

//all doctor
$objDoctor = new Doctor();
$allDoctors = $objDoctor->allDoctorInfoWithDesignation();


?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Message Box</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />

    <link href='http://eoffice.codeslab.net/assets/plugin/select2/select2.min.css' rel='stylesheet' media='screen'>

    <!--Select2 use-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><b>Doctor38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Admin: <a href="" class="btn btn-info"><b>DOCTOR38</b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../dashboard.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li>
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Doctor</b><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li>
                            <a href="../doctor_type/doctor_type.php"><b><i class="fa fa-sign-out"></i> Doctor Type</b></a>
                            <a href="../doctor/doctor.php"><b><i class="fa fa-sign-out"></i> Doctors</b></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="../user/user_info.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;User</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-history"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-edit"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../profile/admin_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-envelope"></i> Message List</b></h2>
                </div>

                <div class="col-md-12">
                    <a class="btn btn-primary" data-toggle="modal" data-target="#myModal">Doctor Msg Send</a>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr/>


            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <!--<div>
                        <a href="appoint_excel.php?date=" class="btn btn-primary"><span class="fa fa-download"></span> Excel</a>
                        <a href="" class="btn btn-warning"><span class="fa fa-envelope"></span> Email</a>
                    </div>
                    <br>-->
                    <div class="panel panel-default btn-default" style="font-family: monospace;">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed table-hover text-center" id="dataTables">
                                    <thead>
                                    <tr class="btn-info text-center">
                                        <td><b>SL #</b></td>
                                        <td><b>DOCTOR</b></td>
                                        <td><b>USER</b></td>
                                        <td><b>MESSAGE DETAILS</b></td>
                                        <td><b>DATE TIME</b></td>
                                        <td><b>ACTION</b></td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    $serial = 1;
                                    foreach ($allMessageList as $message){
                                        echo "
                                                <tr>
                                                    <td><b>$serial</b></td>
                                            
                                                    <td><b>Name : Dr. $message->doctor_info_name 
                                                        <br>Email : $message->doctor_info_email 
                                                        <br>Phone : $message->doctor_info_phone</b>
                                                    </td>
                                                 
                                                    <td><b>Name : $message->user_name 
                                                        <br>Phone : $message->user_phone 
                                                        <br>Blood : $message->user_blood_group</b>
                                                    </td>
                                                    
                                                    <td><b>$message->msg_details</b></td>
                                                    <td><b>$message->msg_datetime</b></td>
                                                    
                                                    <td>
                                                       <b><a class='btn btn-info btn-sm' href='msg_reply.php?msg_id=$message->msg_id&user_id=$message->msg_user_id_ref&doctor_id=$message->msg_doctor_id_ref'><span class='fa fa-reply'></span> Reply</a></b>
                                                       <b><a class='btn btn-danger btn-sm' href='msg_delete.php?msg_id=$message->msg_id'><span class='fa fa-remove'></span> Delete</a></b>
                                                    </td>
                                  
                                                </tr>
                                             ";
                                        $serial++;
                                    }
                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="btn-info text-center">

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <h2 class="text-center"><b>Total Message : <?php echo count($allMessageList)?></b></h2>
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>

            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <span class="btn pull-left"><b>Developed by @ Doctor38 Ltd.</b></span>
                    <span class="btn pull-right"><b>Copyright @ 2019</b></span>
                </div>
            </div>




        </div>
    </div>
    <!-- /. PAGE INNER  -->
</div>

<!-- Modal add doctor msg -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Doctor Msg</h4>
            </div>

            <form id="form" action="msg_reply_store.php" method="post" autocomplete="off" >

                <input type="hidden" name="msg_user_id" value="0">
                <input type="hidden" name="msg_admin_id" value="1">
                <input type="hidden" name="msg_send" value="0">

                <div class="modal-body">

                    <!--  Message insert-->
                    <?php

                    $msg =Message::message();
                    if ($msg){
                        echo "
                           $msg
                           ";
                    }
                    ?>
                    <!-- Message insert end-->
                    <!-- Form Elements -->

                    <table class="table table-bordered table-condensed text-left">
                        <thead>
                        <tr class="btn-info text-center">
                            <td colspan="2"><h3><b>Enter Right Information Below</b></h3></td>
                        </tr>

                        </thead>
                        <tbody>

                        <tr>
                            <td class="text-right text-info"><h5><b>Doctor :</b></h5></td>
                            <td>
                                    <select class="form-control select2" style="width: 100%" name="msg_doctor_id" required>
                                        <option selected >Select Doctor</option>
                                        <?php
                                        foreach ($allDoctors as $doctor){
                                            echo "
                                            <option value='$doctor->doctor_info_id'>$doctor->doctor_info</option>
                                    ";
                                        }
                                        ?>

                                    </select>

                            </td>
                        </tr>

                        <tr>
                            <td class="text-right text-info"><h5><b>Msg Details :</b></h5></td>
                            <td>
                                <textarea class="form-control" name="msg_details" id="" cols="30" rows="5" placeholder="Details..."></textarea>
                            </td>
                        </tr>

                        </tbody>
                    </table>

                    <!-- End Form Elements -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary">
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../../../resources/js/admin/jquery.dataTables.js"></script>
<script src="../../../resources/js/admin/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables').dataTable();
    });
</script>



<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>



</body>
</html>

