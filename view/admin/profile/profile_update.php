<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Admin\Admin;

if (!isset($_SESSION['admin_id']) && empty($_SESSION['admin_id'])){

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

$objAdminInfo = new Admin();
$objAdminInfo->setAdminData($_POST);
$objAdminInfo->singleAdminInfoUpdate();

return Utility::redirect($_SERVER['HTTP_REFERER']);