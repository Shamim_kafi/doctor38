<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Auth\AssistantAuth;

$auth= new AssistantAuth();
$auth->setData($_POST);

$check=$auth->assistantLogin();
//Utility::dd($check);

if ($check==false){
    Message::message("<div style='color: red' class='eula'>
                                  <strong>Sorry!</strong> Your information is incorrect!
                              </div>");
    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

else{
    $_SESSION['assistant_id']= $check;
    Utility::redirect('../index.php');
}