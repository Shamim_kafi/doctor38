<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;

if (isset($_SESSION['assistant_id'])){
    Utility::redirect('../index.php');
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Doctor38 | Assistant</title>
    <link rel="stylesheet" href="../../../resources/css/admin/login.css">
    <!--<link rel="stylesheet" href="../../resources/bootstrap/css/bootstrap.min.css">-->
    <!-- js -->
    <script src="../../../resources/js/user/jquery.min.js"></script>
</head>
<body>
<div class="page">
    <div class="container">

        <div class="left">
            <!--  Message insert-->
            <?php

            $msg =Message::message();
            if ($msg){
                echo "
                           $msg
                     ";
            }
            ?>
            <div class="login">Login</div>
        </div>

        <div class="right">
            <form action="login_store.php" method="post" autocomplete="off">
                <div class="form" >
                    <label for="phone">Phone</label>
                    <input  name="assistant_phone" type="text" id="phone" required>
                    <label for="password">Password</label>
                    <input  name="assistant_pass" type="password" id="password" required>
                    <input style="color: white;background: #1c87ff" type="submit" id="submit" value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>

<!--For message-->
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
</script>

</body>
</html>