<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;
use App\DoctorType\DoctorType;
use App\Appointment\Appointment;
use App\Deposit\Deposit;


$objAppointment = new Appointment();
$singleAppointDetails = $objAppointment->singleAppointDetails($_POST['appoint_id']);

$orderNo = $singleAppointDetails->appoint_invoice_id;
$serialNumber = $singleAppointDetails->appoint_serial_num;

$userId = $singleAppointDetails->user_id;
$userName = $singleAppointDetails->user_name;
$userPhone = $singleAppointDetails->user_phone;

$doctorId = $singleAppointDetails->doctor_info_id;
$doctorName = $singleAppointDetails->doctor_info_name;

$chamberId = $singleAppointDetails->doctor_chamber_id;
$chamberName = $singleAppointDetails->doctor_chamber_loc_title;

$tnxId = $singleAppointDetails->appoint_transaction_id;
$totalAmount = 100;

//Utility::dd($totalAmount);

if ($_POST['tnx_method'] == 1){
    if ($_POST['amount'] = $totalAmount){

        $dep_tnxId=strtoupper(uniqid('D'));

        $objAppointment->updateSingleUserAppointConfirm($_POST['appoint_id'],$dep_tnxId);

        $_POST['deposit_tnx_id'] = $dep_tnxId;
        $_POST['deposit_user_id'] = $userId;
        $_POST['deposit_doctor_id'] = $doctorId;
        $_POST['deposit_appoint_id'] = $_POST['appoint_id'];
        $_POST['deposit_chamber_id'] = $chamberId;
        $_POST['dep_method_id'] = $_POST['tnx_method'];
        $_POST['dep_amount'] = $totalAmount;

        //Utility::dd($_POST);
        $objDeposit = new Deposit();
        $objDeposit->setData($_POST);
        $objDeposit->storeDeposit();

        /*Mobile msg*/
        /*$token = "1ef057c18f3470d6abd83f8a45df6e95";

        $message = "Hello,".$userName." Order details for your order No.".$orderNo." 
           have been confirmed. We will inform you when your order is on its way to you. Thank you for with us....";

        $url = "http://sms.greenweb.com.bd/api.php";


        $data= array(
            'to'=>"$userPhone",
            'message'=>"$message",
            'token'=>"$token"
        ); // Add parameters in key value
        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $smsresult = curl_exec($ch);*/

        /*Mobile msg end*/

        Message::message("<div class='alert alert-success' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Successfully!</strong> Payment has been received!
                              </div>");
        Utility::redirect("appointment_list.php");
    }
}

elseif ($_POST['tnx_method'] == 2){
    if ($_POST['txId'] == $tnxId && $_POST['amount'] = $totalAmount){

        /*Mobile msg*/
          /*$token = "1ef057c18f3470d6abd83f8a45df6e95";

           $message = "Hello,".$userName." Order details for your order No.".$orderNo." 
           have been confirmed. We will inform you when your order is on its way to you. Thank you for with us....";

           $url = "http://sms.greenweb.com.bd/api.php";


           $data= array(
               'to'=>"$userPhone",
               'message'=>"$message",
               'token'=>"$token"
           ); // Add parameters in key value
           $ch = curl_init(); // Initialize cURL
           curl_setopt($ch, CURLOPT_URL,$url);
           curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           $smsresult = curl_exec($ch);*/

        /*Mobile msg end*/

        $objAppointment->updateSingleUserAppointConfirm($_POST['appoint_id'],$_POST['txId']);

        $_POST['deposit_tnx_id'] = $_POST['txId'];
        $_POST['deposit_user_id'] = $userId;
        $_POST['deposit_doctor_id'] = $doctorId;
        $_POST['deposit_appoint_id'] = $_POST['appoint_id'];
        $_POST['deposit_chamber_id'] = $chamberId;
        $_POST['dep_method_id'] = $_POST['tnx_method'];
        $_POST['dep_amount'] = $totalAmount;

        $objDeposit = new Deposit();
        $objDeposit->setData($_POST);
        $objDeposit->storeDeposit();

        Message::message("<div class='alert alert-success' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Done!</strong> Transaction Number is correct!
                              </div>");
        Utility::redirect("appointment_list.php");
    }
    else{
        Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Wrong transaction Number!
                              </div>");
        Utility::redirect("appointment_list.php");
    }
}
else{
    Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Wrong transaction Number!
                              </div>");
    Utility::redirect("appointment_list.php");
}



