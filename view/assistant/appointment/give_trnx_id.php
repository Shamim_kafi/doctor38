<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Assistant\Assistant;
use App\Appointment\Appointment;

if (!isset($_SESSION['assistant_id']) && empty($_SESSION['assistant_id'])){

    Utility::redirect("../login/login.php");
}


$objAssistantInfo = new Assistant();
$assistantDetails = $objAssistantInfo->singleAssistantDetails($_SESSION['assistant_id']);
$doctorId = $assistantDetails->assistant_doctor_id_ref;

$objAppointment = new Appointment();
$singleAppointDetails = $objAppointment->singleAppointDetails($_GET['appoint_id']);



?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="../../../resources/css/admin/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    <!--Select2 use-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><b>Doctor38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Doctor Name: <a href="" class="btn btn-info"><b><?php echo $assistantDetails->assistant_name?></b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../index.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li class="active">
                    <a href="../profile/assistant_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>


                <li>
                    <a href="appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>


                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-lock"></i>
                            <?php if ($singleAppointDetails->appoint_payment_method_id_ref == 1){?>
                                Cash Amount Receive
                            <?php }elseif($singleAppointDetails->appoint_payment_method_id_ref == 2){?>
                                Check bKash TnxId.
                            <?php }?>
                        </b></h2>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <!-- Form Elements -->
                    <div class="panel panel-default btn-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form id="form" action="check_tnxId.php" method="post" autocomplete="off" >
                                        <table class="table table-condensed text-left">
                                            <thead>
                                            <tr class="btn-danger text-center">
                                                <?php if ($singleAppointDetails->appoint_payment_method_id_ref == 1){?>
                                                    <td colspan="2"><h3><b>Cash Payment</b></h3></td>
                                                <?php }elseif($singleAppointDetails->appoint_payment_method_id_ref == 2){?>
                                                    <td colspan="2"><h3><b>Enter Patient bKash TnxId.</b></h3></td>
                                                <?php } ?>
                                            </tr>
                                            <tr>
                                                <td class="text-center" colspan="2">
                                                    <img class="img img-thumbnail img-circle btn disabled" src="../../../resources/images/lock.png" alt="Lock" />
                                                </td>
                                            </tr>
                                            <?php if ($singleAppointDetails->appoint_payment_method_id_ref == 1){?>
                                                <tr>
                                                    <td class="text-right text-info"><h5><b>Enter Amount :</b></h5></td>
                                                    <td>
                                                        <b>
                                                            <input class="form-control" type="number" min="100"  name="amount" placeholder="Enter amount" autocomplete="off" required="" />
                                                            <input type="hidden" name="appoint_id" value="<?php echo $_GET['appoint_id']?>" />
                                                            <input type="hidden" name="tnx_method" value="<?php echo $singleAppointDetails->appoint_payment_method_id_ref ?>" />
                                                        </b>
                                                    </td>
                                                </tr>
                                            <?php }elseif($singleAppointDetails->appoint_payment_method_id_ref == 2){?>
                                                <tr>
                                                    <td class="text-right text-info"><h5><b>Tnx No. :</b></h5></td>
                                                    <td>
                                                        <b>
                                                            <input class="form-control" type="text" name="txId" placeholder="Enter Tnx No." autocomplete="off" required="" />
                                                            <input type="hidden" name="appoint_id" value="<?php echo $_GET['appoint_id']?>" />
                                                            <input type="hidden" name="tnx_method" value="<?php echo $singleAppointDetails->appoint_payment_method_id_ref ?>" />
                                                        </b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right text-info"><h5><b>Amount :</b></h5></td>
                                                    <td>
                                                        <b>
                                                            <input class="form-control" type="number"  min="100" name="amount" placeholder="Enter Amount." autocomplete="off" required="" />
                                                        </b>
                                                    </td>
                                                </tr>
                                            <?php } ?>

                                            <tr>
                                                <td colspan="2" class="text-right">
                                                    <a href="appointment_list.php" class="btn btn-danger pull-left"><b><i class="fa fa-reply-all"></i> Back</b></a>
                                                    <button type="submit" class="btn btn-primary"><b><i class="fa fa-eye"></i>
                                                            <?php if ($singleAppointDetails->appoint_payment_method_id_ref == 1){?>
                                                                Receive
                                                            <?php }elseif($singleAppointDetails->appoint_payment_method_id_ref == 2){?>
                                                                Check
                                                            <?php } ?>
                                                        </b>
                                                    </button>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr class="btn-danger text-center">
                                                <td colspan="2"><b></b></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Elements -->
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
    </div>
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="../../../resources/js/admin/raphael-2.1.0.min.js"></script>
<script src="../../../resources/js/admin/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>

<!--Select2 use-->
<script src='http://eoffice.codeslab.net/assets/plugin/select2/select2.full.min.js'></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<!--Select2 use end-->

<!--For message-->
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
</script>
</body>
</html>
