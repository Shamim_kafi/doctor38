<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Assistant\Assistant;


$objAssistantInfo = new Assistant();
$objAssistantInfo->setDoctorAssistantData($_POST);
$objAssistantInfo->assistantSelfProfileUpdate();

return Utility::redirect($_SERVER['HTTP_REFERER']);