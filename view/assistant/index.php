<?php
session_start();
require_once("../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;
use App\Assistant\Assistant;

if (!isset($_SESSION['assistant_id']) && empty($_SESSION['assistant_id'])){

    Utility::redirect("login/login.php");
}

$objAssistantInfo = new Assistant();
$assistantDetails = $objAssistantInfo->singleAssistantDetails($_SESSION['assistant_id']);

$doctorId = $assistantDetails->assistant_doctor_id_ref;

$objDoctorInfo = new Doctor();
$currentDate  = date("Y-m-d");
$patientListCount = count($objDoctorInfo->allSingleDoctorPatientCount($doctorId,$currentDate));

//Utility::dd($patientListCount);
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Assistant Dashboard</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- MORRIS CHART STYLES-->
    <link href="../../resources/css/admin/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><b>Doctor38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Assistant Name: <a href="" class="btn btn-info"><b><?php echo $assistantDetails->assistant_name?></b></a></b>&nbsp;
            <a href="logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="index.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li>
                    <a href="profile/assistant_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment
                            <?php if ($patientListCount>0){?>
                                <sup style="background: red;color: white;padding:.3em .8em .3em;border-radius:.25em;border-top-left-radius: 30%;border-top-right-radius: 30%;border-bottom-right-radius: 30%;border-bottom-left-radius: 30%;margin-left: 5px;"><?php echo $patientListCount?></sup>
                            <?php }?>
                        </b>
                    </a>
                </li>


                <li>
                    <a href="logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2 style="font-family: Showcard Gothic;"><b><i class="fa fa-dashboard fa-2x"></i> Dashboard</b></h2>
                    <h5><b style="color: #3C8DBC;">Welcome <?php echo $assistantDetails->assistant_name?>	 , Love to see you back</h5>
                </div>
            </div>

        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../resources/js/user/jquery.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="../../resources/js/admin/raphael-2.1.0.min.js"></script>
<script src="../../resources/js/admin/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../../resources/js/admin/custom.js"></script>
</body>
</html>
