<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\User\User;

$objUserInfo = new User();
$objUserInfo->setUserData($_POST);
$objUserInfo->singleUserInfoUpdate();

return Utility::redirect($_SERVER['HTTP_REFERER']);