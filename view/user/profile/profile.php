<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\DoctorType\DoctorType;
use App\Message\Message;
use App\Doctor\Doctor;
use App\User\User;

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

//for user details
$objUser = new User();
$userInfo = $objUser->singleUserInfo($_SESSION['u_id']);


//\App\Utility\Utility::dd($allDoctorListShow);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Doctor38 | Profile</title>

    <!-- Font awesome -->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="../../../resources/css/user/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="../../../resources/css/user/nouislider.css">
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="../../../resources/css/user/jquery.fancybox.css" type="text/css" media="screen" />
    <!-- Theme color -->
    <link id="switcher" href="../../../resources/css/user/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="../../../resources/css/user/style.css" rel="stylesheet">


    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



</head>
<body class="aa-price-range">
<!-- Pre Loader -->
<div id="aa-preloader-area">
    <div class="pulse"></div>
</div>
<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
<!-- END SCROLL TOP BUTTON -->


<!-- Start header section -->
<header id="aa-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-header-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="aa-header-left">
                                <div class="aa-telephone-no">
                                    <span class="fa fa-phone"></span>
                                    880-01837210137
                                </div>
                                <div class="aa-email hidden-xs">
                                    <span class="fa fa-envelope-o"></span> doctor38@gmail.com
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="aa-header-right">
                                <?php if (!isset($_SESSION['u_id'])) {
                                    echo "
                                        <a href=\"#\" class=\"aa-register\">Doctor</a>
                                        <a href=\"../signup/signin.php\" class=\"aa-login\">User</a>
                                    ";
                                } else{
                                    echo "
                                        <a style='color: red' href='../logout/logout.php' class=\"aa-login\">Logout</a>
                                    ";
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End header section -->
<!-- Start menu section -->
<section id="aa-menu-area">
    <nav class="navbar navbar-default main-navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- LOGO -->
                <!-- Text based logo -->
                <a class="navbar-brand aa-logo" href="../../../index.php"> Doctor <span>38</span></a>
                <!-- Image based logo -->
                <!-- <a class="navbar-brand aa-logo-img" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul id="top-menu" class="nav navbar-nav navbar-right aa-main-nav">
                    <li><a href="../../../index.php">HOME</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">DOCTORS <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">

                            <?php
                            foreach ($allDoctorType as $doctorType){
                                echo "
                                     <li><a href='../doctor_profile_view.php?doctor_type_id=$doctorType->doctor_type_id'>$doctorType->doctor_type_name</a></li>
                                    ";
                            }
                            ?>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">BLOG <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">ARTICLES</a></li>
                            <li><a href="#">TIPS</a></li>
                        </ul>
                    </li>
                    <li><a href="#">CONTACT</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</section>
<!-- End menu section -->

<!-- Start Proerty header  -->

<section id="aa-property-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-property-header-inner">
                    <h2><?php echo strtoupper($userInfo->user_name)?></h2>
                    <ol class="breadcrumb">
                        <li><a href="#">HOME</a></li>
                        <li class="active">PROFILE</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Proerty header  -->

<!-- Start Properties  -->
<section id="aa-properties">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="aa-properties-content">
                    <!-- Start properties content body -->
                    <div class="aa-properties-details">
                        <!--  Message insert-->
                        <?php

                        $msg =Message::message();
                        if ($msg){
                            echo "
                                      $msg
                                ";
                        }
                        ?>

                        <!-- Message insert end-->

                        <div class="row">
                            <h4>About <?php echo $userInfo->user_name?></h4>
                            <hr>
                            <form action="user_profile_update.php" method="post">

                                <input type="hidden" name="user_id"  value="<?php echo $userInfo->user_id?>">

                                <h4>Id :</h4>
                                <input type="text" class="form-control" name="user_unique_id" value="<?php echo $userInfo->user_unique_id?>" readonly disabled>

                                <h4>Name :</h4>
                                <input type="text" class="form-control" name="user_name" value="<?php echo $userInfo->user_name?>">

                                <h4>Phone :</h4>
                                <input type="text" class="form-control" name="phone_num" value="<?php echo $userInfo->user_phone?>">

                                <h4>Blood Group :</h4>
                                <input type="text" class="form-control" name="blood_group" value="<?php echo $userInfo->user_blood_group?>">

                                <h4>Last Donation Date :</h4>
                                <input type="text" id="datepicker1" class="form-control" name="last_donation_date" value="<?php echo $userInfo->user_last_donation_date?>">

                                <h4>Email :</h4>
                                <input type="text" class="form-control" name="user_email" value="<?php echo $userInfo->user_email?>">

                                <h4>Key Point :</h4>
                                <select class="form-control" style="width: 100%" name="user_address">
                                    <option value="GEC" <?php if ($userInfo->user_address == 'GEC') echo "selected "?>>GEC</option>
                                    <option value="Agrabad" <?php if ($userInfo->user_address == 'Agrabad') echo "selected "?>>Agrabad</option>
                                    <option value="Chawkbazar" <?php if ($userInfo->user_address == 'Chawkbazar') echo "selected "?>>Chawkbazar</option>
                                    <option value="Provortok" <?php if ($userInfo->user_address == 'Provortok') echo "selected "?>>Provortok</option>
                                    <option value="2 Number Gate" <?php if ($userInfo->user_address == '2 Number Gate') echo "selected "?>>2 Number Gate</option>
                                    <option value="Muradpur" <?php if ($userInfo->user_address == 'Muradpur') echo "selected "?>>Muradpur</option>
                                    <option value="Andorkilla" <?php if ($userInfo->user_address == 'Andorkilla') echo "selected "?>>Andorkilla</option>
                                    <option value="Kumira" <?php if ($userInfo->user_address == 'Kumira') echo "selected "?>>Kumira</option>
                                </select>

                                <h4>Password :</h4>
                                <input type="text" class="form-control" name="user_pass" placeholder="Enter New Password">

                                <br>
                                <input type="submit" value="Update" class="btn btn-info">

                            </form>

                        </div>
                        <!-- Properties social share -->
                        <div class="aa-properties-social">
                            <ul>
                                <li>Share</li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start properties sidebar -->
            <div class="col-md-4">
                <aside class="aa-properties-sidebar">
                    <!-- Start Single properties sidebar -->
                    <div class="aa-properties-single-sidebar">
                        <h3>Doctors Search</h3>
                        <form action="">
                            <div class="aa-single-advance-search">
                                <input type="text" placeholder="Type Your Location">
                            </div>
                            <div class="aa-single-advance-search">
                                <select id="" name="">
                                    <option value="0" selected>Select Location</option>
                                    <option value="1">GEC</option>
                                    <option value="2">Agrabad</option>
                                    <option value="2">Chawkbazar</option>
                                    <option value="2">Andorkilla</option>
                                    <option value="2">Povortok</option>
                                    <option value="2">Medical Gate</option>
                                </select>
                            </div>
                            <div class="aa-single-advance-search">
                                <select id="" name="">
                                    <option value="0">Category</option>
                                    <option value="1" selected>Doctor</option>
                                    <option value="2">Blood Donor</option>
                                </select>
                            </div>
                            <div class="aa-single-advance-search">
                                <select id="" name="">
                                    <option value="0" selected disabled>Select Specialities</option>
                                    <?php
                                    foreach ($allDoctorType as $doctorType){
                                        echo "
                                 <option value='$doctorType->doctor_type_id'>$doctorType->doctor_type_name</option>
                            ";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="aa-single-filter-search">
                                <span>AREA (MILES)</span>
                                <span>FROM</span>
                                <span id="skip-value-lower" class="example-val">30.00</span>
                                <span>TO</span>
                                <span id="skip-value-upper" class="example-val">100.00</span>
                                <div id="aa-sqrfeet-range" class="noUi-target noUi-ltr noUi-horizontal noUi-background">
                                </div>
                            </div>
                            <div class="aa-single-advance-search">
                                <input type="submit" value="Search" class="aa-search-btn">
                            </div>
                        </form>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</section>
<!-- / Properties  -->

<!-- Footer -->
<footer id="aa-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-footer-area">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="aa-footer-left">
                                <p>Designed by <a rel="nofollow" href="#">Doctor38</a></p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="aa-footer-middle">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="aa-footer-right">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- / Footer -->

<!-- jQuery library -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script src="../../../resources/js/user/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="../../../resources/js/user/slick.js"></script>
<!-- Price picker slider -->
<script type="text/javascript" src="../../../resources/js/user/nouislider.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="../../../resources/js/user/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="../../../resources/js/user/jquery.fancybox.pack.js"></script>
<!-- Custom js -->
<script src="../../../resources/js/user/custom.js"></script>

<!-- Calendar -->
<link rel="stylesheet" href="../../../resources/dateinput/jquery-ui.css" />
<script src="../../../resources/dateinput/jquery-ui.js"></script>

<script>
    $(document).ready(function () {

        $("#datepicker1").datepicker({

            showAnim :'drop',
            numberOfMonths : 2,
            dateFormat : 'dd-mm-yy'
        });
    });
</script>
<!-- //Calendar -->

<!--For message-->
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
</script>

</body>
</html>