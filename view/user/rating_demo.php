
<html>
    <head>
        <title>5 Star Rating system with jQuery, AJAX, and PHP</title>

        <!-- CSS -->

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link href='RatingRes/fontawesome-stars.css' rel='stylesheet' type='text/css'>
        
        <!-- Script -->
        <script src="../../resources/js/user/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="RatingRes/jquery.barrating.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(function() {
            $('.rating').barrating({
                theme: 'fontawesome-stars',
                onSelect: function(value, text, event) {

                    // Get element id by data-id attribute
                    var el = this;
                    var el_id = el.$elem.data('id');

                    // rating was selected by a user
                    if (typeof(event) !== 'undefined') {

                        var split_id = el_id.split("_");

                        var postid = split_id[1];  // postid

                        // AJAX Request
                        $.ajax({
                            url: 'rating_ajax.php',
                            type: 'post',
                            data: {postid:postid,rating:value},
                            dataType: 'json',
                            success: function(data){
                                // Update average
                                var average = data['averageRating'];
                                $('#avgrating_'+postid).text(average);
                            }
                        });
                    }
                }
            });
        });

        </script>
    </head>
    <body>
        <div class="content">




                <!-- Rating -->
                <select class='rating' id='rating_' data-id='rating_'>
                    <option value="1" >1</option>
                    <option value="2" >2</option>
                    <option value="3" >3</option>
                    <option value="4" >4</option>
                    <option value="5" >5</option>
                </select>
                <div style='clear: both;'></div>
                Average Rating : <span id='avgrating_'>1</span>

                <!-- Set rating -->
                <script type='text/javascript'>
                    $(document).ready(function(){
                        $('#rating_').barrating('set',1);
                    });

                </script>



        </div>

        
    </body>
</html>
