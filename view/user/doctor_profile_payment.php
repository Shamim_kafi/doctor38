<?php
session_start();
require_once ("../../vendor/autoload.php");

use App\DoctorType\DoctorType;
use App\DoctorChamber\DoctorChamber;
use App\Utility\Utility;
use App\User\User;

if (!isset($_SESSION['u_id'])){
    Utility::redirect("signup/signin.php");
}

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

//for user details
$objUser = new User();
$userInfo = $objUser->singleUserInfo($_SESSION['u_id']);

$objChamber = new DoctorChamber();
$doctorSingleChamberInfo = $objChamber->doctorSingleChamberInfo($_GET['chamber_id']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Doctor38 | Payment</title>

    <!-- Font awesome -->
    <link href="../../resources/awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="../../resources/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="../../resources/css/user/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="../../resources/css/user/nouislider.css">
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="../../resources/css/user/jquery.fancybox.css" type="text/css" media="screen" />
    <!-- Theme color -->
    <link id="switcher" href="../../resources/css/user/theme-color/default-theme.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="../../resources/css/user/payment_method.css">

    <!-- Main style sheet -->
    <link href="../../resources/css/user/style.css" rel="stylesheet">


    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href='http://eoffice.codeslab.net/assets/plugin/select2/select2.min.css' rel='stylesheet' media='screen'>

    <!--Select2 use-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>


</head>
<body class="aa-price-range">
<!-- Pre Loader -->
<div id="aa-preloader-area">
    <div class="pulse"></div>
</div>
<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
<!-- END SCROLL TOP BUTTON -->


<!-- Start header section -->
<header id="aa-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-header-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="aa-header-left">
                                <div class="aa-telephone-no">
                                    <span class="fa fa-phone"></span>
                                    880-01837210137
                                </div>
                                <div class="aa-email hidden-xs">
                                    <span class="fa fa-envelope-o"></span> doctor38@gmail.com
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="aa-header-right">
                                <?php if (!isset($_SESSION['u_id'])) {
                                    echo "
                                        <a href=\"#\" class=\"aa-register\">Doctor</a>
                                        <a href=\"signup/signin.php\" class=\"aa-login\">User</a>
                                    ";
                                } else{
                                    echo "
                                        <a style='color: red' href='logout/logout.php' class=\"aa-login\">Logout</a>
                                    ";
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End header section -->
<!-- Start menu section -->
<section id="aa-menu-area">
    <nav class="navbar navbar-default main-navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- LOGO -->
                <!-- Text based logo -->
                <a class="navbar-brand aa-logo" href="../../index.php"> Doctor <span>38</span></a>
                <!-- Image based logo -->
                <!-- <a class="navbar-brand aa-logo-img" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul id="top-menu" class="nav navbar-nav navbar-right aa-main-nav">
                    <li><a href="../../index.php">HOME</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">DOCTORS <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">

                            <?php
                            foreach ($allDoctorType as $doctorType){
                                echo "
                                     <li><a href='doctor_profile_view.php?doctor_type_id=$doctorType->doctor_type_id'>$doctorType->doctor_type_name</a></li>
                                    ";
                            }
                            ?>

                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">BLOG <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">ARTICLES</a></li>
                            <li><a href="#">TIPS</a></li>
                        </ul>
                    </li>
                    <li><a href="#">CONTACT</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</section>
<!-- End menu section -->

<!-- Start Proerty header  -->

<section id="aa-property-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-property-header-inner">
                    <h2>Doctor Appointment Payment</h2>
                    <ol class="breadcrumb">
                        <li><a href="../../index.php">HOME</a></li>
                        <li class="active">APPOINTMENT PAYMENT</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Proerty header  -->

<div id="payment-page">
    <div class="container">
        <div id="payment-content-wrapper">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <section class="checkout-header">
                        <h1>Payment Method</h1>
                        <span class="payment-header__sub-title">(Please select <em>only one!</em> payment method)</span>
                    </section>

                    <div id="payment-option">

                        <!-- Cash -->
                        <section class="payment-option-item ">
                            <div class="custom-control custom-checkbox">
                                <form action="payment_confirm.php" method="post">

                                    <input type="hidden" name="chamber_id" value="<?php echo $doctorSingleChamberInfo->doctor_chamber_id?>">
                                    <input type="hidden" name="doctor_id" value="<?php echo $doctorSingleChamberInfo->doctor_chamber_doctor_id_ref?>">
                                    <input type="hidden" name="user_id" value="<?php echo $userInfo->user_id?>">
                                    <input type="hidden" name="user_email" value="<?php echo $userInfo->user_email?>">
                                    <input type="hidden" name="user_phone" value="<?php echo $userInfo->user_phone?>">
                                    <input type="hidden" name="payment_method" value="1">
                                    <input type="hidden" name="tnxId" value="N/A">

                                    <input type="radio" class="custom-control-input" name="pay_mode" value="Cash">
                                    <label class="custom-control-label" for="cod">
                                        <img src="../../resources/images/payment_method/cod.png"
                                             alt="cash">
                                        <p>Cash</p>
                                    </label>

                                    <div class="payment-details" data-height="725">
                                        <h2>Your Payable Amount:
                                            <em><?php echo '100 Tk.'?></em>
                                        </h2>
                                        <h3>How to Use</h3>

                                        <ol>
                                            <li>Click on "Confirm" .</li>
                                        </ol>

                                        <p class="inconvenience-message">
                                            <i class="fa fa-microphone"></i>
                                            In that case, If you don't pay within 1hr.Then your appointment will cancel.
                                        </p>
                                        <p>
                                            <input type="checkbox" required> I agree to the <a target="_blank" href="#">Privacy Policy</a>
                                        </p>

                                        <div class="checkout-footer d-flex justify-content-end pr-3">
                                            <input class="btn btn-info" type="submit" value="Confirm Appointment">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>

                        <!-- Bkash -->
                        <section class="payment-option-item">
                            <div class="custom-control custom-checkbox">
                                <form action="payment_confirm.php" method="post">

                                    <input type="hidden" name="chamber_id" value="<?php echo $doctorSingleChamberInfo->doctor_chamber_id?>">
                                    <input type="hidden" name="doctor_id" value="<?php echo $doctorSingleChamberInfo->doctor_chamber_doctor_id_ref?>">
                                    <input type="hidden" name="user_id" value="<?php echo $userInfo->user_id?>">
                                    <input type="hidden" name="user_email" value="<?php echo $userInfo->user_email?>">
                                    <input type="hidden" name="user_phone" value="<?php echo $userInfo->user_phone?>">
                                    <input type="hidden" name="payment_method" value="2">

                                    <input type="radio" class="custom-control-input" name="pay_mode" value="bKash">
                                    <label class="custom-control-label" for="bkash">
                                        <img src="../../resources/images/payment_method/bkash.png"
                                             alt="Bkash Image">
                                        <p>bKash</p>
                                    </label>

                                    <div class="payment-details" data-height="1025">
                                        <h2>Your Payable Amount:
                                            <em><?php echo '100 Tk.'?></em>
                                        </h2>
                                        <h3>How to Pay</h3>

                                        <ol>
                                            <li>Step 1: Dial *247# .</li>
                                            <li>Step 2: Select Payment option 3 .</li>
                                            <li>Step 3: Write Merchant Account Number: <b>01841113003</b> .</li>
                                            <li>Step 4: Write Order Amount 1240</li>
                                            <li>Step 5: Write Order No. in Reference Box 2271245 .</li>
                                            <li>Step 6: Write Counter Number: 1 .</li>
                                            <li>Step 7: Write Four Digit Secret PIN (XXXX) .</li>
                                        </ol>

                                        <div class="form-group">
                                            <label for="trans-id">Now, You will get a Transaction ID through SMS.
                                                Please write down that.</label>
                                            <input type="text" name="tnxId" class="form-control" placeholder="Transaction Id">
                                        </div>

                                        <p class="inconvenience-message">
                                            <i class="fa fa-microphone"></i>
                                            In that case, If you don't pay within 1hr.Then your appointment will cancel.
                                        </p>
                                        <p><input type="checkbox" required> I agree to the <a target="_blank" href="#">Privacy Policy</a></p>

                                        <div class="checkout-footer d-flex justify-content-end pr-3">
                                            <input class="btn btn-info" type="submit" value="Confirm Appointment">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4">
                    <section id="checkout-sidebar">
                        <div class="checkout-sidebar__header">
                            <h1>Checkout Summary</h1>
                            <p><?php echo date("F j, Y")?></p>
                        </div>

                        <div class="checkout-sidebar__content">
                            <div class="payment-breakdown">
                                <h2>
                                    Payment Details
                                </h2>
                                <div class="payment-breakdown__content">
                                    <table class="table">
                                        <tbody>

                                        <tr>
                                            <td>Confirmation Payable</td>
                                            <td class="text-right" id="payable">
                                               100 TK. Only
                                            </td>
                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="user-address">
                                <h2>
                                    Doctor Information
                                </h2>
                                <p>
                                    <em>Doctor Name:</em><?php echo $doctorSingleChamberInfo->doctor_info_name?>
                                </p>
                                <p>
                                    <em>Chamber Address:</em><?php echo $doctorSingleChamberInfo->doctor_chamber_loc_details?>
                                </p>
                                <p>
                                    <em>Start In:</em><?php echo $doctorSingleChamberInfo->doctor_chamber_visiting_time_start?>
                                </p>
                                <p>
                                    <em>Total Payable:</em><?php echo $doctorSingleChamberInfo->doctor_chamber_visiting_fee?>
                                </p>
                            </div>
                            <div class="user-address">
                                <h2>
                                    Your Information
                                </h2>
                                <p>
                                    <em>User Id:</em><?php echo $userInfo->user_unique_id?>
                                </p>
                                <p>
                                    <em>Phone No.:</em><?php echo $userInfo->user_phone?>
                                </p>
                                <p>
                                    <em>Name:</em><?php echo $userInfo->user_name?>
                                </p>
                                <p>
                                    <em>Address: </em><?php echo $userInfo->user_address?>
                                </p>
                            </div>
                        </div>
                    </section>
                    <!-- End Checkout Sidebar -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<footer id="aa-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-footer-area">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="aa-footer-left">
                                <p>Designed by <a rel="nofollow" href="#">Doctor38</a></p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="aa-footer-middle">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="aa-footer-right">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- / Footer -->


<!-- jQuery library -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script src="../../resources/js/user/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="../../resources/js/user/slick.js"></script>
<!-- Price picker slider -->
<script type="text/javascript" src="../../resources/js/user/nouislider.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="../../resources/js/user/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="../../resources/js/user/jquery.fancybox.pack.js"></script>
<!-- Custom js -->
<script src="../../resources/js/user/custom.js"></script>

<!--Select2 use-->
<script src='http://eoffice.codeslab.net/assets/plugin/select2/select2.full.min.js'></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<!--Select2 use end-->

</body>
</html>