<?php
session_start();
require_once ("../../../vendor/autoload.php");


use App\Utility\Utility;
use App\Comments\Comments;


//Utility::dd($_SESSION);

if (!isset($_SESSION['u_id'])){
    Utility::redirect("../signup/signin.php");
}

else{
    $useId = trim($_POST['user_id']);
    $doctorId = trim($_POST['doctor_id']);
    $commentText = trim($_POST['comments']);

    $objComments= new Comments();
    $objComments->storeComments($useId,$doctorId,$commentText);

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}