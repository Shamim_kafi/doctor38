<?php
session_start();
require_once ("../../vendor/autoload.php");

require '../../vendor/phpmailer/phpmailer/src/Exception.php';
require '../../vendor/phpmailer/PHPMailer/src/PHPMailer.php';
require '../../vendor/phpmailer/PHPMailer/src/SMTP.php';

use App\Utility\Utility;
use App\Appointment\Appointment;
use App\DoctorChamber\DoctorChamber;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


if (!isset($_SESSION['u_id'])){
    Utility::redirect("../../index.php");
}

//Utility::dd($_POST);

$objChamber = new DoctorChamber();
$doctorSingleChamberInfo = $objChamber->doctorSingleChamberInfo($_POST['chamber_id']);

$objAppointment = new Appointment();
$lastSerial = $objAppointment->lastSerialNumber($_POST['doctor_id'],$_POST['chamber_id']);

//Utility::dd($lastSerial);

$userPhone = $_POST['user_phone'];

if (!empty($lastSerial)){

    $chamberStartTime = $doctorSingleChamberInfo->doctor_chamber_visiting_time_start;

    $invoiceId = generateInvoiceId();
    $serialNumber = $lastSerial->appoint_serial_num + 1;

    $extraMsg ='';

    if ($serialNumber <= 5){

        $chamberStartTimeConvertTimeFormat = strtotime("+45 minutes", strtotime($chamberStartTime));
        $endTime =  date('g:i A', $chamberStartTimeConvertTimeFormat);
        $extraMsg = "Please Come in $chamberStartTime to $endTime";
    }

    elseif ($serialNumber >5 && $serialNumber <=10){

        $chamberStartTimeConvertTimeFormat = strtotime("+45 minutes", strtotime($chamberStartTime));
        $startTime =  date('g:i A', $chamberStartTimeConvertTimeFormat);

        $endTimeFormat = strtotime("+45 minutes", strtotime($startTime));
        $endTime =  date('g:i A', $endTimeFormat);

        $extraMsg = " Please Come in $startTime to $endTime";
    }

    $objAppointment->storeAppointment($_POST['user_id'],$_POST['doctor_id'],$_POST['chamber_id'],$_POST['payment_method'],$invoiceId,$serialNumber,$_POST['tnxId']);

    $bodyMsg = 'Serial Number :'.$serialNumber.'<br>Doctor Name Dr.:'.$doctorSingleChamberInfo->doctor_info_name.'('.$doctorSingleChamberInfo->doctor_info_degree.')<br>Chamber Location:'.$doctorSingleChamberInfo->doctor_chamber_loc_details.'<br>'.$extraMsg;

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication

        $mail->Username = 'pro.event38@gmail.com';                 // SMTP username
        $mail->Password = 'projectevent38';
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($_POST['user_email']);
        $mail->addAddress($_POST['user_email']);     // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject =  'Dr.'.$doctorSingleChamberInfo->doctor_info_name." Appointment";
        $mail->Body    = $bodyMsg;

        $mail->send();
        //echo 'Message has been sent';
    }

    catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }

    /*Mobile msg*/
    $token = "289ded62c68135bfe5c7ca1c0fbe7f57";

    $message = 'Serial Number :'.$serialNumber.' Doctor Name : Dr.'.$doctorSingleChamberInfo->doctor_info_name.'('.$doctorSingleChamberInfo->doctor_info_degree.') Chamber Location:'.$doctorSingleChamberInfo->doctor_chamber_loc_details. $extraMsg.' Thanks for beings with us.';

    $url = "http://sms.greenweb.com.bd/api.php";


    $data= array(
        'to'=>"$userPhone",
        'message'=>"$message",
        'token'=>"$token"
    ); // Add parameters in key value
    $ch = curl_init(); // Initialize cURL
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $smsresult = curl_exec($ch);
    /*Mobile msg end*/


    $_SESSION['invoice_id'] = $invoiceId;
    Utility::redirect("payment_confirmation_msg.php");
}
else{

    $serialNumber = 1;
    $invoiceId=generateInvoiceId();
    $objAppointment->storeAppointment($_POST['user_id'],$_POST['doctor_id'],$_POST['chamber_id'],$_POST['payment_method'],$invoiceId,$serialNumber,$_POST['tnxId']);

    $chamberStartTime = $doctorSingleChamberInfo->doctor_chamber_visiting_time_start;
    $chamberStartTimeConvertTimeFormat = strtotime("-25 minutes", strtotime($chamberStartTime));
    $endTime =  date('g:i A', $chamberStartTimeConvertTimeFormat);
    $extraMsg = " Please Come in $chamberStartTime to $endTime";

    $bodyMsg = 'Serial Number :'.$serialNumber.'<br>Doctor Name : Dr.'.$doctorSingleChamberInfo->doctor_info_name.'('.$doctorSingleChamberInfo->doctor_info_degree.')<br>Chamber Location:'.$doctorSingleChamberInfo->doctor_chamber_loc_details. $extraMsg;

    $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication

        $mail->Username = 'pro.event38@gmail.com';                 // SMTP username
        $mail->Password = 'projectevent38';
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom($_POST['user_email']);
        $mail->addAddress($_POST['user_email']);     // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Dr.'.$doctorSingleChamberInfo->doctor_info_name."Appointment";
        $mail->Body    = $bodyMsg;

        $mail->send();
        //echo 'Message has been sent';
    }

    catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }


    /*Mobile msg*/
    $token = "289ded62c68135bfe5c7ca1c0fbe7f57";

    $message = 'Serial Number :'.$serialNumber.' Doctor Name Dr.:'.$doctorSingleChamberInfo->doctor_info_name.'('.$doctorSingleChamberInfo->doctor_info_degree.') Chamber Location:'.$doctorSingleChamberInfo->doctor_chamber_loc_details. $extraMsg.' Thank you for beings with us.';

    $url = "http://sms.greenweb.com.bd/api.php";


    $data= array(
        'to'=>"$userPhone",
        'message'=>"$message",
        'token'=>"$token"
    ); // Add parameters in key value
    $ch = curl_init(); // Initialize cURL
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $smsresult = curl_exec($ch);
    /*Mobile msg end*/

    $_SESSION['invoice_id'] = $invoiceId;
    Utility::redirect("payment_confirmation_msg.php");

}

//for generate invoice id
function generateInvoiceId($digits = 6){
    $i = 0; //counter
    $tpin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $tpin .= mt_rand(0, 9);
        $i++;
    }
    return 'AP'.$tpin;
}