<?php
session_start();
require_once ("../../vendor/autoload.php");

use App\DoctorType\DoctorType;
use App\Doctor\Doctor;
use App\Rating\Rating;

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

//for doctor list show by doctor type
$objDoctor = new Doctor();
$allDoctorListShow = $objDoctor->doctorListShowByDoctorType($_GET['doctor_type_id']);

//\App\Utility\Utility::dd($allDoctorListShow);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Doctor38 | Doctor List</title>

      <!-- Font awesome -->
      <link href="../../resources/awesome/css/font-awesome.css" rel="stylesheet">
      <!-- Bootstrap -->
      <link href="../../resources/bootstrap/css/bootstrap.css" rel="stylesheet">
      <!-- slick slider -->
      <link rel="stylesheet" type="text/css" href="../../resources/css/user/slick.css">
      <!-- price picker slider -->
      <link rel="stylesheet" type="text/css" href="../../resources/css/user/nouislider.css">
      <!-- Fancybox slider -->
      <link rel="stylesheet" href="../../resources/css/user/jquery.fancybox.css" type="text/css" media="screen" />
      <!-- Theme color -->
      <link id="switcher" href="../../resources/css/user/theme-color/default-theme.css" rel="stylesheet">

      <!-- Main style sheet -->
      <link href="../../resources/css/user/style.css" rel="stylesheet">

      <!--for rating-->
      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
      <link href='RatingRes/fontawesome-stars.css' rel='stylesheet' type='text/css'>

      <!-- Google Font -->
      <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


      <script src="../../resources/js/user/jquery.min.js"></script>

      <!--for rating-->
      <script src="RatingRes/jquery.barrating.min.js" type="text/javascript"></script>

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <script type="text/javascript">
          $(function() {
              $('.rating').barrating({
                  theme: 'fontawesome-stars',
                  onSelect: function(value, text, event) {

                      // Get element id by data-id attribute
                      var el = this;
                      var el_id = el.$elem.data('id');


                      // rating was selected by a user
                      if (typeof(event) !== 'undefined') {

                          var split_id = el_id.split("_");

                          var postid = split_id[1];  // postid

                          var userId = $("#userId").val();

                          //alert(userId);

                          if (userId == ""){

                              alert('Please registration first');
                              window.location.reload();
                          }

                          else {

                              // AJAX Request
                              $.ajax({
                                  url: 'rating_ajax.php',
                                  type: 'post',
                                  data: {postid:postid,rating:value},
                                  dataType: 'json',
                                  success: function(data){
                                      // Update average
                                      var average = data['averageRating'];
                                      $('#avgrating_'+postid).text(average);
                                  }
                              });
                          }

                      }

                  }
              });
          });
      </script>
  

  </head>
  <body class="aa-price-range">   
  <!-- Pre Loader -->
  <div id="aa-preloader-area">
    <div class="pulse"></div>
  </div>
  <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Start header section -->
  <header id="aa-header">  
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-header-area">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="aa-header-left">
                  <div class="aa-telephone-no">
                    <span class="fa fa-phone"></span>
                      880-01837210137
                  </div>
                  <div class="aa-email hidden-xs">
                    <span class="fa fa-envelope-o"></span> doctor38@gmail.com
                  </div>
                </div>              
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="aa-header-right">
                    <?php if (!isset($_SESSION['u_id'])) {
                        echo "
                                        <a href=\"#\" class=\"aa-register\">Doctor</a>
                                        <a href=\"signup/signin.php\" class=\"aa-login\">User</a>
                                    ";
                    } else{
                        echo "
                                        <a style='color: red' href='logout/logout.php' class=\"aa-login\">Logout</a>
                                    ";
                    }?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- End header section -->
  <!-- Start menu section -->
  <section id="aa-menu-area">
      <nav class="navbar navbar-default main-navbar" role="navigation">
          <div class="container">
              <div class="navbar-header">
                  <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <!-- LOGO -->
                  <!-- Text based logo -->
                  <a class="navbar-brand aa-logo" href="../../index.php"> Doctor <span>38</span></a>
                  <!-- Image based logo -->
                  <!-- <a class="navbar-brand aa-logo-img" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
              </div>
              <div id="navbar" class="navbar-collapse collapse">
                  <ul id="top-menu" class="nav navbar-nav navbar-right aa-main-nav">
                      <li><a href="../../index.php">HOME</a></li>
                      <li class="dropdown">
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#">DOCTORS <span class="caret"></span></a>
                          <ul class="dropdown-menu" role="menu">

                              <?php
                              foreach ($allDoctorType as $doctorType){
                                  echo "
                                     <li><a href='doctor_profile_view.php?doctor_type_id=$doctorType->doctor_type_id'>$doctorType->doctor_type_name</a></li>
                                    ";
                              }
                              ?>

                          </ul>
                      </li>
                      <li class="dropdown">
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#">BLOG <span class="caret"></span></a>
                          <ul class="dropdown-menu" role="menu">
                              <li><a href="#">ARTICLES</a></li>
                              <li><a href="#">TIPS</a></li>
                          </ul>
                      </li>
                      <li><a href="#">CONTACT</a></li>
                  </ul>
              </div><!--/.nav-collapse -->
          </div>
      </nav>
  </section>
  <!-- End menu section -->

  <!-- Start Proerty header  -->

  <section id="aa-property-header">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-property-header-inner">
            <h2>Doctors Page</h2>
            <ol class="breadcrumb">
            <li><a href="../../index.php">HOME</a></li>
            <li class="active">DOCTORS</li>
          </ol>
          </div>
        </div>
      </div>
    </div>
  </section> 
  <!-- End Proerty header  -->

  <!-- Start Properties  -->
  <section id="aa-properties">
    <div class="container">
      <div class="row">
        <div class="col-md-10">
          <div class="aa-properties-content">
            <!-- start properties content head -->
            <div class="aa-properties-content-head">              
              <div class="aa-properties-content-head-left">
                <form action="" class="aa-sort-form">
                  <label for="">Sort by</label>
                  <select name="">
                    <option value="1" selected="Default">Default</option>
                    <option value="2">Name</option>
                  </select>
                </form>
                <form action="" class="aa-show-form">
                  <label for="">Show</label>
                  <select name="">
                    <option value="1" selected="12">6</option>
                    <option value="2">12</option>
                    <option value="3">24</option>
                  </select>
                </form>
              </div>
              <div class="aa-properties-content-head-right">
                <a id="aa-grid-properties" href="#"><span class="fa fa-th"></span></a>
                <a id="aa-list-properties" href="#"><span class="fa fa-list"></span></a>
              </div>            
            </div>

            <!-- Start doctors content body -->
            <div class="aa-properties-content-body">
              <ul class="aa-properties-nav">
                  <?php
                  foreach ($allDoctorListShow as $doctorListShow){
                      $details = substr("$doctorListShow->doctor_info_details",0,150)." ....";

                      //for rating point
                      $objRating = new Rating();
                      $singleRatingInfo = $objRating->singleDoctorRatingInfo($doctorListShow->doctor_info_id);

                      $singleAverageRatingInfo = $objRating->singleDoctorAverageInfo($doctorListShow->doctor_info_id);
                      $averageRating = $singleAverageRatingInfo->averageRating;
                      if($averageRating <= 0){
                          $averageRating = "No rating yet.";
                      }
                  ?>
                      
                      <li>
                          <article class="aa-properties-item">
                              <a class="aa-properties-item-img" href="#">
                                  <img alt="img" src='../../resources/images/doctor/<?php echo $doctorListShow->doctor_info_image ?>'>
                              </a>
                             <div class="aa-tag for-rent">
                                Verified
                             </div>
                             <div class="aa-properties-item-content">
                                <div class="aa-properties-info">
                                    <span class='fa fa-info-circle'></span>
                                    <span><?php echo $doctorListShow->doctor_info_degree ?></span>

                                    <!-- Rating -->
                                    <div class="post-action">
                                        <select class='rating' id='rating_<?php echo $doctorListShow->doctor_info_id?>' data-id='rating_<?php echo $doctorListShow->doctor_info_id?>'>
                                            <option value="1" >1</option>
                                            <option value="2" >2</option>
                                            <option value="3" >3</option>
                                            <option value="4" >4</option>
                                            <option value="5" >5</option>
                                        </select>
                                        <div style='clear: both;'></div>
                                        Average Rating : <span id='avgrating_<?php echo $doctorListShow->doctor_info_id?>'><?php echo $averageRating; ?></span>

                                        <?php if (isset($_SESSION['u_id'])) { ?>
                                            <input type="hidden" id="userId" name="user_id" value="<?php echo $_SESSION['u_id']?>">
                                        <?php } else{?>
                                            <input type="hidden" id="userId" name="user_id" value="">
                                        <?php }?>
                                        <!-- Set rating -->
                                        <script type='text/javascript'>
                                        $(document).ready(function(){
                                            $('#rating_<?php echo $doctorListShow->doctor_info_id?>').barrating('set',<?php echo $singleRatingInfo->rating_point; ?>);
                                        });
                                        </script>
                                    </div>
                                    <!--End Rating -->

                                </div>
                                <div class="aa-properties-about">
                                   <h3><a href="#"><?php echo $doctorListShow->doctor_info_name?></a></h3>
                                   <p><?php echo $details ?></p>
                                </div>
                                <div class="aa-properties-detial">
                                   <a href='doctor_profile_details.php?doctor_id=<?php echo $doctorListShow->doctor_info_id?>' class="aa-secondary-btn">View Details</a>
                                </div>
                             </div>
                          </article>
                      </li>

                      <?php } ?>
              </ul>
            </div>
            <!-- Start doctor list content bottom -->
            <!--<div class="aa-properties-content-bottom">
              <nav>
                <ul class="pagination">
                  <li>
                    <a href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li class="active"><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li>
                    <a href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>-->
          </div>
        </div>
        <!-- Start properties sidebar -->
        <div class="col-md-4">

        </div>
      </div>
    </div>
  </section>
  <!-- / Properties  -->

  <!-- Footer -->
  <footer id="aa-footer">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="aa-footer-area">
                      <div class="row">
                          <div class="col-md-3 col-sm-6 col-xs-12">
                              <div class="aa-footer-left">
                                  <p>Designed by <a rel="nofollow" href="#">Doctor38</a></p>
                              </div>
                          </div>
                          <div class="col-md-3 col-sm-6 col-xs-12">
                              <div class="aa-footer-middle">
                                  <a href="#"><i class="fa fa-facebook"></i></a>
                                  <a href="#"><i class="fa fa-twitter"></i></a>
                                  <a href="#"><i class="fa fa-google-plus"></i></a>
                                  <a href="#"><i class="fa fa-youtube"></i></a>
                              </div>
                          </div>
                          <div class="col-md-6 col-sm-12 col-xs-12">
                              <div class="aa-footer-right">

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </footer>
  <!-- / Footer -->


  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->


  <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
  <!-- slick slider -->
  <script type="text/javascript" src="../../resources/js/user/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="../../resources/js/user/nouislider.js"></script>
  <!-- mixit slider -->
  <script type="text/javascript" src="../../resources/js/user/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->
  <script type="text/javascript" src="../../resources/js/user/jquery.fancybox.pack.js"></script>
  <!-- Custom js -->
  <script src="../../resources/js/user/custom.js"></script>

  </body>
</html>