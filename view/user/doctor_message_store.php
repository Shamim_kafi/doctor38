<?php
session_start();
require_once ("../../vendor/autoload.php");


use App\Utility\Utility;
use App\User\User;
use App\MessageBox\MessageBox;

//Utility::dd($_SESSION);

if (!isset($_SESSION['u_id'])){
    Utility::redirect("signup/signin.php");
}

else{
    $_POST['msg_user_id'] = $_SESSION['u_id'];
    $_POST['msg_admin_id'] = 0;

    $objMessageBox = new MessageBox();
    $objMessageBox->setMsgData($_POST);
    $objMessageBox->storeMsg();

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}