<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;

if (!isset($_SESSION['doctor_id']) && empty($_SESSION['doctor_id'])){

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

$objDoctorInfo = new Doctor();
$objDoctorInfo->setDoctorData($_POST);
$objDoctorInfo->singleDoctorInfoUpdate();

return Utility::redirect($_SERVER['HTTP_REFERER']);