<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Doctor\Doctor;
use App\Message\Message;


$objDoctor = new Doctor();

$imgDoctorName = time().$_FILES['doctor_img']['name'];
$sourceImgOne = $_FILES['doctor_img']['tmp_name'];
$destinationImgOne = "../../../resources/images/doctor/".$imgDoctorName;

move_uploaded_file($sourceImgOne,$destinationImgOne);

$_POST['doctor_image_info'] =  $imgDoctorName;


$eventUniquePin=generatePIN();
$_POST['doctor_unique_id'] =  $eventUniquePin;

//Utility::dd($_POST);

$doctorPhone = $_POST['doctor_phone'];

$objDoctor->setDoctorData($_POST);
$objDoctor->doctorRegistration();

/*Mobile msg*/
$token = "289ded62c68135bfe5c7ca1c0fbe7f57";

$message = 'Sir, your request has been accepted! As soon as we contact with you. Thank you for beings with us.';

$url = "http://sms.greenweb.com.bd/api.php";


$data= array(
    'to'=>"$doctorPhone",
    'message'=>"$message",
    'token'=>"$token"
); // Add parameters in key value
$ch = curl_init(); // Initialize cURL
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$smsresult = curl_exec($ch);
/*Mobile msg end*/

Utility::redirect("signup.php");


function generatePIN($digits = 6){
    $i = 0; //counter
    $tpin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $tpin .= mt_rand(0, 9);
        $i++;
    }
    return 'DI'.$tpin;
}