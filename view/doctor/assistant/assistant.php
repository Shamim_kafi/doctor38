<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;
use App\DoctorType\DoctorType;
use App\Assistant\Assistant;
use App\DoctorChamber\DoctorChamber;

if (!isset($_SESSION['doctor_id']) && empty($_SESSION['doctor_id'])){

    Utility::redirect("../../../index.php");
}

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

$objDoctorInfo = new Doctor();
$doctorDetails = $objDoctorInfo->singleDoctorDetails($_SESSION['doctor_id']);

$objAssistant = new Assistant();
$allAssistants = $objAssistant->allSingleDoctorAssistant($_SESSION['doctor_id']);

$objChamber = new DoctorChamber();
$allChamberLists = $objChamber->allSingleDoctorUniqueChamberLocation($_SESSION['doctor_id']);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Assistant</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><b>Doctor38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Doctor Name: <a href="" class="btn btn-info"><b><?php echo $doctorDetails->doctor_info_name?></b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../index.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li class="active">
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../chamber/chamber.php"><span class="fa fa-location-arrow"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Chamber</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-money"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-archive"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-history"></i> Assistant Info</b></h2>
                </div>
                <div class="col-md-12">
                    <a class="btn btn-info" data-toggle="modal" data-target="#assistantModal">Add Assistant</a>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default btn-default" style="font-family: monospace;">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed table-hover text-center" id="dataTables-example">
                                    <thead>
                                    <tr class="btn-info text-center">
                                        <td><b>SL#</b></td>
                                        <td><b>ASSISTANT NAME</b></td>
                                        <td><b>ASSISTANT PHONE</b></td>
                                        <td><b>ASSISTANT CHAMBER</b></td>
                                        <td><b>ACTIVE STATUS</b></td>
                                        <td><b>INSERT DATETIME</b></td>
                                        <td><b>ACTION</b></td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    $sl = 1;
                                    foreach ($allAssistants as $allAssistant){

                                        if ($allAssistant->assistant_isActive == 1){
                                            $status = '<span class="label label-success">Success</span>';
                                        }else{
                                            $status = '<span class="label label-danger">Delete</span>';
                                        }

                                        echo "
                                    <tr>

                                        <td><b>$sl</b></td>
                                        <td class='text-center'>
                                             <b>$allAssistant->assistant_name</b></td>
                                             
                                        <td><b>$allAssistant->assistant_phone</b></td>
                                        <td><b>$allAssistant->doctor_chamber_loc_title</b></td>   
                                        
                                        <td>$status</td>
                                              
                                        <td><b>$allAssistant->assistant_insert_date</b></td>
                                      
                                      
                                        <td>                                 
                                           <b><a class='btn btn-info btn-sm' href='assistant_profile.php?assistant_id=$allAssistant->assistant_id'><span class='fa fa-edit'></span> Edit</a></b>                                        
                                           <b><a class='btn btn-danger btn-sm' href='#'><span class='fa fa-remove'></span> Delete</a></b>                                        
                                        </td>
                                    </tr>    
                                        ";
                                        $sl++;
                                    }

                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="btn-info text-center">

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <h2 class="text-center"><b>Total Assistant : <?php echo count($allAssistants)?></b></h2>
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <span class="btn pull-left"><b>Developed by @ Doctor38 Ltd.</b></span>
                    <span class="btn pull-right"><b>Copyright @ 2018</b></span>
                </div>
            </div>
        </div>
    </div>
    <!-- /. PAGE INNER  -->

    <!-- Modal add doctor info -->
    <div class="modal fade" id="assistantModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Assistant</h4>
                </div>

                <form id="form" action="assistant_store.php" method="post"  autocomplete="off" >
                    <div class="modal-body">

                        <table class="table table-bordered table-condensed text-left">
                            <thead>
                            <tr class="btn-info text-center">
                                <td colspan="2"><h3><b>Enter Right Information Below</b></h3></td>
                            </tr>

                            </thead>
                            <tbody>

                            <input type="hidden" name="doctor_id" value="<?php echo $_SESSION['doctor_id']?>">


                            <tr>
                                <td class="text-right text-info"><h5><b>Chamber Name</b></h5></td>
                                <td>
                                    <select class="form-control" style="width: 100%" name="chamber_id" required>
                                        <option selected >Select Chamber</option>
                                        <?php
                                        foreach ($allChamberLists as $chamber){
                                            echo "
                                            <option value='$chamber->doctor_chamber_id'>$chamber->doctor_chamber_loc_title</option>
                                    ";
                                        }
                                        ?>

                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Assistant Name</b></h5></td>
                                <td><input class="form-control" type="text" name="assistant_name" placeholder="Enter Assistant Name" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Assistant Phone</b></h5></td>
                                <td><input class="form-control" type="text" name="assistant_phone" placeholder="Enter Assistant Phone" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Assistant Password</b></h5></td>
                                <td><input class="form-control" type="text" name="assistant_pass" placeholder="Enter User Password" required="" /></td>
                            </tr>

                            </tbody>
                        </table>

                        <!-- End Form Elements -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit"  value="Save" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/js/user/jquery.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../../../resources/js/admin/jquery.dataTables.js"></script>
<script src="../../../resources/js/admin/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
    });
</script>
<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>


</body>
</html>

