<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Assistant\Assistant;
use App\Message\Message;


$objAssistant = new Assistant();
//Utility::dd($_POST);
$objAssistant->setDoctorAssistantData($_POST);
$objAssistant->storeDoctorAssistant();
Utility::redirect("assistant.php");