<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;
use App\DoctorType\DoctorType;
use App\Assistant\Assistant;
use App\DoctorChamber\DoctorChamber;

if (!isset($_SESSION['doctor_id']) && empty($_SESSION['doctor_id'])){

    Utility::redirect("../../../index.php");
}

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

$objDoctorInfo = new Doctor();
$doctorDetails = $objDoctorInfo->singleDoctorDetails($_SESSION['doctor_id']);

$objAssistant = new Assistant();
$assistantDetails = $objAssistant->singleAssistantDetails($_GET['assistant_id']);

$objChamber = new DoctorChamber();
$allChamberLists = $objChamber->allSingleDoctorUniqueChamberLocation($_SESSION['doctor_id']);

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Assistant</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><b>Doctor38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Doctor Name: <a href="" class="btn btn-info"><b><?php echo $doctorDetails->doctor_info_name?></b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../index.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li class="active">
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../chamber/chamber.php"><span class="fa fa-location-arrow"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Chamber</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-money"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-archive"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">

                <div class="col-md-12">
                    <!--  Message insert-->
                    <?php

                    $msg =Message::message();
                    if ($msg){
                        echo "
                           $msg
                           ";
                    }
                    ?>
                    <!-- Message insert end-->
                </div>

                <div class="col-md-12">
                    <form action="profile_update.php" method="post">

                        <input type="hidden" name="assistant_id" value="<?php echo $assistantDetails->assistant_id?>">

                        <table class="table table-bordered table-condensed text-left">
                            <thead>
                            <tr class="btn-info text-center">
                                <td colspan="2">
                                    <h3>
                                        Assistant <?php echo $assistantDetails->assistant_name?> Information</b>
                                    </h3>
                                </td>
                            </tr>

                            </thead>
                            <tbody>

                            <tr>
                                <td class="text-right text-info"><h5><b>Assistant Name :</b></h5></td>
                                <td><input class="form-control" type="text" name="assistant_name" value="<?php echo $assistantDetails->assistant_name?>"  /></td>
                            </tr>


                            <tr>
                                <td class="text-right text-info"><h5><b>Assistant Mobile :</b></h5></td>
                                <td><input class="form-control" type="text" name="assistant_phone" value="<?php echo $assistantDetails->assistant_phone?>" max="13" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Assistant Password :</b></h5></td>
                                <td><input class="form-control" type="password" name="assistant_pass" placeholder="Enter Password" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Active Status:</b></h5></td>
                                <td>
                                    <select class="form-control select2"  name="is_active" required>
                                        <option value="1" <?php if ($assistantDetails->assistant_isActive ==1) echo 'selected'?>>Active</option>
                                        <option value="0" <?php if ($assistantDetails->assistant_isActive ==0) echo 'selected'?>>Deactive</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" class="text-right">
                                    <a href="assistant_store.php" class="btn btn-danger pull-left"><b><i class="fa fa-reply-all"></i> Back</b></a>
                                    <button type="submit" class="btn btn-primary"><b><i class="fa fa-edit"></i> update</b></button>
                                </td>
                            </tr>



                            </tbody>

                            <tfoot>
                            <tr class="btn-danger text-center">
                                <td colspan="2"><b></b></td>
                            </tr>
                            </tfoot>
                        </table>
                    </form>
                </div>
            </div>

        </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="../../../resources/js/admin/raphael-2.1.0.min.js"></script>
<script src="../../../resources/js/admin/morris.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>


<!--For message-->
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
</script>
</body>
</html>
