<?php
session_start();
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\Doctor\Doctor;
use App\DoctorType\DoctorType;
use App\Blog\Blog;

if (!isset($_SESSION['doctor_id']) && empty($_SESSION['doctor_id'])){

    Utility::redirect("../../../index.php");
}

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

$objDoctorInfo = new Doctor();
$doctorDetails = $objDoctorInfo->singleDoctorDetails($_SESSION['doctor_id']);

$objBlog = new Blog();
$allBlogs = $objBlog->allSingleDoctorBlogList($_SESSION['doctor_id']);

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Blog Info</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />

    <link href='http://eoffice.codeslab.net/assets/plugin/select2/select2.min.css' rel='stylesheet' media='screen'>
</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><b>Doctor38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Doctor Name: <a href="" class="btn btn-info"><b><?php echo $doctorDetails->doctor_info_name?></b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../index.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li class="active">
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../chamber/chamber.php"><span class="fa fa-location-arrow"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Chamber</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-money"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-archive"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-history"></i> Blog Info</b></h2>
                </div>
                <div class="col-md-12">
                    <a class="btn btn-info" data-toggle="modal" data-target="#myModalBlog">Add Blog</a>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default btn-default" style="font-family: monospace;">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-condensed table-hover text-center" id="dataTables-example">
                                    <thead>
                                    <tr class="btn-info text-center">
                                        <td><b>SL#</b></td>
                                        <td><b>BLOG IMAGE</b></td>
                                        <td><b>BLOG TITLE</b></td>
                                        <td><b>BLOG DETAILS</b></td>
                                        <td><b>INSERT DATETIME</b></td>
                                        <td><b>ACTION</b></td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    $sl = 1;
                                    foreach ($allBlogs as $allBlog){
                                        $details = substr("$allBlog->blog_details",0,50)." ....";


                                        echo "
                                    <tr>

                                        <td><b>$sl</b></td>
                                        <td class='text-center'>
                                             <img class='img-responsive img-thumbnail' src='../../../resources/images/blog/$allBlog->blog_image' height='70' width='70' alt=''><br>
                                        </td>
                                        <td><b>$allBlog->blog_title</b></td>
                                        <td><b>$details</b></td>
                                
                                        <td><b>$allBlog->blog_post_datetime</b></td>
                                      
                                      
                                        <td>                                 
                                           <b><a class='btn btn-info btn-sm' href='view_blog_details.php?blog_id=$allBlog->blog_id'><span class='fa fa-remove'></span> View</a></b>                                        
                                           <b><a class='btn btn-danger btn-sm' href='#'><span class='fa fa-remove'></span> Delete</a></b>                                        
                                        </td>
                                    </tr>    
                                        ";
                                        $sl++;
                                    }

                                    ?>
                                    </tbody>
                                    <tfoot>
                                    <tr class="btn-info text-center">

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <h2 class="text-center"><b>Total Blog : <?php echo count($allBlogs)?></b></h2>
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <span class="btn pull-left"><b>Developed by @ Doctor38 Ltd.</b></span>
                    <span class="btn pull-right"><b>Copyright @ 2018</b></span>
                </div>
            </div>
        </div>
    </div>
    <!-- /. PAGE INNER  -->

    <!-- Modal add doctor info -->
    <div class="modal fade" id="myModalBlog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Blog</h4>
                </div>

                <form id="form" action="blog_store.php" method="post" enctype="multipart/form-data" autocomplete="off" >
                    <div class="modal-body">

                        <table class="table table-bordered table-condensed text-left">
                            <thead>
                            <tr class="btn-info text-center">
                                <td colspan="2"><h3><b>Enter Right Information Below</b></h3></td>
                            </tr>

                            </thead>
                            <tbody>

                            <input type="hidden" name="doctor_id" value="<?php echo $_SESSION['doctor_id']?>">
                            <input type="hidden" name="admin_id" value="0">

                            <tr>
                                <td class="text-right text-info"><h5><b>Blog Image(360*240) :</b></h5></td>
                                <td><input class="form-control" type="file" name="blog_img" required=""/></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Blog Title :</b></h5></td>
                                <td><input class="form-control" type="text" name="blog_title" placeholder="Enter User Name" required="" /></td>
                            </tr>

                            <tr>
                                <td class="text-right text-info"><h5><b>Blog Details :</b></h5></td>
                                <td>
                                    <textarea class="form-control" name="blog_details" id="" cols="30" rows="7" placeholder="Details"></textarea>
                                </td>
                            </tr>

                            </tbody>
                        </table>

                        <!-- End Form Elements -->

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <input type="submit"  value="Save changes" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<!--Select2 use-->
<script src='http://eoffice.codeslab.net/assets/plugin/select2/select2.full.min.js'></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<!--Select2 use end-->
<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/js/user/jquery.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>
<!-- DATA TABLE SCRIPTS -->
<script src="../../../resources/js/admin/jquery.dataTables.js"></script>
<script src="../../../resources/js/admin/dataTables.bootstrap.js"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').dataTable();
    });
</script>
<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>


</body>
</html>

