<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Blog\Blog;
use App\Message\Message;


$objBlog = new Blog();

$imgBlogName = time().$_FILES['blog_img']['name'];
$sourceImg = $_FILES['blog_img']['tmp_name'];
$destinationImgOne = "../../../resources/images/blog/".$imgBlogName;

move_uploaded_file($sourceImg,$destinationImgOne);

$_POST['blog_image'] =  $imgBlogName;


//Utility::dd($_POST);

$objBlog->setData($_POST);
$objBlog->storeBlog();
Utility::redirect("blog.php");