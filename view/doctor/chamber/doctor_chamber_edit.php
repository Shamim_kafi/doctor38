<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;
use App\Doctor\Doctor;
use App\DoctorChamber\DoctorChamber;


if (!isset($_SESSION['doctor_id']) && empty($_SESSION['doctor_id'])){

    return Utility::redirect($_SERVER['HTTP_REFERER']);
}

$objDoctorInfo = new Doctor();
$doctorDetails = $objDoctorInfo->singleDoctorDetails($_SESSION['doctor_id']);

//for chamber
$objDoctorChamber = new DoctorChamber();
$singleDoctorChamberInfo = $objDoctorChamber->singleDoctorChamberInfo(($_GET['doctor_chamber_id']));



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Chamber Edit</title>
    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <link href="../../../resources/css/admin/button_style.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../../../resources/plugin/timepicker/jquery.timepicker.min.css" rel="stylesheet" />
</head>
<body onload="DisableAutoComp();">
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><b>DOCTOR38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Doctor Name: <a href="" class="btn btn-info"><b><?php echo $doctorDetails->doctor_info_name?></b></a></b>
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../index.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li class="active">
                    <a href="#"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="chamber.php"><span class="fa fa-location-arrow"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Chamber</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-money"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-archive"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="../message/message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>


    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-edit"></i> Edit Doctor Chamber</b></h2>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <!--  Message insert-->
                    <?php

                    $msg =Message::message();
                    if ($msg){
                        echo "
                           $msg
                           ";
                    }
                    ?>
                    <!-- Message insert end-->
                    <!-- Form Elements -->
                    <div class="panel panel-default btn-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form id="form" action="doctor_chamber_update.php" method="post" autocomplete="off" >

                                        <table class="table table-bordered table-condensed text-left">
                                            <thead>
                                            <tr class="btn-info text-center">
                                                <td colspan="2"><h3><b>Enter Right Information Below</b></h3></td>
                                            </tr>

                                            </thead>
                                            <tbody>
                                            <input type="hidden" name="doctor_chamber_id" value="<?php echo $_GET['doctor_chamber_id']?>">
                                            <tr>
                                                <td class="text-right text-info"><h5><b>Chamber Address Title :</b></h5></td>
                                                <td><b>
                                                        <select class="form-control select2" style="width: 100%" name="location_head_name" required>
                                                            <option selected >Please Select Head Name...</option>
                                                            <option value="GEC" <?php if ($singleDoctorChamberInfo->doctor_chamber_loc_title == 'GEC') echo "selected "?>>GEC</option>
                                                            <option value="Agrabad" <?php if ($singleDoctorChamberInfo->doctor_chamber_loc_title == 'Agrabad') echo "selected "?>>Agrabad</option>
                                                            <option value="Chawkbazar" <?php if ($singleDoctorChamberInfo->doctor_chamber_loc_title == 'Chawkbazar') echo "selected "?>>Chawkbazar</option>
                                                            <option value="Provortok" <?php if ($singleDoctorChamberInfo->doctor_chamber_loc_title == 'Provortok') echo "selected "?>>Provortok</option>
                                                            <option value="2 Number Gate" <?php if ($singleDoctorChamberInfo->doctor_chamber_loc_title == '2 Number Gate') echo "selected "?>>2 Number Gate</option>
                                                            <option value="Muradpur" <?php if ($singleDoctorChamberInfo->doctor_chamber_loc_title == 'Muradpur') echo "selected "?>>Muradpur</option>
                                                            <option value="Andorkilla" <?php if ($singleDoctorChamberInfo->doctor_chamber_loc_title == 'Andorkilla') echo "selected "?>>Andorkilla</option>
                                                        </select>
                                                    </b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Chamber Address Details:</b></h5></td>
                                                <td>
                                                    <textarea class="form-control" name="chamber_address" id="" cols="30" rows="5"><?php echo $singleDoctorChamberInfo->doctor_chamber_loc_details?></textarea>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Day :</b></h5></td>
                                                <td><b>
                                                        <select class="form-control select2" style="width: 100%" name="visiting_day" required>
                                                            <option selected >Please Select Day...</option>
                                                            <option value="Saturday" <?php if ($singleDoctorChamberInfo->doctor_chamber_visiting_day == 'Saturday') echo "selected "?>>Saturday</option>
                                                            <option value="Sunday" <?php if ($singleDoctorChamberInfo->doctor_chamber_visiting_day == 'Sunday') echo "selected "?>>Sunday</option>
                                                            <option value="Monday" <?php if ($singleDoctorChamberInfo->doctor_chamber_visiting_day == 'Monday') echo "selected "?>>Monday</option>
                                                            <option value="Thursday" <?php if ($singleDoctorChamberInfo->doctor_chamber_visiting_day == 'Thursday') echo "selected "?>>Thursday</option>
                                                            <option value="Wednesday" <?php if ($singleDoctorChamberInfo->doctor_chamber_visiting_day == 'Wednesday') echo "selected "?>>Wednesday</option>
                                                            <option value="Tuesday" <?php if ($singleDoctorChamberInfo->doctor_chamber_visiting_day == 'Tuesday') echo "selected "?>>Tuesday</option>
                                                            <option value="Friday" <?php if ($singleDoctorChamberInfo->doctor_chamber_visiting_day == 'Friday') echo "selected "?>>Friday</option>

                                                        </select>
                                                    </b>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Visiting Time Start:</b></h5></td>
                                                <td><input class="form-control" type="text" name="visiting_time_start" id="startTime" value="<?php echo $singleDoctorChamberInfo->doctor_chamber_visiting_time_start?>" required="" /></td>
                                            </tr>

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Visiting Time End:</b></h5></td>
                                                <td><input class="form-control" type="text" name="visiting_time_end" id="endTime" value="<?php echo $singleDoctorChamberInfo->doctor_chamber_visiting_time_end?>" required="" /></td>
                                            </tr>

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Consultancy Fee :</b></h5></td>
                                                <td><input class="form-control" type="number" name="consultancy_fee" value="<?php echo $singleDoctorChamberInfo->doctor_chamber_visiting_fee?>" min="50" max="1000000" required="" /></td>
                                            </tr>

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Total Patient View Daile :</b></h5></td>
                                                <td><input class="form-control" type="number" name="total_patient_view_daily" value="<?php echo $singleDoctorChamberInfo->doctor_chamber_total_patient?>" min="10" max="300" required="" /></td>
                                            </tr>

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Serial Number Contact :</b></h5></td>
                                                <td><input class="form-control" type="text" name="serial_number_contact" value="<?php echo $singleDoctorChamberInfo->doctor_chamber_contact_serial_num?>" required="" /></td>
                                            </tr>

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Is Active :</b></h5></td>
                                                <td><b>
                                                        <select class="form-control" style="width: 100%" name="is_active" required>
                                                            <option value="1" <?php if ($singleDoctorChamberInfo->doctor_chamber_verify == 1) echo "selected "?>>Yes</option>
                                                            <option value="0" <?php if ($singleDoctorChamberInfo->doctor_chamber_verify == 0) echo "selected "?>>No</option>
                                                        </select>
                                                    </b>
                                                </td>
                                            </tr>\
                                            <tr>
                                                <td colspan="2" class="text-right">
                                                    <a href="chamber.php" class="btn btn-danger pull-left"><b><i class="fa fa-reply-all"></i> Back</b></a>
                                                    <button type="submit" class="btn btn-primary"><b><i class="fa fa-check-circle"></i> Update Chamber</b></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr class="btn-info text-center">
                                                <td colspan="2"><b></b></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Elements -->
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <span class="btn pull-left"><b>Developed by @ Doctor38 Ltd.</b></span>
                    <span class="btn pull-right"><b>Copyright @ 2019</b></span>
                </div>
            </div>    </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>



<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>

<!-- CUSTOM SCRIPTS -->
<script src="../../../resources/js/admin/custom.js"></script>

<!-- bootstrap time picker -->
<script src="../../../resources/plugin/timepicker/jquery.timepicker.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#startTime').timepicker();
    });

    $(document).ready(function(){
        $('#endTime').timepicker();
    });
</script>

<!--For message-->
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
</script>
</body>
</html>
