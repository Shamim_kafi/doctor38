<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Appointment\Appointment;

$objAppoint = new Appointment();
$objAppoint->appointIsDelete($_GET['appoint_id']);

return Utility::redirect($_SERVER['HTTP_REFERER']);

