<?php
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Appointment\Appointment;
use App\DoctorChamber\DoctorChamber;
use App\Message\Message;


//Utility::dd($_POST);

$objChamber = new DoctorChamber();
$doctorSingleChamberInfo = $objChamber->doctorSingleChamberInfo($_POST['chamber_id']);

$objAppointment = new Appointment();
$lastSerial = $objAppointment->lastSerialNumber($_POST['doctor_id'],$_POST['chamber_id']);

//Utility::dd($lastSerial);


if (!empty($lastSerial)){

    $chamberStartTime = $doctorSingleChamberInfo->doctor_chamber_visiting_time_start;

    $invoiceId = generateInvoiceId();
    $serialNumber = $lastSerial->appoint_serial_num + 1;


    $objAppointment->storeAppointment($_POST['user_id'],$_POST['doctor_id'],$_POST['chamber_id'],$_POST['payment_method'],$invoiceId,$serialNumber,$_POST['tnxId']);

    Message::message("<div class='alert alert-success' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Successfully!</strong> Serial Num : $serialNumber (Emergency) has been stored!
                              </div>");

    Utility::redirect("appointment_list.php");
}
else{

    $serialNumber = 1;
    $invoiceId=generateInvoiceId();
    $objAppointment->storeAppointment($_POST['user_id'],$_POST['doctor_id'],$_POST['chamber_id'],$_POST['payment_method'],$invoiceId,$serialNumber,$_POST['tnxId']);

    Message::message("<div class='alert alert-success' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Successfully!</strong> Serial Num : $serialNumber (Emergency) has been stored!
                              </div>");

    Utility::redirect("appointment_list.php");

}

//for generate invoice id
function generateInvoiceId($digits = 6){
    $i = 0; //counter
    $tpin = ""; //our default pin is blank.
    while($i < $digits){
        //generate a random number between 0 and 9.
        $tpin .= mt_rand(0, 9);
        $i++;
    }
    return 'AP'.$tpin;
}