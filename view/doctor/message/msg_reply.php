<?php
session_start();
require_once ("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\MessageBox\MessageBox;
use App\Doctor\Doctor;
use App\DoctorType\DoctorType;

if (!isset($_SESSION['doctor_id']) && empty($_SESSION['doctor_id'])){

    Utility::redirect("../../../index.php");
}

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

$objDoctorInfo = new Doctor();
$doctorDetails = $objDoctorInfo->singleDoctorDetails($_SESSION['doctor_id']);


//for message history
$objMessageBox = new MessageBox();
$objMessageBox->msgIsSeen($_GET['msg_id']);
$allMessageList = $objMessageBox->singleDoctorAllMessage($_SESSION['doctor_id']);



?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Doctor38 | Message Box</title>

    <!-- BOOTSTRAP STYLES-->
    <link href="../../../resources/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLES-->
    <link href="../../../resources/awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="../../../resources/css/admin/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="../../../resources/css/admin/dataTables.bootstrap.css" rel="stylesheet" />

    <link href='http://eoffice.codeslab.net/assets/plugin/select2/select2.min.css' rel='stylesheet' media='screen'>

    <!--Select2 use-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body>
<div id="wrapper">

    <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../index.php"><b>Doctor38</b></a>
        </div>
        <div style="color: white;padding: 15px 50px 5px 50px;float: right;font-size: 16px;">
            <b>Doctor Name: <a href="" class="btn btn-info"><b><?php echo $doctorDetails->doctor_info_name?></b></a></b>&nbsp;
            <a href="../logout.php" class="btn btn-danger square-btn-adjust"><b>Logout</b></a>
        </div>
    </nav>
    <!-- /. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a href="../index.php"><span class="fa fa-dashboard"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Dashboard</b></a>
                </li>

                <li class="active">
                    <a href="../profile/doctor_profile.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Profile</b></a>
                </li>

                <li>
                    <a href="../chamber/chamber.php"><span class="fa fa-location-arrow"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Chamber</b></a>
                </li>

                <li>
                    <a href="../appointment/appointment_list.php"><span class="fa fa-user"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Appointment</b></a>
                </li>

                <li>
                    <a href="../transaction/transaction_history.php"><span class="fa fa-money"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Transaction</b></a>
                </li>

                <li>
                    <a href="../blog/blog.php"><span class="fa fa-archive"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Blog</b></a>
                </li>

                <li>
                    <a href="message.php"><span class="fa fa-envelope"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Message</b></a>
                </li>

                <li>
                    <a href="../logout.php"><span class="fa fa-lock"></span><b>&nbsp;&nbsp;&nbsp;&nbsp;Logout</b></a>
                </li>
            </ul>
        </div>
    </nav>


    <div id="page-wrapper" >
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2><b><i class="fa fa-send"></i> Message</b></h2>
                </div>
            </div>
            <!-- /. ROW  -->
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <!--  Message insert-->
                    <?php

                    $msg =Message::message();
                    if ($msg){
                        echo "
                           $msg
                           ";
                    }
                    ?>
                    <!-- Message insert end-->
                    <!-- Form Elements -->
                    <div class="panel panel-default btn-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <form id="form" action="msg_reply_store.php" method="post" autocomplete="off" >
                                        <table class="table table-bordered table-condensed text-left">
                                            <thead>
                                            <tr class="btn-danger text-center">
                                                <td colspan="2"><h3><b>Enter Information Below</b></h3></td>
                                            </tr>

                                            </thead>
                                            <tbody>

                                            <input type="hidden" name="msg_doctor_id" value="<?php echo $_SESSION['doctor_id']?>">
                                            <input type="hidden" name="msg_user_id" value="<?php echo $_GET['user_id']?>">
                                            <input type="hidden" name="msg_admin_id" value="<?php echo $_GET['admin_id']?>">
                                            <input type="hidden" name="msg_send" value="1">

                                            <tr>
                                                <td class="text-right text-info"><h5><b>Message :</b></h5></td>
                                                <td><b>
                                                        <textarea name="msg_details" class="form-control" placeholder="Write your message...."></textarea>
                                                    </b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-right">
                                                    <a href="message.php" class="btn btn-danger pull-left"><b><i class="fa fa-reply-all"></i> Back</b></a>
                                                    <button type="submit" class="btn btn-primary"><b><i class="fa fa-send"></i> Send</b></button>
                                                </td>
                                            </tr>
                                            </tbody>
                                            <tfoot>
                                            <tr class="btn-danger text-center">
                                                <td colspan="2"><b></b></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Form Elements -->
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <span class="btn pull-left"><b>Developed by @ Doctor38 Ltd.</b></span>
                    <span class="btn pull-right"><b>Copyright @ 2018</b></span>
                </div>
            </div>    </div>
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->
</div>

<!-- /. PAGE WRAPPER  -->
<!-- /. WRAPPER  -->
<!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
<!-- JQUERY SCRIPTS -->
<script src="../../../resources/jQuery/jquery-3.1.1.min.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../../../resources/bootstrap/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS(Menu Drop Down) -->
<script src="../../../resources/js/admin/jquery.metisMenu.js"></script>

<!--For message-->
<script>
    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
    }, 4000);
</script>

</body>
</html>
