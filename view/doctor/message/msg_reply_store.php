<?php
require_once("../../../vendor/autoload.php");

use App\Utility\Utility;
use App\Message\Message;
use App\MessageBox\MessageBox;

//Utility::dd($_POST);

$objMsgBox =  new MessageBox();
$objMsgBox->setMsgData($_POST);
$objMsgBox->storeMsg();
Message::message("<div class='alert alert-success' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Nice!</strong> Message is sent!
                              </div>");
return Utility::redirect($_SERVER['HTTP_REFERER']);


