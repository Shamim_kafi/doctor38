-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2019 at 04:34 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `doctor38`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_info`
--

CREATE TABLE `tbl_admin_info` (
  `a_id` int(11) NOT NULL,
  `a_name` varchar(255) NOT NULL,
  `a_contact` varchar(255) NOT NULL,
  `a_email` varchar(255) NOT NULL,
  `a_password` varchar(255) NOT NULL,
  `a_type` int(11) NOT NULL,
  `a_reg_date` varchar(255) NOT NULL,
  `a_reg_time` varchar(255) NOT NULL,
  `a_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin_info`
--

INSERT INTO `tbl_admin_info` (`a_id`, `a_name`, `a_contact`, `a_email`, `a_password`, `a_type`, `a_reg_date`, `a_reg_time`, `a_status`) VALUES
(1, 'Event38', '01837210137', 'doctor38@gmail.com', '12345', 1, '09-02-18', '12:45AM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin_type`
--

CREATE TABLE `tbl_admin_type` (
  `at_id` int(11) NOT NULL,
  `at_type` varchar(11) NOT NULL,
  `at_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin_type`
--

INSERT INTO `tbl_admin_type` (`at_id`, `at_type`, `at_status`) VALUES
(1, 'Super Admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_blog`
--

CREATE TABLE `tbl_blog` (
  `blog_id` int(11) NOT NULL,
  `blog_title` varchar(1000) NOT NULL,
  `blog_details` text NOT NULL,
  `blog_image` varchar(1000) NOT NULL,
  `blog_admin_ref` int(11) NOT NULL,
  `blog_doctor_ref` int(11) NOT NULL,
  `blog_post_datetime` varchar(100) NOT NULL,
  `blog_is_verify` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_blog`
--

INSERT INTO `tbl_blog` (`blog_id`, `blog_title`, `blog_details`, `blog_image`, `blog_admin_ref`, `blog_doctor_ref`, `blog_post_datetime`, `blog_is_verify`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio est quaerat magnam exercitationem voluptas, voluptatem sed quam ab laborum voluptatum tempore dolores itaque.', '1553587650news1.jpg', 1, 0, '2019-03-26 14:03:30', 1),
(2, 'Multiple abscesses in the lower extremities caused by Trichophyton rubrum', 'Dermatophytes occasionally invade the dermis, subcutaneous tissues, and internal organs, resulting in a condition called deep dermatophytosi.\r\n\r\n', '1553600141news1.jpg', 0, 5, '2019-03-26 17:03:41', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comments`
--

CREATE TABLE `tbl_comments` (
  `comm_id` int(11) NOT NULL,
  `comm_user_id` int(11) NOT NULL,
  `comm_doctor_id` int(11) NOT NULL,
  `comm_comments` text NOT NULL,
  `comm_datetime` varchar(100) NOT NULL,
  `comm_isseen` int(11) NOT NULL DEFAULT '0',
  `comm_isDeleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_comments`
--

INSERT INTO `tbl_comments` (`comm_id`, `comm_user_id`, `comm_doctor_id`, `comm_comments`, `comm_datetime`, `comm_isseen`, `comm_isDeleted`) VALUES
(1, 3, 1, 'Hi', '2019/04/18 06:00:40 pm', 0, 0),
(2, 2, 1, 'Best Doctor', '2019/04/18 06:28:46 pm', 0, 0),
(3, 2, 1, 'Nice', '2019/04/22 07:53:57 am', 0, 0),
(4, 3, 1, 'Good', '2019/04/22 03:51:13 pm', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deposit_history`
--

CREATE TABLE `tbl_deposit_history` (
  `deposit_id` int(11) NOT NULL,
  `deposit_type_id_ref` int(11) NOT NULL,
  `deposit_transaction_id` varchar(100) NOT NULL,
  `deposit_user_id_ref` int(11) NOT NULL,
  `deposit_doctor_id_ref` int(11) NOT NULL,
  `deposit_appoint_id_ref` int(11) NOT NULL,
  `deposit_chamber_id_ref` int(11) NOT NULL,
  `deposit_method_id_ref` int(11) NOT NULL,
  `deposit_amount` double(13,2) NOT NULL,
  `deposit_insertdate` varchar(100) NOT NULL,
  `deposit_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_deposit_history`
--

INSERT INTO `tbl_deposit_history` (`deposit_id`, `deposit_type_id_ref`, `deposit_transaction_id`, `deposit_user_id_ref`, `deposit_doctor_id_ref`, `deposit_appoint_id_ref`, `deposit_chamber_id_ref`, `deposit_method_id_ref`, `deposit_amount`, `deposit_insertdate`, `deposit_status`) VALUES
(1, 1, 'D5C8BE8F21C24F', 2, 1, 3, 2, 1, 100.00, '2019-03-16 00:03:30', 1),
(2, 1, 'D5CC297EA7C045', 1, 1, 10, 2, 1, 100.00, '2019-04-26 11:04:26', 1),
(3, 1, 'D5CC312B976F37', 1, 1, 12, 2, 1, 100.00, '2019-04-26 20:04:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_deposit_type`
--

CREATE TABLE `tbl_deposit_type` (
  `deposit_type_id` int(11) NOT NULL,
  `deposit_type_name` varchar(100) NOT NULL,
  `deposit_type_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_deposit_type`
--

INSERT INTO `tbl_deposit_type` (`deposit_type_id`, `deposit_type_name`, `deposit_type_status`) VALUES
(1, 'User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor_appointment`
--

CREATE TABLE `tbl_doctor_appointment` (
  `appoint_id` int(11) NOT NULL,
  `appoint_user_id_ref` int(11) NOT NULL,
  `appoint_doctor_id_ref` int(11) NOT NULL,
  `appoint_chamber_id_ref` int(11) NOT NULL,
  `appoint_payment_method_id_ref` int(11) NOT NULL,
  `appoint_invoice_id` varchar(100) NOT NULL,
  `appoint_serial_num` int(11) NOT NULL,
  `appoint_transaction_id` varchar(100) NOT NULL DEFAULT 'N/A',
  `appoint_insertdate` varchar(100) NOT NULL,
  `appoint_isverify` int(11) NOT NULL,
  `appoint_isDeleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_doctor_appointment`
--

INSERT INTO `tbl_doctor_appointment` (`appoint_id`, `appoint_user_id_ref`, `appoint_doctor_id_ref`, `appoint_chamber_id_ref`, `appoint_payment_method_id_ref`, `appoint_invoice_id`, `appoint_serial_num`, `appoint_transaction_id`, `appoint_insertdate`, `appoint_isverify`, `appoint_isDeleted`) VALUES
(1, 2, 1, 2, 1, 'AP011325', 1, 'N/A', '2019-03-01', 0, 0),
(2, 2, 1, 2, 1, 'AP482820', 1, 'N/A', '2019-03-06', 0, 0),
(3, 2, 1, 2, 1, 'AP544953', 1, 'D5C8BE8F21C24F', '2019-03-10', 1, 0),
(4, 2, 1, 1, 1, 'AP046630', 1, 'N/A', '2019-03-17', 0, 0),
(5, 2, 1, 2, 1, 'AP293375', 1, 'N/A', '2019-03-26', 0, 0),
(6, 2, 1, 1, 2, 'AP197773', 1, '1452893258', '2019-03-26', 0, 0),
(7, 4, 1, 2, 1, 'AP070195', 1, 'N/A', '2019-04-22', 0, 0),
(8, 3, 1, 2, 1, 'AP482098', 2, 'N/A', '2019-04-22', 0, 0),
(9, 3, 1, 2, 1, 'AP299384', 1, 'N/A', '2019-04-26', 0, 0),
(10, 1, 1, 2, 1, 'AP916291', 2, 'D5CC297EA7C045', '2019-04-26', 1, 0),
(11, 1, 1, 1, 1, 'AP253182', 1, 'N/A', '2019-04-26', 0, 0),
(12, 1, 1, 2, 1, 'AP836666', 3, 'D5CC312B976F37', '2019-04-26', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor_assistant`
--

CREATE TABLE `tbl_doctor_assistant` (
  `assistant_id` int(11) NOT NULL,
  `assistant_doctor_id_ref` int(11) NOT NULL,
  `assistant_chamber_id_ref` int(11) NOT NULL,
  `assistant_name` varchar(100) NOT NULL,
  `assistant_phone` varchar(100) NOT NULL,
  `assistant_password` varchar(100) NOT NULL,
  `assistant_insert_date` varchar(100) NOT NULL,
  `assistant_isActive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_doctor_assistant`
--

INSERT INTO `tbl_doctor_assistant` (`assistant_id`, `assistant_doctor_id_ref`, `assistant_chamber_id_ref`, `assistant_name`, `assistant_phone`, `assistant_password`, `assistant_insert_date`, `assistant_isActive`) VALUES
(1, 1, 2, 'Abdur Rahim', '01733393709', 'd41d8cd98f00b204e9800998ecf8427e', '2019-04-23 17:52:01 PM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor_chamber`
--

CREATE TABLE `tbl_doctor_chamber` (
  `doctor_chamber_id` int(11) NOT NULL,
  `doctor_chamber_doctor_id_ref` int(11) DEFAULT NULL,
  `doctor_chamber_loc_title` varchar(100) NOT NULL,
  `doctor_chamber_loc_lat` double(100,6) NOT NULL,
  `doctor_chamber_loc_long` double(100,6) NOT NULL,
  `doctor_chamber_loc_details` text NOT NULL,
  `doctor_chamber_visiting_day` varchar(100) NOT NULL,
  `doctor_chamber_visiting_time_start` varchar(100) NOT NULL,
  `doctor_chamber_visiting_time_end` varchar(100) NOT NULL,
  `doctor_chamber_visiting_fee` double(13,2) NOT NULL,
  `doctor_chamber_total_patient` int(11) NOT NULL,
  `doctor_chamber_contact_serial_num` varchar(100) NOT NULL,
  `doctor_chamber_insert_datetime` varchar(100) NOT NULL,
  `doctor_chamber_verify` int(11) NOT NULL,
  `doctor_chamber_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_doctor_chamber`
--

INSERT INTO `tbl_doctor_chamber` (`doctor_chamber_id`, `doctor_chamber_doctor_id_ref`, `doctor_chamber_loc_title`, `doctor_chamber_loc_lat`, `doctor_chamber_loc_long`, `doctor_chamber_loc_details`, `doctor_chamber_visiting_day`, `doctor_chamber_visiting_time_start`, `doctor_chamber_visiting_time_end`, `doctor_chamber_visiting_fee`, `doctor_chamber_total_patient`, `doctor_chamber_contact_serial_num`, `doctor_chamber_insert_datetime`, `doctor_chamber_verify`, `doctor_chamber_admin`) VALUES
(1, 1, 'GEC', 22.330370, 91.832626, 'House No.201,Hakim Manson,O.R Nizam Road', 'Saturday', '10:00am', '1:00am', 500.00, 25, '8801733393708', '2019-02-21 18:24:36 PM', 1, 1),
(2, 1, 'Chawkbazar', 22.357908, 91.840179, 'Perchival Hill,Hakim Manson,College Road', 'Wednesday', '9:00am', '5:00pm', 600.00, 15, '8801837210137', '2019-02-21 18:59:14 PM', 1, 1),
(3, 1, 'Provortok', 23.125200, 93.152520, 'Mimi super market', 'Monday', '12:00am', '9:30pm', 500.00, 25, '8801733393708', '2019-03-15 16:53:38 PM', 1, 1),
(4, 5, 'Kumira', 22.330370, 91.832630, 'IIUC,Kumira', 'Saturday', '7:00pm', '10:00pm', 400.00, 10, '01815519968', '2019-03-26 13:19:45 PM', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor_info`
--

CREATE TABLE `tbl_doctor_info` (
  `doctor_info_id` int(11) NOT NULL,
  `doctor_info_unique_id` varchar(100) DEFAULT NULL,
  `doctor_info_reg_id` varchar(1000) NOT NULL DEFAULT 'N/A',
  `doctor_info_name` varchar(100) NOT NULL,
  `doctor_info_pass` varchar(100) NOT NULL,
  `doctor_info_email` varchar(100) NOT NULL,
  `doctor_info_image` text NOT NULL,
  `doctor_info_type_ref` int(11) NOT NULL,
  `doctor_info_degree` text NOT NULL,
  `doctor_info_details` text NOT NULL,
  `doctor_info_phone` varchar(100) DEFAULT NULL,
  `doctor_info_presentation_link` text NOT NULL,
  `doctor_info_reg_datetime` varchar(100) NOT NULL,
  `doctor_info_admin` int(11) NOT NULL,
  `doctor_info_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_doctor_info`
--

INSERT INTO `tbl_doctor_info` (`doctor_info_id`, `doctor_info_unique_id`, `doctor_info_reg_id`, `doctor_info_name`, `doctor_info_pass`, `doctor_info_email`, `doctor_info_image`, `doctor_info_type_ref`, `doctor_info_degree`, `doctor_info_details`, `doctor_info_phone`, `doctor_info_presentation_link`, `doctor_info_reg_datetime`, `doctor_info_admin`, `doctor_info_status`) VALUES
(1, 'D396414', '451472363521', 'Irfan Munna', '827ccb0eea8a706c4c34a16891f84e7b', 'shamimkafi123@gmail.com', '1550254448doctor_placeholder.jpg', 16, 'MBBAS,FCPS', 'Dr. Irfan Munna - Dental Surgeon in Aziz Medicare . Access the complete contact details of Dr Sania Faraz along with the relevant information. You can get appointment of doctor by a phone call or you can visit hospital or clinic on given address. It can facilitate you through to the accurate diagnosis and treatment of the disease. You can get hold of the contact details and consultation timings of Dr Sania Faraz in Aziz Medicare . You can also find here other Dental Surgeon doctors and consultants of the City & Hospital', '01733393711', 'https://www.youtube.com/embed/sltGyJvbvWw', '2019-02-12 10:55:25 AM', 1, 1),
(2, 'D687714', '145522363521', 'Hasan Murad', '01cfcd4f6b8770febfb40cb906715822', 'shamimkafi123@gmail.com', '1550253847doctor_placeholder.jpg', 16, 'MBBAS', 'Dr. Hasan Murad - Dental Surgeon in Aziz Medicare . Access the complete contact details of Dr Sania Faraz along with the relevant information. You can get appointment of doctor by a phone call or you can visit hospital or clinic on given address. It can facilitate you through to the accurate diagnosis and treatment of the disease. You can get hold of the contact details and consultation timings of Dr Sania Faraz in Aziz Medicare . You can also find here other Dental Surgeon doctors and consultants of the City & Hospital', '01733393709', 'https://www.youtube.com/embed/sltGyJvbvWw', '2019-02-15 19:04:07 PM', 1, 1),
(3, 'D405215', '123456789152', 'Saidul Islam Jishan', '01cfcd4f6b8770febfb40cb906715822', 'shamimkafi123@gmail.com', '1550256049doctor_placeholder.jpg', 6, 'MBBAS', 'Dr. Saidul Islam Jishan - Dental Surgeon in Aziz Medicare . Access the complete contact details of Dr Sania Faraz along with the relevant information. You can get appointment of doctor by a phone call or you can visit hospital or clinic on given address. It can facilitate you through to the accurate diagnosis and treatment of the disease. You can get hold of the contact details and consultation timings of Dr Sania Faraz in Aziz Medicare . You can also find here other Dental Surgeon doctors and consultants of the City & Hospital', '01733393715', 'https://www.youtube.com/embed/ob7laNlslzo', '2019-02-15 19:40:49 PM', 1, 1),
(4, 'DI551176', '544711226633', 'Kamrul Islam Mishu', '01cfcd4f6b8770febfb40cb906715822', 'shamimkafi123@gmail.com', '1550837414doctor_placeholder.jpg', 14, 'MBBS,FCPS', 'Dr. Kamrul Islam Mishu - Dental Surgeon in Aziz Medicare . Access the complete contact details of Dr Sania Faraz along with the relevant information. You can get appointment of doctor by a phone call or you can visit hospital or clinic on given address. It can facilitate you through to the accurate diagnosis and treatment of the disease. You can get hold of the contact details and consultation timings of Dr Sania Faraz in Aziz Medicare . You can also find here other Dental Surgeon doctors and consultants of the City & Hospital', '01815519969', 'https://www.youtube.com/embed/sltGyJvbvWw', '2019-02-22 13:10:14 PM', 1, 1),
(5, 'DI637052', '25423165887', 'Rafiqul Islam', 'd41d8cd98f00b204e9800998ecf8427e', 'shamimkafi123@gmail.com', '1553585516doctor_placeholder.jpg', 2, 'MBBS', 'Dr. Rafiqul Islam - Dental Surgeon in Aziz Medicare . Access the complete contact details of Dr Rafiqul Islam along with the relevant information. You can get appointment of doctor by a phone call or you can visit hospital or clinic on given address. It can facilitate you through to the accurate diagnosis and treatment of the disease. You can get hold of the contact details and consultation timings of Dr Rafiqul Islam in Aziz Medicare . You can also find here other Dental Surgeon doctors and consultants of the City & Hospital', '01815519968', 'https://www.youtube.com/embed/ob7laNlslzo', '2019-03-26 08:31:56 AM', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_doctor_type`
--

CREATE TABLE `tbl_doctor_type` (
  `doctor_type_id` int(11) NOT NULL,
  `doctor_type_name` varchar(100) NOT NULL,
  `doctor_type_details` text NOT NULL,
  `doctor_type_admin` int(11) NOT NULL,
  `doctor_type_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_doctor_type`
--

INSERT INTO `tbl_doctor_type` (`doctor_type_id`, `doctor_type_name`, `doctor_type_details`, `doctor_type_admin`, `doctor_type_status`) VALUES
(1, 'Cardiologist', 'N/A', 1, 1),
(2, 'Colorectal surgeon', 'N/A', 1, 1),
(3, 'Dentist', 'N/A', 1, 1),
(4, 'Dermatologist', 'N/A', 1, 1),
(5, 'Dietician', 'N/A', 1, 1),
(6, 'Eye Doctor', 'N/A', 1, 1),
(7, 'Eye, Nose, Ear (ENT) specialist', 'N/A', 1, 1),
(8, 'Nephrologist', 'N/A', 1, 1),
(9, 'Neurosurgeon', 'N/A', 1, 1),
(10, 'Nutritionist', 'N/A', 1, 1),
(11, 'Oncologist', 'N/A', 1, 1),
(12, 'Ophthalmologist', 'N/A', 1, 1),
(13, 'Orthodontist', 'N/A', 1, 1),
(14, 'Orthopedic Surgeon', 'N/A', 1, 1),
(15, 'Pediatrician', 'N/A', 1, 1),
(16, 'Physiotherapist', 'N/A', 1, 1),
(17, 'Plastic surgeon', 'N/A', 1, 1),
(18, 'Primary care doctor', 'N/A', 1, 1),
(19, 'Psychiatrist', 'N/A', 1, 1),
(20, 'Psychologist', 'N/A', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE `tbl_message` (
  `msg_id` int(11) NOT NULL,
  `msg_user_id_ref` int(11) NOT NULL,
  `msg_doctor_id_ref` int(11) NOT NULL,
  `msg_admin_id_ref` int(11) NOT NULL,
  `msg_details` text NOT NULL,
  `msg_send` int(11) NOT NULL DEFAULT '0',
  `msg_datetime` varchar(1000) NOT NULL,
  `msg_is_seen` int(11) NOT NULL DEFAULT '0',
  `msg_isDelete` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_message`
--

INSERT INTO `tbl_message` (`msg_id`, `msg_user_id_ref`, `msg_doctor_id_ref`, `msg_admin_id_ref`, `msg_details`, `msg_send`, `msg_datetime`, `msg_is_seen`, `msg_isDelete`) VALUES
(7, 2, 1, 0, 'Please Help me!!', 0, '2019/03/17 06:42:13 am', 1, 0),
(8, 0, 5, 1, 'Assalamu alaikum', 0, '2019/03/26 01:13:02 pm', 0, 0),
(9, 2, 1, 0, 'OK', 1, '2019/04/22 12:43:50 pm', 0, 0),
(10, 0, 1, 1, 'admin', 1, '2019/04/22 01:28:08 pm', 0, 0),
(11, 0, 1, 1, 'hi', 0, '2019/04/22 02:50:56 pm', 0, 1),
(12, 0, 1, 1, 'thik ase', 1, '2019/04/22 02:58:52 pm', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment_method`
--

CREATE TABLE `tbl_payment_method` (
  `payment_id` int(11) NOT NULL,
  `payment_name` varchar(100) NOT NULL,
  `payment_insertdate` varchar(100) NOT NULL,
  `payment_isactive` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_payment_method`
--

INSERT INTO `tbl_payment_method` (`payment_id`, `payment_name`, `payment_insertdate`, `payment_isactive`) VALUES
(1, 'Cash', '2019-02-22 13:10:14 PM', 1),
(2, 'bKash', '2019-02-23 13:10:14 PM', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rating`
--

CREATE TABLE `tbl_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_userid` int(11) NOT NULL,
  `rating_doctorid` int(11) NOT NULL,
  `rating_point` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_rating`
--

INSERT INTO `tbl_rating` (`rating_id`, `rating_userid`, `rating_doctorid`, `rating_point`) VALUES
(1, 2, 1, 2),
(2, 2, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_info`
--

CREATE TABLE `tbl_user_info` (
  `user_id` int(11) NOT NULL,
  `user_unique_id` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `user_phone` varchar(100) DEFAULT NULL,
  `user_blood_group` varchar(100) NOT NULL,
  `user_last_donation_date` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `user_district` varchar(100) NOT NULL,
  `user_thana` varchar(100) NOT NULL,
  `user_address` text NOT NULL,
  `user_insert_datetime` varchar(100) NOT NULL,
  `user_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user_info`
--

INSERT INTO `tbl_user_info` (`user_id`, `user_unique_id`, `user_name`, `user_phone`, `user_blood_group`, `user_last_donation_date`, `user_email`, `user_password`, `user_district`, `user_thana`, `user_address`, `user_insert_datetime`, `user_status`) VALUES
(1, 'UI1122334', 'Emergency', '880', '', '', '', '', '', '', '', '', 0),
(2, 'UI687943', 'Tanzim', '01822220350', 'AB+', '12-03-2017', 'tanzimahmed1994@gmail.com', '01cfcd4f6b8770febfb40cb906715822', '', '', 'Chawkbazar', '2019-02-23 19:36:40 PM', 1),
(3, 'UI749417', 'Shamim Kafi', '01733393709', 'A+', '12-01-2019', 'shamimkafi123@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '', 'Chawkbazar', '2019-02-27 19:14:58 PM', 1),
(4, 'UI411838', 'Rifat', '01733393711', 'B-', '02-09-2018', 'shamimkafi123@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '', 'GEC', '2019-03-17 06:11:32 AM', 1),
(5, 'UI962758', 'SK', '01733393715', 'A+', '14-06-2017', 'shamimkafi123@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '', 'Provortok', '2019-04-22 15:36:38 PM', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin_info`
--
ALTER TABLE `tbl_admin_info`
  ADD PRIMARY KEY (`a_id`),
  ADD KEY `a_email` (`a_email`);

--
-- Indexes for table `tbl_admin_type`
--
ALTER TABLE `tbl_admin_type`
  ADD PRIMARY KEY (`at_id`),
  ADD KEY `at_type` (`at_type`);

--
-- Indexes for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `tbl_comments`
--
ALTER TABLE `tbl_comments`
  ADD PRIMARY KEY (`comm_id`);

--
-- Indexes for table `tbl_deposit_history`
--
ALTER TABLE `tbl_deposit_history`
  ADD PRIMARY KEY (`deposit_id`);

--
-- Indexes for table `tbl_deposit_type`
--
ALTER TABLE `tbl_deposit_type`
  ADD PRIMARY KEY (`deposit_type_id`);

--
-- Indexes for table `tbl_doctor_appointment`
--
ALTER TABLE `tbl_doctor_appointment`
  ADD PRIMARY KEY (`appoint_id`);

--
-- Indexes for table `tbl_doctor_assistant`
--
ALTER TABLE `tbl_doctor_assistant`
  ADD PRIMARY KEY (`assistant_id`);

--
-- Indexes for table `tbl_doctor_chamber`
--
ALTER TABLE `tbl_doctor_chamber`
  ADD PRIMARY KEY (`doctor_chamber_id`),
  ADD KEY `doctor_chamber_doctor_id` (`doctor_chamber_doctor_id_ref`);

--
-- Indexes for table `tbl_doctor_info`
--
ALTER TABLE `tbl_doctor_info`
  ADD PRIMARY KEY (`doctor_info_id`),
  ADD KEY `doctor_info_unique_id` (`doctor_info_unique_id`),
  ADD KEY `doctor_info_phone` (`doctor_info_phone`);

--
-- Indexes for table `tbl_doctor_type`
--
ALTER TABLE `tbl_doctor_type`
  ADD PRIMARY KEY (`doctor_type_id`);

--
-- Indexes for table `tbl_message`
--
ALTER TABLE `tbl_message`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `tbl_payment_method`
--
ALTER TABLE `tbl_payment_method`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `tbl_user_info`
--
ALTER TABLE `tbl_user_info`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_phone` (`user_phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin_info`
--
ALTER TABLE `tbl_admin_info`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_admin_type`
--
ALTER TABLE `tbl_admin_type`
  MODIFY `at_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_blog`
--
ALTER TABLE `tbl_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_comments`
--
ALTER TABLE `tbl_comments`
  MODIFY `comm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_deposit_history`
--
ALTER TABLE `tbl_deposit_history`
  MODIFY `deposit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_deposit_type`
--
ALTER TABLE `tbl_deposit_type`
  MODIFY `deposit_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_doctor_appointment`
--
ALTER TABLE `tbl_doctor_appointment`
  MODIFY `appoint_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_doctor_assistant`
--
ALTER TABLE `tbl_doctor_assistant`
  MODIFY `assistant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_doctor_chamber`
--
ALTER TABLE `tbl_doctor_chamber`
  MODIFY `doctor_chamber_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_doctor_info`
--
ALTER TABLE `tbl_doctor_info`
  MODIFY `doctor_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_doctor_type`
--
ALTER TABLE `tbl_doctor_type`
  MODIFY `doctor_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_message`
--
ALTER TABLE `tbl_message`
  MODIFY `msg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_payment_method`
--
ALTER TABLE `tbl_payment_method`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_rating`
--
ALTER TABLE `tbl_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_user_info`
--
ALTER TABLE `tbl_user_info`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
