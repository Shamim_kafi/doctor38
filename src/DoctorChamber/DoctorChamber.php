<?php
namespace App\DoctorChamber;

use App\Model\Database;
use PDO,PDOException;
use App\Message\Message;

class DoctorChamber extends Database
{
    public $doctorChamberId;
    public $doctorIdRef;
    public $doctorLocTitle;
    public $doctorLocLat;
    public $doctorLocLong;
    public $doctorLocDetails;
    public $visitingDay;
    public $visitingTimeStart;
    public $visitingTimeEnd;
    public $visitingFee;
    public $totalPatientViewDaily;
    public $contactSerialNumber;
    public $chamberVerify;

    public function setDoctorChamberData($rcv){

        if (array_key_exists('doctor_chamber_id',$rcv)){
            $this->doctorChamberId =  $rcv ['doctor_chamber_id'];
        }
        if (array_key_exists('doctor_ref_id',$rcv)){
            $this->doctorIdRef =  $rcv ['doctor_ref_id'];
        }
        if (array_key_exists('location_head_name',$rcv)){
            $this->doctorLocTitle=  $rcv ['location_head_name'];
        }
        if (array_key_exists('location_lat',$rcv)){
            $this->doctorLocLat =  $rcv ['location_lat'];
        }
        if (array_key_exists('location_long',$rcv)){
            $this->doctorLocLong =  $rcv ['location_long'];
        }
        if (array_key_exists('chamber_address',$rcv)){
            $this->doctorLocDetails =  $rcv ['chamber_address'];
        }
        if (array_key_exists('visiting_day',$rcv)){
            $this->visitingDay =  $rcv ['visiting_day'];
        }
        if (array_key_exists('visiting_time_start',$rcv)){
            $this->visitingTimeStart =  $rcv ['visiting_time_start'];
        }
        if (array_key_exists('visiting_time_end',$rcv)){
            $this->visitingTimeEnd =  $rcv ['visiting_time_end'];
        }
        if (array_key_exists('consultancy_fee',$rcv)){
            $this->visitingFee =  $rcv ['consultancy_fee'];
        }
        if (array_key_exists('total_patient_view_daily',$rcv)){
            $this->totalPatientViewDaily =  $rcv ['total_patient_view_daily'];
        }
        if (array_key_exists('serial_number_contact',$rcv)){
            $this->contactSerialNumber =  $rcv ['serial_number_contact'];
        }
        if (array_key_exists('is_active',$rcv)){
            $this->chamberVerify =  $rcv ['is_active'];
        }
    }

    public function storeDoctorChamber(){

        $doctorIdRef =  $this->doctorIdRef;
        $doctorLocTitle =  $this->doctorLocTitle;
        $doctorLocLat =  $this->doctorLocLat;
        $doctorLocLong =  $this->doctorLocLong;
        $doctorLocDetails =  $this->doctorLocDetails;
        $visitingDay =  $this->visitingDay;
        $visitingTimeStart =  $this->visitingTimeStart;
        $visitingTimeEnd =  $this->visitingTimeEnd;
        $visitingFee =  $this->visitingFee;
        $totalPatientViewDaily =  $this->totalPatientViewDaily;
        $contactSerialNumber =  $this->contactSerialNumber;

        $insertDateTime=date("Y-m-d H:i:s A");
        date_default_timezone_set("Asia/Dhaka");

        $chamberVerify =  $this->chamberVerify;
        $admin = 1;

        $dataArray = array($doctorIdRef,$doctorLocTitle,$doctorLocLat,$doctorLocLong,$doctorLocDetails,$visitingDay,$visitingTimeStart,$visitingTimeEnd,$visitingFee,
            $totalPatientViewDaily,$contactSerialNumber,$insertDateTime,$chamberVerify,$admin);

        $sql = "INSERT INTO `tbl_doctor_chamber`(`doctor_chamber_doctor_id_ref`,`doctor_chamber_loc_title`,`doctor_chamber_loc_lat`,`doctor_chamber_loc_long`,`doctor_chamber_loc_details`,
`doctor_chamber_visiting_day`,`doctor_chamber_visiting_time_start`,`doctor_chamber_visiting_time_end`,`doctor_chamber_visiting_fee`,`doctor_chamber_total_patient`,`doctor_chamber_contact_serial_num`,
`doctor_chamber_insert_datetime`, `doctor_chamber_verify`,`doctor_chamber_admin`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");
        }

    }


    public function ownChamberAdd(){

        $doctorIdRef =  $this->doctorIdRef;
        $doctorLocTitle =  $this->doctorLocTitle;
        $doctorLocLat =  $this->doctorLocLat;
        $doctorLocLong =  $this->doctorLocLong;
        $doctorLocDetails =  $this->doctorLocDetails;
        $visitingDay =  $this->visitingDay;
        $visitingTimeStart =  $this->visitingTimeStart;
        $visitingTimeEnd =  $this->visitingTimeEnd;
        $visitingFee =  $this->visitingFee;
        $totalPatientViewDaily =  $this->totalPatientViewDaily;
        $contactSerialNumber =  $this->contactSerialNumber;

        $insertDateTime=date("Y-m-d H:i:s A");
        date_default_timezone_set("Asia/Dhaka");


        $dataArray = array($doctorIdRef,$doctorLocTitle,$doctorLocLat,$doctorLocLong,$doctorLocDetails,$visitingDay,$visitingTimeStart,$visitingTimeEnd,$visitingFee,
            $totalPatientViewDaily,$contactSerialNumber,$insertDateTime);

        $sql = "INSERT INTO `tbl_doctor_chamber`(`doctor_chamber_doctor_id_ref`,`doctor_chamber_loc_title`,`doctor_chamber_loc_lat`,`doctor_chamber_loc_long`,`doctor_chamber_loc_details`,
`doctor_chamber_visiting_day`,`doctor_chamber_visiting_time_start`,`doctor_chamber_visiting_time_end`,`doctor_chamber_visiting_fee`,`doctor_chamber_total_patient`,`doctor_chamber_contact_serial_num`,
`doctor_chamber_insert_datetime`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");
        }

    }

    //for all single doctor chamber
    public function allSingleDoctorChamberInfo($doctorId){
        $sqlQuery = "select * from tbl_doctor_chamber where doctor_chamber_doctor_id_ref='$doctorId' 
order by doctor_chamber_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    /*for deactivate chamber*/
    public function deactivateChamber($chamberId){
        $chamberVerify = 0;
        $dataArray = array($chamberVerify);
        $sql = "UPDATE `tbl_doctor_chamber` SET `doctor_chamber_verify` = ? WHERE doctor_chamber_id='$chamberId'";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Chamber has been deactivated!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been deactivated!
                              </div>");
        }
    }

    //for single doctor chamber
    public function singleDoctorChamberInfo($doctorChamberId){
        $sqlQuery = "select * from tbl_doctor_chamber where doctor_chamber_verify=1 AND doctor_chamber_id='$doctorChamberId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    /*for chamber update*/
    public function singleChamberUpdate(){
        $doctorLocTitle =  $this->doctorLocTitle;
        $doctorLocDetails =  $this->doctorLocDetails;
        $visitingDay =  $this->visitingDay;
        $visitingTimeStart =  $this->visitingTimeStart;
        $visitingTimeEnd =  $this->visitingTimeEnd;
        $visitingFee =  $this->visitingFee;
        $totalPatientViewDaily =  $this->totalPatientViewDaily;
        $contactSerialNumber =  $this->contactSerialNumber;
        $chamberVerify =  $this->chamberVerify;
        $admin = 1;

        $dataArray = array($doctorLocTitle,$doctorLocDetails,$visitingDay,$visitingTimeStart,$visitingTimeEnd,$visitingFee,
            $totalPatientViewDaily,$contactSerialNumber,$chamberVerify,$admin);

        $sql = "UPDATE `tbl_doctor_chamber` SET `doctor_chamber_loc_title`=?,`doctor_chamber_loc_details`=?,`doctor_chamber_visiting_day`=?,
`doctor_chamber_visiting_time_start`=?,`doctor_chamber_visiting_time_end`=?,`doctor_chamber_visiting_fee`=?,`doctor_chamber_total_patient`=?,
`doctor_chamber_contact_serial_num`=?,`doctor_chamber_verify`=?,`doctor_chamber_admin`=? WHERE `doctor_chamber_id`='$this->doctorChamberId'";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Chamber has been updated!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been updated!
                              </div>");
        }
    }

    //admin
    //for single doctor info &  chamber info
    public function doctorSingleChamberInfo($doctorChamberId){
        $sqlQuery = "SELECT * FROM `tbl_doctor_chamber` INNER JOIN tbl_doctor_info on tbl_doctor_chamber.doctor_chamber_doctor_id_ref = tbl_doctor_info.doctor_info_id WHERE `doctor_chamber_id`='$doctorChamberId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    //for all single doctor chamber
    public function allSingleDoctorUniqueChamberLocation($doctorId){
        $sqlQuery = "SELECT DISTINCT  * FROM `tbl_doctor_chamber` where doctor_chamber_verify=1 AND doctor_chamber_doctor_id_ref='$doctorId' 
order by doctor_chamber_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    /*for chamber accept*/
    public function chamberAccept($chamberId){
        $chamberVerify = 1;
        $admin = 1;

        $dataArray = array($chamberVerify,$admin);
        $sql = "UPDATE `tbl_doctor_chamber` SET `doctor_chamber_verify` = ?,`doctor_chamber_admin`=? WHERE doctor_chamber_id='$chamberId'";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Chamber has been accepted!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been accepted!
                              </div>");
        }
    }

}