<?php
namespace App\Auth;

use App\Model\Database;
use PDO,PDOException;

class AssistantAuth extends Database
{

    public $assistantPhone;
    public $assistantPass;


    public function setData($data = Array()){

        if (array_key_exists('assistant_phone', $data)) {
            $this->assistantPhone = $data['assistant_phone'];
        }
        if (array_key_exists('assistant_pass', $data)) {
            $this->assistantPass = $data['assistant_pass'];
        }
        return $this;
    }

    public function is_exist()
    {
        $query="SELECT * FROM `tbl_doctor_assistant` WHERE `tbl_doctor_assistant`.`assistant_phone` ='$this->assistantPhone'";
        $STH=$this->db->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function assistantLogin(){

        $query="SELECT * FROM `tbl_doctor_assistant` WHERE `assistant_phone`='$this->assistantPhone' AND assistant_isActive=1";

        $STH=$this->db->query($query);

        if ($STH->rowCount()<=0){
            return false;
        }
        else {

            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $row = $STH->fetch();
            $passHash = md5($this->assistantPass);

            if($passHash===$row['assistant_password']){
                return $row['assistant_id']; //account existent, email password do not match
            }
        }
    }


}