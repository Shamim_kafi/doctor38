<?php
namespace App\Auth;


use App\Model\Database;
use PDO,PDOException;

class AdminAuth extends Database
{

    public $adminId;
    public $adminEmail;
    public $adminPass;


    public function setData($data = Array()){

        if (array_key_exists('admin_id', $data)) {
            $this->adminId = $data['admin_id'];
        }
        if (array_key_exists('admin_email', $data)) {
            $this->adminEmail = $data['admin_email'];
        }
        if (array_key_exists('admin_pass', $data)) {
            $this->adminPass = $data['admin_pass'];
        }
        return $this;
    }


    public function adminLogin(){

        $query="SELECT * FROM `tbl_admin_info` WHERE `a_status` = 1 AND `a_email`='$this->adminEmail' 
AND a_password='$this->adminPass'";

        $STH=$this->db->query($query);

        if ($STH->rowCount()<=0){
            return false;
        }
        else {

            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $row = $STH->fetch();
            $passHash = $this->adminPass;

            if($passHash === $row['a_password']){
                return $row['a_id'];
            }
        }
    }

}