<?php
namespace App\Auth;


use App\Model\Database;
use PDO,PDOException;

class DoctorAuth extends Database
{

    public $doctorUniqueId;
    public $doctorEmail;
    public $doctorPhone;
    public $doctorPass;


    public function setData($data = Array()){

        if (array_key_exists('doctor_unique_id', $data)) {
            $this->doctorUniqueId = $data['doctor_unique_id'];
        }
        if (array_key_exists('doctor_email', $data)) {
            $this->doctorEmail = $data['doctor_email'];
        }
        if (array_key_exists('doctor_phone', $data)) {
            $this->doctorPhone = $data['doctor_phone'];
        }
        if (array_key_exists('doctor_pass', $data)) {
            $this->doctorPass = $data['doctor_pass'];
        }
        return $this;
    }

    public function is_exist()
    {
        $query="SELECT * FROM `tbl_doctor_info` WHERE `tbl_doctor_info`.`doctor_info_phone` ='$this->doctorPhone'";
        $STH=$this->db->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function doctorLogin(){

        $query="SELECT * FROM `tbl_doctor_info` WHERE `doctor_info_unique_id`='$this->doctorUniqueId' AND doctor_info_status=1 AND doctor_info_admin=1";

        $STH=$this->db->query($query);

        if ($STH->rowCount()<=0){
            return false;
        }
        else {

            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $row = $STH->fetch();
            $passHash = md5($this->doctorPass);

            if($passHash===$row['doctor_info_pass']){
                return $row['doctor_info_id']; //account existent, email password do not match
            }
        }
    }

}