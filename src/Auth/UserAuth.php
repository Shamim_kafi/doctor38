<?php
namespace App\Auth;

use App\Model\Database;
use PDO,PDOException;

class UserAuth extends Database
{

    public $userUniqueId;
    public $userPhone;
    public $userPass;


    public function setData($data = Array()){
        if (array_key_exists('customer_unique_id', $data)) {
            $this->userUniqueId = $data['customer_unique_id'];
        }
        if (array_key_exists('user_phone', $data)) {
            $this->userPhone = $data['user_phone'];
        }
        if (array_key_exists('user_pass', $data)) {
            $this->userPass = $data['user_pass'];
        }
        return $this;
    }

    public function is_exist()
    {
        $query="SELECT * FROM `tbl_user_info` WHERE `tbl_user_info`.`u_phone` ='$this->userPhone'";
        $STH=$this->db->query($query);

        $STH->setFetchMode(PDO::FETCH_OBJ);
        $STH->fetchAll();

        $count = $STH->rowCount();

        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function customerLogin(){

        $query="SELECT * FROM `tbl_user_info` WHERE `user_phone`='$this->userPhone' AND user_status=1";

        $STH=$this->db->query($query);

        if ($STH->rowCount()<=0){
            return false;
        }
        else {

            $STH->setFetchMode(PDO::FETCH_ASSOC);
            $row = $STH->fetch();
            $passHash = md5($this->userPass);

            if($passHash===$row['user_password']){
                return $row['user_id']; //account existent, email password do not match
            }
        }
    }


}