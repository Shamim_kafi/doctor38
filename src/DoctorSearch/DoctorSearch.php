<?php
namespace App\DoctorSearch;

use PDO,PDOException;
use App\Model\Database;

class DoctorSearch extends Database
{

    //for doctor list show by doctor type
    public function doctorSearch($doctorChamberTitle,$specialitiesIn){

        $sqlQuery = "
                    SELECT * ,
                    1 as distance_in_km
                    FROM `tbl_doctor_info` 
                    
                    INNER JOIN tbl_doctor_chamber on `tbl_doctor_info`.`doctor_info_id` = tbl_doctor_chamber.doctor_chamber_doctor_id_ref 
                    LEFT JOIN tbl_rating on `tbl_doctor_info`.`doctor_info_id` = tbl_rating.rating_doctorid
                    
                    WHERE 
                    `tbl_doctor_chamber`.`doctor_chamber_loc_title`='$doctorChamberTitle' AND 
                    `tbl_doctor_info`.`doctor_info_type_ref`='$specialitiesIn' AND
                    `tbl_doctor_info`.doctor_info_status=1
                    
                    ORDER BY tbl_rating.rating_point DESC
                    ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }


    //for doctor list show by doctor type
    public function doctorSearchByGeoLocation($geoLat,$geoLong,$specialitiesIn){



        $sqlQuery = "
                   SELECT 
                        *,
                        111.045* 
                        DEGREES(ACOS(COS(RADIANS($geoLat)) * 
                        COS(RADIANS(tbl_doctor_chamber.`doctor_chamber_loc_lat`)) * 
                        COS(RADIANS($geoLong) - RADIANS(tbl_doctor_chamber.`doctor_chamber_loc_long`)) + 
                        SIN(RADIANS($geoLat)) 
                        * SIN(RADIANS(tbl_doctor_chamber.`doctor_chamber_loc_lat`))))
                        AS distance_in_km 
                        
                   FROM `tbl_doctor_info` 
                   
                   INNER JOIN 
                      tbl_doctor_chamber on `tbl_doctor_info`.`doctor_info_id` = tbl_doctor_chamber.doctor_chamber_doctor_id_ref 
                   LEFT JOIN 
                      tbl_rating on `tbl_doctor_info`.`doctor_info_id` = tbl_rating.rating_doctorid
                       
                   WHERE 
                     `tbl_doctor_info`.`doctor_info_type_ref`='$specialitiesIn' AND 
                     `tbl_doctor_info`.doctor_info_status=1 
                  
                  ORDER BY 
                    tbl_rating.rating_point DESC";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //for doctor list show by doctor type
    public function singleDoctorAndSingleChamber($doctorId,$chamberId){
        $sqlQuery = "SELECT * FROM `tbl_doctor_info` 
INNER JOIN tbl_doctor_chamber on `tbl_doctor_info`.`doctor_info_id` = tbl_doctor_chamber.doctor_chamber_doctor_id_ref
INNER JOIN tbl_doctor_type on `tbl_doctor_info`.`doctor_info_type_ref` = tbl_doctor_type.doctor_type_id
WHERE `tbl_doctor_chamber`.`doctor_chamber_id`='$chamberId' AND `tbl_doctor_info`.`doctor_info_id`='$doctorId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

}