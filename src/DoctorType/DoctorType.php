<?php
namespace App\DoctorType;

use PDO,PDOException;
use App\Model\Database;

class DoctorType extends Database
{

    public $doctorTypeId;
    public $doctorTypeName;
    public $doctorTypeDetails;

    public function setDoctorTypeData($rcv){

        if (array_key_exists('doctor_type_id',$rcv)){
            $this->doctorTypeId =  $rcv ['doctor_type_id'];
        }
        if (array_key_exists('doctor_type_name',$rcv)){
            $this->doctorTypeName =  $rcv ['doctor_type_name'];
        }
        if (array_key_exists('doctor_type_details',$rcv)){
            $this->doctorTypeDetails =  $rcv ['doctor_type_details'];
        }
    }

    public function storeDoctorType(){

        $doctorTypeName =  $this->doctorTypeName;
        $doctorTypeDetails =  $this->doctorTypeDetails;
        $admin = 1;
        $status = 1;

        $dataArray = array($doctorTypeName,$doctorTypeDetails,$admin,$status);
        $sql = "INSERT INTO `tbl_doctor_type`(`doctor_type_name`,`doctor_type_details`,`doctor_type_admin`,`doctor_type_status`) VALUES (?,?,?,?);";
        $sth = $this->db->prepare($sql);
        $sth->execute($dataArray);

    }

    //for all doctor type get
    public function allDoctorTypeInfo(){
        $sqlQuery = "select doctor_type_id,doctor_type_name from tbl_doctor_type where doctor_type_admin=1 AND doctor_type_status=1 order by doctor_type_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }


}