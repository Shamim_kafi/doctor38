<?php
namespace App\MessageBox;


use App\Model\Database;
use PDO,PDOException;
use App\Message\Message;

class MessageBox extends Database
{
    public $msgId;
    public $msgUserId;
    public $msgDoctorId;
    public $msgAdminId;
    public $msgDetails;
    public $msgSend;


    public function setMsgData($rcv){

        if (array_key_exists('msg_id',$rcv)){
            $this->msgId =  $rcv ['msg_id'];
        }
        if (array_key_exists('msg_user_id',$rcv)){
            $this->msgUserId =  $rcv ['msg_user_id'];
        }
        if (array_key_exists('msg_doctor_id',$rcv)){
            $this->msgDoctorId =  $rcv ['msg_doctor_id'];
        }
        if (array_key_exists('msg_admin_id',$rcv)){
            $this->msgAdminId =  $rcv ['msg_admin_id'];
        }
        if (array_key_exists('msg_details',$rcv)){
            $this->msgDetails =  $rcv ['msg_details'];
        }

        if (array_key_exists('msg_send',$rcv)){
            $this->msgSend =  $rcv ['msg_send'];
        }
    }

    public function storeMsg(){

        $msgUserId = $this->msgUserId;
        $msgDoctorId = $this->msgDoctorId;
        $msgAdminId = $this->msgAdminId;
        $msgDetails = $this->msgDetails;
        $msgSend = $this->msgSend;

        $msgDate=date("Y/m/d h:i:s a");
        date_default_timezone_set("Asia/Dhaka");

        $dataArray = array($msgUserId,$msgDoctorId,$msgAdminId,$msgDetails,$msgSend,$msgDate);

        $sql = "INSERT INTO `tbl_message` (`msg_user_id_ref`,`msg_doctor_id_ref`,`msg_admin_id_ref`,`msg_details`,`msg_send`,`msg_datetime`) VALUES (?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);
        $sth->execute($dataArray);
    }


    //for latest new msg user
    public function latestNewMsg($userId){
        $sqlQuery = "select * from `tbl_message` WHERE msg_user_id=$userId AND msg_admin_id > 0 ORDER BY msg_id DESC LIMIT 10";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //all message history
    public function allMessageHistory(){

        $sqlQuery = "select * 
                    from `tbl_message` 
                  
                    LEFT JOIN 
                        tbl_user_info ON `tbl_message`.msg_user_id_ref=`tbl_user_info`.user_id  
            
                    LEFT JOIN 
                        tbl_doctor_info ON tbl_message.msg_doctor_id_ref = tbl_doctor_info.doctor_info_id
                        
                    WHERE `tbl_message`.msg_isDelete =0
                    
                   ORDER BY msg_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }


    //all message history for single doctor
    public function singleDoctorAllMessage($doctorId){

        $sqlQuery = "select * 
                    from `tbl_message` 
                    
                    LEFT JOIN 
                        tbl_admin_info ON `tbl_message`.msg_admin_id_ref=`tbl_admin_info`.a_id  
                  
                    LEFT JOIN 
                        tbl_user_info ON `tbl_message`.msg_user_id_ref=`tbl_user_info`.user_id  
                      
                    WHERE `tbl_message`.msg_doctor_id_ref='$doctorId' AND `tbl_message`.msg_send=0 AND msg_isDelete=0
                    
                    ORDER BY msg_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //all unseen message history for single doctor
    public function singleDoctorAllUnSeenMessage($doctorId){

        $sqlQuery = "
                    select * 
                    from `tbl_message` 
                    
                    LEFT JOIN 
                        tbl_admin_info ON `tbl_message`.msg_admin_id_ref=`tbl_admin_info`.a_id  
                  
                    LEFT JOIN 
                        tbl_user_info ON `tbl_message`.msg_user_id_ref=`tbl_user_info`.user_id  
                      
                    WHERE `tbl_message`.msg_doctor_id_ref='$doctorId' AND `tbl_message`.msg_is_seen=0 AND `tbl_message`.msg_send=0 AND msg_isDelete=0
                   ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }


    //for count all unseen msg
    public function unSeenMsgCount(){
        $sqlQuery = "select * from `tbl_message` WHERE msg_is_seen=0 AND msg_isDelete=0";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }


    //for seen msg
    public function msgIsSeen($msgId){

        $isSeen = 1;
        $dataArray = array($isSeen);

        $sql = "UPDATE `tbl_message` SET `msg_is_seen`=? WHERE `msg_id`='$msgId'";

        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            /*Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");*/
        }else{
            /*Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");*/
        }

    }

    //for seen msg
    public function msgIsDelete($msgId){

        $isSeen = 1;
        $dataArray = array($isSeen);

        $sql = "UPDATE `tbl_message` SET `msg_isDelete`=? WHERE `msg_id`='$msgId'";

        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            /*Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");*/
        }else{
            /*Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");*/
        }

    }

}