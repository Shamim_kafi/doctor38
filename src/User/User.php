<?php
namespace App\User;

use PDO,PDOException;
use App\Model\Database;
use App\Message\Message;

class User extends Database
{

    public $userId;
    public $userUniqueId;
    public $userName;
    public $userPhone;
    public $bloodGroup;
    public $lastDonationDate;
    public $userEmail;
    public $userAddress;
    public $userPassword;

    public function setUserData($rcv){

        if (array_key_exists('user_id',$rcv)){
            $this->userId =  $rcv ['user_id'];
        }
        if (array_key_exists('user_unique_id',$rcv)){
            $this->userUniqueId =  $rcv ['user_unique_id'];
        }
        if (array_key_exists('user_name',$rcv)){
            $this->userName=  $rcv ['user_name'];
        }
        if (array_key_exists('phone_num',$rcv)){
            $this->userPhone =  $rcv ['phone_num'];
        }
        if (array_key_exists('blood_group',$rcv)){
            $this->bloodGroup =  $rcv ['blood_group'];
        }
        if (array_key_exists('last_donation_date',$rcv)){
            $this->lastDonationDate =  $rcv ['last_donation_date'];
        }
        if (array_key_exists('user_email',$rcv)){
            $this->userEmail =  $rcv ['user_email'];
        }
        if (array_key_exists('user_address',$rcv)){
            $this->userAddress =  $rcv ['user_address'];
        }
        if (array_key_exists('user_pass',$rcv)){
            $this->userPassword =  md5($rcv ['user_pass']);
        }
    }

    public function storeUserInfo(){

        $userUniqueId =  $this->userUniqueId;
        $userName =  $this->userName;
        $userPhone =  $this->userPhone;
        $bloodGroup =  $this->bloodGroup;
        $lastDonationDate =  $this->lastDonationDate;
        $userEmail =  $this->userEmail;
        $userAddress =  $this->userAddress;
        $userPassword =  $this->userPassword;

        $joinDateTime=date("Y-m-d H:i:s A");
        date_default_timezone_set("Asia/Dhaka");
        $status = 1;

        $dataArray = array($userUniqueId,$userName,$userPhone,$bloodGroup,$lastDonationDate,$userEmail,$userAddress,$userPassword,
            $joinDateTime,$status);

        $sql = "INSERT INTO `tbl_user_info`(`user_unique_id`,`user_name`,`user_phone`,`user_blood_group`,`user_last_donation_date`,
`user_email`,`user_address`,`user_password`,`user_insert_datetime`,`user_status`) VALUES (?,?,?,?,?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> User Reg has been completed!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> User reg has not been completed!
                              </div>");
        }

    }

    //for single user info
    public function singleUserInfo($userId){
        $sqlQuery = "select * from tbl_user_info where user_id='$userId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    //for all user info
    public function allUserInfo(){
        $sqlQuery = "select * from tbl_user_info";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    /*for user info update*/
    public function singleUserInfoUpdate(){

        $userName =  $this->userName;
        $userPhone =  $this->userPhone;
        $bloodGroup =  $this->bloodGroup;
        $lastDonationDate =  $this->lastDonationDate;
        $userEmail =  $this->userEmail;
        $userAddress =  $this->userAddress;
        $userPassword =  $this->userPassword;

        $dataArray = array($userName,$userPhone,$bloodGroup,$lastDonationDate,$userEmail,$userAddress,$userPassword);

        $sql = "UPDATE `tbl_user_info` SET `user_name`=?,`user_phone`=?,`user_blood_group`=?,
`user_last_donation_date`=?,`user_email`=?,`user_address`=?,`user_password`=? WHERE `user_id`='$this->userId'";
        $sth = $this->db->prepare($sql);
        //$sth->execute($dataArray);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> User info has been updated!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> User info has not been updated!
                              </div>");
        }
    }


    //for user delete
    public function userIsDelete($userId){

        $isDelete = 0;
        $dataArray = array($isDelete);

        $sql = "UPDATE `tbl_user_info` SET `user_status`=? WHERE `user_id`='$userId'";

        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            /*Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");*/
        }else{
            /*Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");*/
        }

    }

}