<?php

namespace App\Deposit;

use PDO,PDOException;
use App\Model\Database;

class Deposit extends Database
{
    public $depId;
    public $depTypeId;
    public $depTnxId;
    public $depUserId;
    public $depDoctorId;
    public $depAppointId;
    public $depChamberId;
    public $depMethodId;
    public $depAmount;


    public function setData($rcv){

        if (array_key_exists('deposit_id',$rcv)){
            $this->depId =  $rcv ['deposit_id'];
        }
        if (array_key_exists('deposit_type_id',$rcv)){
            $this->depTypeId =  $rcv ['deposit_type_id'];
        }
        if (array_key_exists('deposit_tnx_id',$rcv)){
            $this->depTnxId =  $rcv ['deposit_tnx_id'];
        }
        if (array_key_exists('deposit_user_id',$rcv)){
            $this->depUserId =  $rcv ['deposit_user_id'];
        }
        if (array_key_exists('deposit_doctor_id',$rcv)){
            $this->depDoctorId =  $rcv ['deposit_doctor_id'];
        }
        if (array_key_exists('deposit_appoint_id',$rcv)){
            $this->depAppointId =  $rcv ['deposit_appoint_id'];
        }
        if (array_key_exists('deposit_chamber_id',$rcv)){
            $this->depChamberId =  $rcv ['deposit_chamber_id'];
        }
        if (array_key_exists('dep_method_id',$rcv)){
            $this->depMethodId =  $rcv ['dep_method_id'];
        }
        if (array_key_exists('dep_amount',$rcv)){
            $this->depAmount =  $rcv ['dep_amount'];
        }
    }

    public function storeDeposit(){

        $depTypeId =  1;
        $depTnxId =  $this->depTnxId;
        $depUserId =  $this->depUserId;
        $depDoctorId =  $this->depDoctorId;
        $depAppointId =  $this->depAppointId;
        $depChamberId =  $this->depChamberId;
        $depMethodId =  $this->depMethodId;
        $depAmount =  $this->depAmount;

        date_default_timezone_set("Asia/Dhaka");
        $depDateTime = date('Y-m-d H:m:s');

        $depStatus = 1;

        $dataArray = array($depTypeId,$depTnxId,$depUserId,$depDoctorId,$depAppointId,$depChamberId,$depMethodId,$depAmount,$depDateTime,$depStatus);

        $sql = "INSERT INTO `tbl_deposit_history` (`deposit_type_id_ref`,`deposit_transaction_id`,`deposit_user_id_ref`,`deposit_doctor_id_ref`, `deposit_appoint_id_ref`,
`deposit_chamber_id_ref`,`deposit_method_id_ref`, `deposit_amount`, `deposit_insertdate`, `deposit_status`) VALUES (?,?,?,?,?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);
        $sth->execute($dataArray);

    }


    //single doctor deposit history
    public function allDepositHistorySingleDoctor($doctorId){

        $sqlQuery = "select * 
                    from `tbl_deposit_history` 
                    
                    INNER JOIN 
                        tbl_user_info ON `tbl_deposit_history`.deposit_user_id_ref=`tbl_user_info`.user_id  
                    INNER JOIN 
                        tbl_doctor_chamber ON tbl_deposit_history.deposit_chamber_id_ref = tbl_doctor_chamber.doctor_chamber_id
                    
                    WHERE deposit_doctor_id_ref = '$doctorId' AND deposit_status = 1";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //all deposit history
    public function allDepositHistory(){

        $sqlQuery = "select * 
                    from `tbl_deposit_history` 
                  
                    INNER JOIN 
                        tbl_user_info ON `tbl_deposit_history`.deposit_user_id_ref=`tbl_user_info`.user_id  
                    INNER JOIN 
                        tbl_doctor_chamber ON tbl_deposit_history.deposit_chamber_id_ref = tbl_doctor_chamber.doctor_chamber_id
                    INNER JOIN 
                        tbl_doctor_info ON tbl_deposit_history.deposit_doctor_id_ref = tbl_doctor_info.doctor_info_id
                    
                    WHERE deposit_status = 1";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

}