<?php
namespace App\Doctor;

use PDO,PDOException;
use App\Model\Database;
use App\Message\Message;

class Doctor extends Database
{

    public $doctorId;
    public $doctorUniqueId;
    public $doctorRegId;
    public $doctorImage;
    public $doctorName;
    public $doctorPass;
    public $doctorEmail;
    public $doctorDegree;
    public $doctorDetails;
    public $doctorPhone;
    public $doctorPresentationLink;
    public $doctorTypeRef;
    public $doctorActiveStatus;

    public function setDoctorData($rcv){

        if (array_key_exists('doctor_id',$rcv)){
            $this->doctorId =  $rcv ['doctor_id'];
        }
        if (array_key_exists('doctor_unique_id',$rcv)){
            $this->doctorUniqueId =  $rcv ['doctor_unique_id'];
        }
        if (array_key_exists('doctor_reg_id',$rcv)){
            $this->doctorRegId=  $rcv ['doctor_reg_id'];
        }
        if (array_key_exists('doctor_image_info',$rcv)){
            $this->doctorImage =  $rcv ['doctor_image_info'];
        }
        if (array_key_exists('doctor_name',$rcv)){
            $this->doctorName =  $rcv ['doctor_name'];
        }
        if (array_key_exists('doctor_pass',$rcv)){
            $this->doctorPass =  $rcv ['doctor_pass'];
        }
        if (array_key_exists('doctor_email',$rcv)){
            $this->doctorEmail =  $rcv ['doctor_email'];
        }
        if (array_key_exists('doctor_degree',$rcv)){
            $this->doctorDegree =  $rcv ['doctor_degree'];
        }
        if (array_key_exists('doctor_details',$rcv)){
            $this->doctorDetails =  $rcv ['doctor_details'];
        }
        if (array_key_exists('doctor_phone',$rcv)){
            $this->doctorPhone =  $rcv ['doctor_phone'];
        }
        if (array_key_exists('doctor_presentation_link',$rcv)){
            $this->doctorPresentationLink =  $rcv ['doctor_presentation_link'];
        }
        if (array_key_exists('doctor_type_ref',$rcv)){
            $this->doctorTypeRef =  $rcv ['doctor_type_ref'];
        }
        if (array_key_exists('doctor_status',$rcv)){
            $this->doctorActiveStatus =  $rcv ['doctor_status'];
        }
    }

    public function storeDoctor(){

        $doctorUniqueId =  $this->doctorUniqueId;
        $doctorRegId =  $this->doctorRegId;
        $doctorName =  $this->doctorName;
        $doctorPass =  md5($this->doctorPass);
        $doctorEmail =  $this->doctorEmail;
        $doctorImage =  $this->doctorImage;
        $doctorTypeRef =  $this->doctorTypeRef;
        $doctorDegree =  $this->doctorDegree;
        $doctorDetails =  $this->doctorDetails;
        $doctorPhone =  $this->doctorPhone;
        $doctorPresentationLink =  $this->doctorPresentationLink;
        $doctorActiveStatus =  $this->doctorActiveStatus;

        $joinDateTime=date("Y-m-d H:i:s A");
        date_default_timezone_set("Asia/Dhaka");

        $admin = 1;

        $dataArray = array($doctorUniqueId,$doctorRegId,$doctorName,$doctorPass,$doctorEmail,$doctorImage,$doctorTypeRef,$doctorDegree,$doctorDetails,
            $doctorPhone,$doctorPresentationLink,$joinDateTime,$admin,$doctorActiveStatus);

        $sql = "INSERT INTO `tbl_doctor_info`(`doctor_info_unique_id`,`doctor_info_reg_id`,`doctor_info_name`,`doctor_info_pass`,`doctor_info_email`,`doctor_info_image`,
`doctor_info_type_ref`,`doctor_info_degree`,`doctor_info_details`,`doctor_info_phone`,`doctor_info_presentation_link`,`doctor_info_reg_datetime`, `doctor_info_admin`, 
`doctor_info_status`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);
        $sth->execute($dataArray);

    }


    public function doctorRegistration(){

        $doctorUniqueId =  $this->doctorUniqueId;
        $doctorRegId =  $this->doctorRegId;
        $doctorName =  $this->doctorName;
        $doctorPass =  md5($this->doctorPass);
        $doctorEmail =  $this->doctorEmail;
        $doctorImage =  $this->doctorImage;
        $doctorTypeRef =  $this->doctorTypeRef;
        $doctorDegree =  $this->doctorDegree;
        $doctorDetails =  $this->doctorDetails;
        $doctorPhone =  $this->doctorPhone;
        $doctorPresentationLink =  $this->doctorPresentationLink;

        $joinDateTime=date("Y-m-d H:i:s A");
        date_default_timezone_set("Asia/Dhaka");

        $dataArray = array($doctorUniqueId,$doctorRegId,$doctorName,$doctorPass,$doctorEmail,$doctorImage,$doctorTypeRef,$doctorDegree,$doctorDetails,
            $doctorPhone,$doctorPresentationLink,$joinDateTime);

        $sql = "INSERT INTO `tbl_doctor_info`(`doctor_info_unique_id`,`doctor_info_reg_id`,`doctor_info_name`,`doctor_info_pass`,`doctor_info_email`,`doctor_info_image`,
`doctor_info_type_ref`,`doctor_info_degree`,`doctor_info_details`,`doctor_info_phone`,`doctor_info_presentation_link`,`doctor_info_reg_datetime`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);

        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Sir, your request has been accepted! As soon as we contact with you.
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Doctor info has not been inserted!
                              </div>");
        }
    }

    //for all doctor
    public function allDoctorInfo(){
        $sqlQuery = "select * from tbl_doctor_info order by doctor_info_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //for single doctor info
    public function singleDoctorInfo($doctorId){
        $sqlQuery = "SELECT tbl_doctor_info.`doctor_info_id`,tbl_doctor_info.`doctor_info_reg_id`,tbl_doctor_info.`doctor_info_name`,tbl_doctor_info.`doctor_info_email`,
tbl_doctor_info.`doctor_info_image`,tbl_doctor_info.`doctor_info_degree`,tbl_doctor_info.`doctor_info_details`,tbl_doctor_info.`doctor_info_phone`,
tbl_doctor_info.`doctor_info_presentation_link`,tbl_doctor_info.`doctor_info_reg_datetime`,tbl_doctor_info.`doctor_info_admin`,tbl_doctor_info.`doctor_info_status`,
tbl_doctor_type.doctor_type_name FROM `tbl_doctor_info` INNER JOIN tbl_doctor_type on tbl_doctor_info.doctor_info_type_ref = tbl_doctor_type.doctor_type_id where doctor_info_id='$doctorId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    //user
    //for latest top 3 doctor
    public function latestDoctorJoin(){
        $sqlQuery = "SELECT * FROM `tbl_doctor_info` where doctor_info_status=1 ORDER BY doctor_info_id DESC LIMIT 3";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //for doctor list show by doctor type
    public function doctorListShowByDoctorType($doctorTypeId){
        $sqlQuery = "
                      SELECT * 
                      FROM `tbl_doctor_info` 
                      
                      LEFT JOIN tbl_rating on tbl_doctor_info.doctor_info_id =tbl_rating.rating_doctorid
                      
                      WHERE 
                      tbl_doctor_info.doctor_info_type_ref='$doctorTypeId' 
                      AND doctor_info_status=1
                      
                      ORDER BY tbl_rating.rating_point DESC
                      ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //for all doctor serach with designation
    public function allDoctorInfoWithDesignation(){
        $sqlQuery = "SELECT `doctor_info_id`,CONCAT(`doctor_info_name`,\" (\", `doctor_info_degree`,\" )\") as doctor_info  FROM `tbl_doctor_info` WHERE doctor_info_admin=1 AND doctor_info_status=1 order by doctor_info_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //admin
    //for search single doctor patient list
    public function allSingleDoctorPatientList($doctorId,$chamberId,$appointDate){
        $sqlQuery = "
                      SELECT * 
                      FROM `tbl_doctor_appointment` 

                      INNER JOIN tbl_user_info on tbl_doctor_appointment.`appoint_user_id_ref`=tbl_user_info.user_id 
                      
                      WHERE 
                      tbl_doctor_appointment.`appoint_doctor_id_ref`='$doctorId' AND
                      tbl_doctor_appointment.`appoint_chamber_id_ref` = '$chamberId' AND
                      tbl_doctor_appointment.`appoint_insertdate` = '$appointDate'
                    ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }


    public function allSingleDoctorPatientCount($doctorId,$appointDate){
        $sqlQuery = "
                      SELECT * 
                      FROM `tbl_doctor_appointment` 

                      INNER JOIN tbl_user_info on tbl_doctor_appointment.`appoint_user_id_ref`=tbl_user_info.user_id 
                      
                      WHERE 
                      tbl_doctor_appointment.`appoint_doctor_id_ref`='$doctorId' AND
                      tbl_doctor_appointment.`appoint_insertdate` = '$appointDate'
                    ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //for all patient list count
    public function allDoctorPatientList($appointDate){
        $sqlQuery = "
                      SELECT * 
                      FROM `tbl_doctor_appointment` 

                      INNER JOIN tbl_user_info on tbl_doctor_appointment.`appoint_user_id_ref`=tbl_user_info.user_id 
                      
                      WHERE 
                      tbl_doctor_appointment.`appoint_isverify`='0' AND
                      tbl_doctor_appointment.`appoint_isDeleted` = 0 AND
                      tbl_doctor_appointment.`appoint_insertdate` = '$appointDate'
                    ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //for single doctor info
    public function singleDoctorDetails($doctorId){
        $sqlQuery = "SELECT * FROM `tbl_doctor_info` where doctor_info_id='$doctorId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    /*for doctor info update*/
    public function singleDoctorInfoUpdate(){

        $doctorRegId =  $this->doctorRegId;
        $doctorName =  $this->doctorName;
        $doctorPass =  md5($this->doctorPass);
        $doctorEmail =  $this->doctorEmail;
        $doctorTypeRef =  $this->doctorTypeRef;
        $doctorDegree =  $this->doctorDegree;
        $doctorDetails =  $this->doctorDetails;
        $doctorPhone =  $this->doctorPhone;
        $doctorPresentationLink =  $this->doctorPresentationLink;
        $doctorActiveStatus =  $this->doctorActiveStatus;

        $dataArray = array($doctorRegId,$doctorName,$doctorPass,$doctorEmail,$doctorTypeRef,$doctorDegree,$doctorDetails,
            $doctorPhone,$doctorPresentationLink,$doctorActiveStatus);

        $sql = "UPDATE `tbl_doctor_info` SET `doctor_info_reg_id`=?,`doctor_info_name`=?,`doctor_info_pass`=?,
`doctor_info_email`=?,`doctor_info_type_ref`=?,`doctor_info_degree`=?,`doctor_info_details`=?,
`doctor_info_phone`=?,`doctor_info_presentation_link`=?,`doctor_info_status`=? WHERE `doctor_info_id`='$this->doctorId'";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Doctor info has been updated!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Doctor info has not been updated!
                              </div>");
        }
    }


    /*for doctor accept*/
    public function updateDoctorAccept(){

        $admin =  1;
        $status =  1;

        $dataArray = array($admin,$status);

        $sql = "UPDATE `tbl_doctor_info` SET `doctor_info_admin`=?,`doctor_info_status`=? WHERE `doctor_info_id`='$this->doctorId'";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Doctor has been accepted!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Doctor has not been accepted!
                              </div>");
        }
    }


    //for single doctor info
    public function singleDoctorAllInfo($doctorId,$chamberId){
        $sqlQuery = "SELECT * FROM `tbl_doctor_info` 

                    INNER JOIN tbl_doctor_type 
                    on tbl_doctor_info.doctor_info_type_ref = tbl_doctor_type.doctor_type_id 
                    
                    INNER JOIN tbl_doctor_chamber 
                    on tbl_doctor_info.doctor_info_id = tbl_doctor_chamber.doctor_chamber_doctor_id_ref
                    
                    where doctor_info_id='$doctorId' AND doctor_chamber_id='$chamberId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

}


























