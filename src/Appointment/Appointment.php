<?php
namespace App\Appointment;

use App\Model\Database;
use PDO,PDOException;
use App\Message\Message;

class Appointment extends Database
{

    //for find serial number
    public function lastSerialNumber($doctorId,$doctorChamberId){

        $currentDate=date("Y-m-d");
        date_default_timezone_set("Asia/Dhaka");

        $sqlQuery = "select appoint_serial_num from tbl_doctor_appointment 
where appoint_doctor_id_ref='$doctorId' AND appoint_chamber_id_ref='$doctorChamberId' AND appoint_insertdate='$currentDate' order by appoint_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    //for store appointment
    public function storeAppointment($userId,$doctorId,$chamberId,$methodId,$invoiceId,$serialNum,$trnxId){

        $insertDate=date("Y-m-d");
        date_default_timezone_set("Asia/Dhaka");


        $dataArray = array($userId,$doctorId,$chamberId,$methodId,$invoiceId,$serialNum,$trnxId,
            $insertDate);

        $sql = "INSERT INTO `tbl_doctor_appointment`(`appoint_user_id_ref`,`appoint_doctor_id_ref`,`appoint_chamber_id_ref`,`appoint_payment_method_id_ref`,
`appoint_invoice_id`,`appoint_serial_num`,`appoint_transaction_id`,`appoint_insertdate`) VALUES (?,?,?,?,?,?,?,?);";

        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            /*Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");*/
        }else{
            /*Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");*/
        }

    }

    //for single appoint details
    public function singleAppointDetails($appointId){
        $sqlQuery = "
                    SELECT * 
                    
                    FROM `tbl_doctor_appointment` 
                    
                    INNER JOIN 
                        tbl_user_info ON `tbl_doctor_appointment`.appoint_user_id_ref=`tbl_user_info`.user_id 
                    INNER JOIN
                        tbl_doctor_info ON tbl_doctor_appointment.appoint_doctor_id_ref =tbl_doctor_info.doctor_info_id
                    INNER JOIN 
                        tbl_doctor_chamber ON tbl_doctor_appointment.appoint_chamber_id_ref =tbl_doctor_chamber.doctor_chamber_id
                    
                    where appoint_id='$appointId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    //for confirm appoint
    public function updateSingleUserAppointConfirm($appointId,$trnxId){

        $isVerify = 1;
        $dataArray = array($trnxId,$isVerify);

        $sql = "UPDATE `tbl_doctor_appointment` SET `appoint_transaction_id`=?,`appoint_isverify`=? WHERE `appoint_id`='$appointId'";

        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            /*Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");*/
        }else{
            /*Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");*/
        }

    }


    //for appoint is deleted
    public function appointIsDelete($appointId){

        $isDeleted = 1;
        $dataArray = array($isDeleted);

        $sql = "UPDATE `tbl_doctor_appointment` SET `appoint_isDeleted`=? WHERE `appoint_id`='$appointId'";

        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            /*Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");*/
        }else{
            /*Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");*/
        }

    }

}