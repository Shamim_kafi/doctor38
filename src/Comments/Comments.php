<?php
namespace App\Comments;


use App\Model\Database;
use PDO,PDOException;
use App\Message\Message;

class Comments extends Database
{

    public function storeComments($userId,$doctorId,$comments){

        $comUserId = $userId;
        $comDoctorId = $doctorId;
        $comText = $comments;

        $comDate=date("Y/m/d h:i:s a");
        date_default_timezone_set("Asia/Dhaka");

        $dataArray = array($comUserId,$comDoctorId,$comText,$comDate);

        $sql = "INSERT INTO `tbl_comments` (`comm_user_id`,`comm_doctor_id`,`comm_comments`,`comm_datetime`) VALUES (?,?,?,?);";
        $sth = $this->db->prepare($sql);
        $sth->execute($dataArray);
    }


    //all comments history
    public function allCommentsHistory(){

        $sqlQuery = "select * 
                    from `tbl_comments` 
                  
                    LEFT JOIN 
                        tbl_user_info ON tbl_comments.`comm_user_id`=`tbl_user_info`.user_id  
            
                    LEFT JOIN 
                        tbl_doctor_info ON tbl_comments.`comm_doctor_id` = tbl_doctor_info.doctor_info_id
                    
                   ORDER BY comm_id DESC";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //for comments is deleted
    public function commentsIsDelete($commentsId){

        $isDeleted = 1;
        $dataArray = array($isDeleted);

        $sql = "UPDATE `tbl_comments` SET `comm_isDeleted`=? WHERE `comm_id`='$commentsId'";

        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            /*Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Chamber has been inserted!
                              </div>");*/
        }else{
            /*Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Chamber has not been inserted!
                              </div>");*/
        }

    }


    //for single Doctor Latest Five Comments Show
    public function singleDoctorLatestFiveCommentsShow($doctorId){

        $sqlQuery = "select * 
                    from `tbl_comments` 
                  
                    LEFT JOIN 
                        tbl_user_info ON tbl_comments.`comm_user_id`=`tbl_user_info`.user_id  
            
                    LEFT JOIN 
                        tbl_doctor_info ON tbl_comments.`comm_doctor_id` = tbl_doctor_info.doctor_info_id
                        
                    WHERE tbl_comments.`comm_doctor_id`='$doctorId' AND tbl_comments.`comm_isDeleted`=0
                    
                    ORDER BY comm_id DESC LIMIT 3";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

}