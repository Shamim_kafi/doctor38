<?php
namespace App\Assistant;

use App\Model\Database;
use PDO,PDOException;
use App\Message\Message;

class Assistant extends Database
{

    public $doctorAssistantId;
    public $doctorIdRef;
    public $doctorChamberIdRef;
    public $doctorAssistantName;
    public $doctorAssistantPhone;
    public $doctorAssistantPass;
    public $doctorAssistantIsActive;

    public function setDoctorAssistantData($rcv){

        if (array_key_exists('assistant_id',$rcv)){
            $this->doctorAssistantId =  $rcv ['assistant_id'];
        }
        if (array_key_exists('doctor_id',$rcv)){
            $this->doctorIdRef =  $rcv ['doctor_id'];
        }
        if (array_key_exists('chamber_id',$rcv)){
            $this->doctorChamberIdRef=  $rcv ['chamber_id'];
        }
        if (array_key_exists('assistant_name',$rcv)){
            $this->doctorAssistantName =  $rcv ['assistant_name'];
        }
        if (array_key_exists('assistant_phone',$rcv)){
            $this->doctorAssistantPhone =  $rcv ['assistant_phone'];
        }
        if (array_key_exists('assistant_pass',$rcv)){
            $this->doctorAssistantPass =  $rcv ['assistant_pass'];
        }
        if (array_key_exists('is_active',$rcv)){
            $this->doctorAssistantIsActive =  $rcv ['is_active'];
        }
    }

    public function storeDoctorAssistant(){

        $doctorIdRef =  $this->doctorIdRef;
        $doctorChamberIdRef =  $this->doctorChamberIdRef;
        $doctorAssistantName =  $this->doctorAssistantName;
        $doctorAssistantPhone =  $this->doctorAssistantPhone;
        $doctorAssistantPass =  md5($this->doctorAssistantPass);

        $insertDateTime=date("Y-m-d H:i:s A");
        date_default_timezone_set("Asia/Dhaka");


        $dataArray = array($doctorIdRef,$doctorChamberIdRef,$doctorAssistantName,$doctorAssistantPhone,$doctorAssistantPass,$insertDateTime);

        $sql = "INSERT INTO `tbl_doctor_assistant`(`assistant_doctor_id_ref`,`assistant_chamber_id_ref`,`assistant_name`,`assistant_phone`,`assistant_password`,
`assistant_insert_date`) VALUES (?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> New Assistant has been inserted!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Assistant has not been inserted!
                              </div>");
        }

    }


    //all single assistant data
    public function allSingleDoctorAssistant($doctorId){

        $sqlQuery = "
                  select * 
                    from `tbl_doctor_assistant` 
                    
                  INNER JOIN 
                    tbl_doctor_chamber ON tbl_doctor_assistant.assistant_chamber_id_ref = tbl_doctor_chamber.doctor_chamber_id
                  
                  WHERE 
                    `tbl_doctor_assistant`.assistant_doctor_id_ref ='$doctorId'
                  ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    /*for doctor assistant profile update*/
    public function doctorAssistantProfileUpdate(){

        $assistantName =  $this->doctorAssistantName;
        $assistantPhone =  $this->doctorAssistantPhone;
        $assistantPass =  md5($this->doctorAssistantPass);
        $assistantIsActive =  $this->doctorAssistantIsActive;


        $dataArray = array($assistantName,$assistantPhone,$assistantPass,$assistantIsActive);

        $sql = "UPDATE `tbl_doctor_assistant` SET `assistant_name`=?,`assistant_phone`=?,`assistant_password`=?,`assistant_isActive`=?
                WHERE `assistant_id`='$this->doctorAssistantId'";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Assistant info has been updated!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Assistant info has not been updated!
                              </div>");
        }
    }


    //for single assistant info
    public function singleAssistantDetails($assistantId){
        $sqlQuery = "SELECT * FROM `tbl_doctor_assistant` where assistant_id='$assistantId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }


    /*for assistant self profile info update*/
    public function assistantSelfProfileUpdate(){

        $assistantName =  $this->doctorAssistantName;
        $assistantPhone =  $this->doctorAssistantPhone;
        $assistantPass =  md5($this->doctorAssistantPass);


        $dataArray = array($assistantName,$assistantPhone,$assistantPass);

        $sql = "UPDATE `tbl_doctor_assistant` SET `assistant_name`=?,`assistant_phone`=?,`assistant_password`=?
                WHERE `assistant_id`='$this->doctorAssistantId'";
        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Assistant info has been updated!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Assistant info has not been updated!
                              </div>");
        }
    }

}