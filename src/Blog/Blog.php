<?php
namespace App\Blog;

use PDO,PDOException;
use App\Model\Database;


class Blog extends Database
{

    public $blogId;
    public $blogTitle;
    public $blogDetails;
    public $blogImage;
    public $adminIdRef;
    public $doctorIdRef;


    public function setData($rcv){

        if (array_key_exists('blog_id',$rcv)){
            $this->blogId =  $rcv ['blog_id'];
        }
        if (array_key_exists('blog_title',$rcv)){
            $this->blogTitle =  $rcv ['blog_title'];
        }
        if (array_key_exists('blog_details',$rcv)){
            $this->blogDetails =  $rcv ['blog_details'];
        }
        if (array_key_exists('blog_image',$rcv)){
            $this->blogImage =  $rcv ['blog_image'];
        }
        if (array_key_exists('admin_id',$rcv)){
            $this->adminIdRef =  $rcv ['admin_id'];
        }
        if (array_key_exists('doctor_id',$rcv)){
            $this->doctorIdRef =  $rcv ['doctor_id'];
        }
    }

    public function storeBlog(){

        $blogTitle =  $this->blogTitle;
        $blogDetails =  $this->blogDetails;
        $blogImage =  $this->blogImage;
        $adminIdRef =  $this->adminIdRef;
        $doctorIdRef =  $this->doctorIdRef;

        date_default_timezone_set("Asia/Dhaka");
        $blogDateTime = date('Y-m-d H:m:s');

        $isVerify = 1;

        $dataArray = array($blogTitle,$blogDetails,$blogImage,$adminIdRef,$doctorIdRef,$blogDateTime,$isVerify);

        $sql = "INSERT INTO `tbl_blog` (`blog_title`,`blog_details`,`blog_image`,`blog_admin_ref`, `blog_doctor_ref`,
`blog_post_datetime`,`blog_is_verify`) VALUES (?,?,?,?,?,?,?);";
        $sth = $this->db->prepare($sql);
        $sth->execute($dataArray);

    }


    //all blog list
    public function allSingleDoctorBlogList($doctorId){

        $sqlQuery = "
                  select * 
                    from `tbl_blog` 
                    
                    INNER JOIN 
                        tbl_doctor_info ON tbl_blog.blog_doctor_ref = tbl_doctor_info.doctor_info_id
                    WHERE blog_doctor_ref='$doctorId'
                  ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }


    //all blog list
    public function allBlogList(){

        $sqlQuery = "
                  select * 
                    from `tbl_blog` 
                    
                    LEFT JOIN 
                        tbl_admin_info ON tbl_blog.blog_admin_ref = tbl_admin_info.a_id
                    LEFT JOIN 
                        tbl_doctor_info ON tbl_blog.blog_doctor_ref = tbl_doctor_info.doctor_info_id
                  ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //latest blog list show
    public function latestBlogList(){

        $sqlQuery = "
                  select * 
                    from `tbl_blog` 
                    
                    LEFT JOIN 
                        tbl_admin_info ON tbl_blog.blog_admin_ref = tbl_admin_info.a_id
                    LEFT JOIN 
                        tbl_doctor_info ON tbl_blog.blog_doctor_ref = tbl_doctor_info.doctor_info_id
                    ORDER BY blog_id DESC LIMIT 3
                  ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }


    //all blog details
    public function singleBlogDetails($blogId){

        $sqlQuery = "
                  select * 
                    from `tbl_blog` 
                    
                    LEFT JOIN 
                        tbl_admin_info ON tbl_blog.blog_admin_ref = tbl_admin_info.a_id
                    LEFT JOIN 
                        tbl_doctor_info ON tbl_blog.blog_doctor_ref = tbl_doctor_info.doctor_info_id
                    WHERE blog_id='$blogId';
                  ";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }


}