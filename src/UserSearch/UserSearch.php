<?php
namespace App\UserSearch;

use PDO,PDOException;
use App\Model\Database;

class UserSearch extends Database
{

    //for doctor list show by doctor type
    public function userSearch($userAddress,$userBloodGroup){
        $sqlQuery = "SELECT * FROM `tbl_user_info` WHERE user_address='$userAddress' AND `user_blood_group`='$userBloodGroup'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

    //for doctor list show by doctor type
    public function userSearchByGeoLocation($geoLat,$geoLong,$userBloodGroup){



        $sqlQuery = "
                   SELECT 
                        *,
                        111.045* 
                        DEGREES(ACOS(COS(RADIANS($geoLat)) * 
                        COS(RADIANS(tbl_doctor_chamber.`doctor_chamber_loc_lat`)) * 
                        COS(RADIANS($geoLong) - RADIANS(tbl_user_info.`doctor_chamber_loc_long`)) + 
                        SIN(RADIANS($geoLat)) 
                        * SIN(RADIANS(tbl_user_info.`doctor_chamber_loc_lat`))))
                        AS distance_in_km 
                        
                   FROM `tbl_user_info` 
         
                   WHERE 
                     `tbl_doctor_info`.`user_blood_group`='$userBloodGroup'
                     ";

        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData = $sth->fetchAll();
        return $allData;
    }

}