<?php
namespace App\Admin;

use PDO,PDOException;
use App\Model\Database;
use App\Message\Message;

class Admin extends Database
{

    public $adminId;
    public $adminName;
    public $adminPass;
    public $adminEmail;
    public $adminPhone;

    public function setAdminData($rcv){

        if (array_key_exists('admin_id',$rcv)){
            $this->adminId =  $rcv ['admin_id'];
        }
        if (array_key_exists('admin_name',$rcv)){
            $this->adminName =  $rcv ['admin_name'];
        }
        if (array_key_exists('admin_pass',$rcv)){
            $this->adminPass=  $rcv ['admin_pass'];
        }
        if (array_key_exists('admin_email',$rcv)){
            $this->adminEmail =  $rcv ['admin_email'];
        }
        if (array_key_exists('admin_phone',$rcv)){
            $this->adminPhone =  $rcv ['admin_phone'];
        }
    }

    //for single admin info
    public function singleAdminInfo($adminId){
        $sqlQuery = "SELECT * FROM `tbl_admin_info` where a_id='$adminId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }


    /*for doctor info update*/
    public function singleAdminInfoUpdate(){

        $adminName =  $this->adminName;
        $adminPhone =  $this->adminPhone;
        $adminEmail =  $this->adminEmail;
        $adminPass =  $this->adminPass;

        $dataArray = array($adminName,$adminPhone,$adminEmail,$adminPass);

        $sql = "UPDATE `tbl_admin_info` SET `a_name`=?,`a_contact`=?,`a_email`=?,
`a_password`=? WHERE `a_id`='$this->adminId'";

        $sth = $this->db->prepare($sql);
        if ($sth->execute($dataArray)){
            Message::message("<div class='alert alert-success' role='alert'>
                                 <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                 <span aria-hidden='true'>&times;</span></button>
                                 <strong>Successfully!</strong> Admin info has been updated!
                              </div>");
        }else{
            Message::message("<div class='alert alert-danger' role='alert'>
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                  <span aria-hidden='true'>&times;</span></button>
                                  <strong>Sorry!</strong> Admin info has not been updated!
                              </div>");
        }
    }

}