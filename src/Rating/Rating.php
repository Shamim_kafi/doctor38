<?php
namespace App\Rating;

use PDO,PDOException;
use App\Model\Database;
use App\Message\Message;

class Rating extends Database
{

    //for single rating point
    public function singleDoctorRatingInfo($doctorId){
        $sqlQuery = "select * from tbl_rating where rating_doctorid='$doctorId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }

    //for single doctor average
    public function singleDoctorAverageInfo($doctorId){
        $sqlQuery = "SELECT ROUND(AVG(rating_point),1) as averageRating from tbl_rating where rating_doctorid='$doctorId'";
        $sth = $this->db->query($sqlQuery);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $singleData = $sth->fetch();
        return $singleData;
    }
}