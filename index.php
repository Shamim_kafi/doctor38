<?php
session_start();
require_once ("vendor/autoload.php");

use App\DoctorType\DoctorType;
use App\Doctor\Doctor;
use App\Blog\Blog;

//for get doctor type list
$objDoctorType = new DoctorType();
$allDoctorType = $objDoctorType->allDoctorTypeInfo();

//for latest 3 doctors join
$objDoctor = new Doctor();
$latestDoctorJoinList = $objDoctor->latestDoctorJoin();

$objBlog = new Blog();
$allBlogs = $objBlog->latestBlogList();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Doctor38| Home</title>

    <!-- Font awesome -->
    <link href="resources/awesome/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="resources/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/user/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="resources/css/user/nouislider.css">
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="resources/css/user/jquery.fancybox.css" type="text/css" media="screen" />
    <!-- Theme color -->
    <link id="switcher" href="resources/css/user/theme-color/default-theme.css" rel="stylesheet">

    <!-- Main style sheet -->
    <link href="resources/css/user/style.css" rel="stylesheet">


    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href='http://eoffice.codeslab.net/assets/plugin/select2/select2.min.css' rel='stylesheet' media='screen'>

    <!--Select2 use-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />


</head>
<body class="aa-price-range">
<!-- Pre Loader -->
<div id="aa-preloader-area">
    <div class="pulse"></div>
</div>
<!-- SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
<!-- END SCROLL TOP BUTTON -->

<!-- Start header section -->
<header id="aa-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-header-area">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="aa-header-left">
                                <div class="aa-telephone-no">
                                    <span class="fa fa-phone"></span>
                                    880-01837210137
                                </div>
                                <div class="aa-email hidden-xs">
                                    <span class="fa fa-envelope-o"></span> doctor38@gmail.com
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="aa-header-right">
                                <?php if (!isset($_SESSION['u_id'])) {
                                    echo "
                                        <a href=\"view/doctor/signup/signin.php\" class=\"aa-register\">Doctor</a>
                                        <a href=\"view/user/signup/signin.php\" class=\"aa-login\">User</a>
                                    ";
                                } else{
                                    echo "
                                        <a style='color: red' href='view/user/logout/logout.php' class=\"aa-login\">Logout</a>
                                    ";
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End header section -->

<!-- Start menu section -->
<section id="aa-menu-area">
    <nav class="navbar navbar-default main-navbar" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- LOGO -->
                <!-- Text based logo -->
                <a class="navbar-brand aa-logo" href="index.php"> Doctor <span>38</span></a>
                <!-- Image based logo -->
                <!-- <a class="navbar-brand aa-logo-img" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul id="top-menu" class="nav navbar-nav navbar-right aa-main-nav">
                    <li class="active"><a href="index.php">HOME</a></li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">DOCTORS <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">

                            <?php
                            foreach ($allDoctorType as $doctorType){
                                echo "
                                     <li><a href='view/user/doctor_profile_view.php?doctor_type_id=$doctorType->doctor_type_id'>$doctorType->doctor_type_name</a></li>
                                    ";
                            }
                            ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">BLOG <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="view/user/blog/all_blog_show.php">ARTICLES</a></li>
                            <li><a href="view/user/blog/all_blog_show.php">TIPS</a></li>
                        </ul>
                    </li>
                    <li><a href="#">CONTACT</a></li>
                    <?php if (isset($_SESSION['u_id'])) {
                        echo "
                             <li><a href='view/user/profile/profile.php'>PROFILE</a></li>
                             ";
                    }?>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</section>
<!-- End menu section -->

<!-- Start slider  -->
<section id="aa-slider">
    <div class="aa-slider-area">
        <!-- Top slider -->
        <div class="aa-top-slider">
            <!-- Top slider single slide -->
            <div class="aa-top-slider-single">
                <img src="resources/images/user/slider/slide1.jpeg" alt="img">
                <!-- Top slider content -->
                <div class="aa-top-slider-content">
                    <h2 class="aa-top-slider-title">We Care for you</h2>
                    <p class="aa-top-slider-location">We place our patients and their needs in the center of all we do.Our strives to</p>
                    <p class="aa-top-slider-location">always remain the most trusted partner in tomorrow’s healthcare industry. Our</p>
                    <p class="aa-top-slider-location">dedication to Quality, Customer Service, Innovation, Advanced Technology and</p>
                    <p class="aa-top-slider-location">Research is what separates us from our competition.</p>
                    <a href="#" class="aa-top-slider-btn">Read More <span class="fa fa-angle-double-right"></span></a>
                </div>
                <!-- / Top slider content -->
            </div>
            <!-- / Top slider single slide -->

            <!-- Top slider single slide -->
            <div class="aa-top-slider-single">
                <img src="resources/images/user/slider/slide1.jpeg" alt="img">
                <!-- Top slider content -->
                <div class="aa-top-slider-content">
                    <h2 class="aa-top-slider-title">We Care for you</h2>
                    <p class="aa-top-slider-location">We place our patients and their needs in the center of all we do.Our strives to</p>
                    <p class="aa-top-slider-location">always remain the most trusted partner in tomorrow’s healthcare industry. Our</p>
                    <p class="aa-top-slider-location">dedication to Quality, Customer Service, Innovation, Advanced Technology and</p>
                    <p class="aa-top-slider-location">Research is what separates us from our competition.</p>
                    <a href="#" class="aa-top-slider-btn">Read More <span class="fa fa-angle-double-right"></span></a>
                </div>
                <!-- / Top slider content -->
            </div>
            <!-- / Top slider single slide -->

            <!-- Top slider single slide -->
            <div class="aa-top-slider-single">
                <img src="resources/images/user/slider/slide1.jpeg" alt="img">
                <!-- Top slider content -->
                <div class="aa-top-slider-content">
                    <h2 class="aa-top-slider-title">We Care for you</h2>
                    <p class="aa-top-slider-location">We place our patients and their needs in the center of all we do.Our strives to</p>
                    <p class="aa-top-slider-location">always remain the most trusted partner in tomorrow’s healthcare industry. Our</p>
                    <p class="aa-top-slider-location">dedication to Quality, Customer Service, Innovation, Advanced Technology and</p>
                    <p class="aa-top-slider-location">Research is what separates us from our competition.</p>
                    <a href="#" class="aa-top-slider-btn">Read More <span class="fa fa-angle-double-right"></span></a>
                </div>
                <!-- / Top slider content -->
            </div>
            <!-- / Top slider single slide -->
        </div>
    </div>
</section>
<!-- End slider  -->

<!-- Advance Search -->
<section id="aa-advance-search">
    <div class="container">
        <div class="aa-advance-search-area">
            <div class="form">
                <form action="view/user/search/doctor_search.php" method="post" autocomplete="off">
                    <div class="aa-advance-search-bottom">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="aa-single-filter-search">
                                    <span>AREA (MILES)</span>
                                    <span>FROM</span>
                                    <span id="skip-value-lower" class="example-val">30.00</span>
                                    <span>TO</span>
                                    <span id="skip-value-upper" class="example-val">100.00</span>
                                    <div id="aa-sqrfeet-range" class="noUi-target noUi-ltr noUi-horizontal noUi-background">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="aa-advance-search-top">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="aa-single-advance-search">
                                    <input onclick="getLocation()" type="text" id="geoLocation" name="location_geo" value="" placeholder="Your Location">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="aa-single-advance-search">
                                    <select name="location_head" required>
                                        <option value="0" selected>Select Location</option>
                                        <option value="GEC">GEC</option>
                                        <option value="Agrabad">Agrabad</option>
                                        <option value="Chawkbazar">Chawkbazar</option>
                                        <option value="Provortok">Provortok</option>
                                        <option value="2 Number Gate">2 Number Gate</option>
                                        <option value="Muradpur">Muradpur</option>
                                        <option value="Andorkilla">Andorkilla</option>
                                        <option value="Medical Gate">Medical Gate</option>
                                        <option value="Kumira">Kumira</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="aa-single-advance-search">
                                    <select name="category" id="category">
                                        <option value="0" selected disabled>Category</option>
                                        <option value="1">Doctor</option>
                                        <option value="2">Blood Donor</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="aa-single-advance-search">
                                    <select class="form-control" name="blood_group" id="blood_group" required>
                                        <option value="0" selected>Select Blood Group</option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="aa-single-advance-search">
                                    <select class="form-control" name="doctor_type" id="doctor_type">
                                        <option value="0" selected disabled>Select Specialities</option>
                                        <?php
                                        foreach ($allDoctorType as $doctorType){
                                            echo "
                                     <option value='$doctorType->doctor_type_id'>$doctorType->doctor_type_name</option>
                                    ";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="aa-single-advance-search">
                                    <input class="aa-search-btn" type="submit" value="Search">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- / Advance Search -->


<!-- Latest property -->
<section id="aa-latest-property">
    <div class="container">
        <div class="aa-latest-property-area">
            <div class="aa-title">
                <h2>Latest Join Doctors</h2>
                <span></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum sit ea nobis quae vero voluptatibus.</p>
            </div>
            <div class="aa-latest-properties-content">
                <div class="row">
                    <?php
                    foreach ($latestDoctorJoinList as $latestDoctor){

                        $details = substr("$latestDoctor->doctor_info_details",0,150)." ....";
                        echo "
                        
                        
                        <div class=\"col-md-4\">
                        <article class=\"aa-properties-item\">
                            <a href=\"#\" class=\"aa-properties-item-img\">
                                <img src='resources/images/doctor/$latestDoctor->doctor_info_image' alt=\"img\">
                            </a>
                            <div class=\"aa-tag for-rent\">
                                Verified
                            </div>
                            <div class=\"aa-properties-item-content\">
                                <div class=\"aa-properties-info\">
                                    <span class='fa fa-info-circle'></span>
                                    <span>$latestDoctor->doctor_info_degree</span>
                                </div>
                                <div class=\"aa-properties-about\">
                                    <h3><a href=\"#\">Dr. $latestDoctor->doctor_info_name</a></h3>
                                    <p>$details</p>
                                </div>
                                <div class=\"aa-properties-detial\">
                                    <a href='view/user/doctor_profile_details.php?doctor_id=$latestDoctor->doctor_info_id' class=\"aa-secondary-btn\">View Details</a>
                                </div>
                            </div>
                        </article>
                    </div>
                        
                        ";
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Latest property -->

<!-- Service section -->
<section id="aa-service">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-service-area">
                    <div class="aa-title">
                        <h2>Our Service</h2>
                        <span></span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum sit ea nobis quae vero voluptatibus.</p>
                    </div>
                    <!-- service content -->
                    <div class="aa-service-content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="aa-single-service">
                                    <div class="aa-service-icon">
                                        <span class="fa fa-user"></span>
                                    </div>
                                    <div class="aa-single-service-content">
                                        <h4><a href="#">Doctors</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto repellendus quasi asperiores itaque dolorem at.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="aa-single-service">
                                    <div class="aa-service-icon">
                                        <span class="fa fa-check"></span>
                                    </div>
                                    <div class="aa-single-service-content">
                                        <h4><a href="#">Hospitals</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto repellendus quasi asperiores itaque dolorem at.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="aa-single-service">
                                    <div class="aa-service-icon">
                                        <span class="fa fa-crosshairs"></span>
                                    </div>
                                    <div class="aa-single-service-content">
                                        <h4><a href="#">Clinics</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto repellendus quasi asperiores itaque dolorem at.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="aa-single-service">
                                    <div class="aa-service-icon">
                                        <span class="fa fa-bar-chart-o"></span>
                                    </div>
                                    <div class="aa-single-service-content">
                                        <h4><a href="#">Blood Donors</a></h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto repellendus quasi asperiores itaque dolorem at.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Service section -->


<!-- Client Testimonial -->
<section id="aa-client-testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-client-testimonial-area">
                    <div class="aa-title">
                        <h2>What Client Say</h2>
                        <span></span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus eaque quas debitis animi ipsum, veritatis!</p>
                    </div>
                    <!-- testimonial content -->
                    <div class="aa-testimonial-content">
                        <!-- testimonial slider -->
                        <ul class="aa-testimonial-slider">
                            <li>
                                <div class="aa-testimonial-single">
                                    <div class="aa-testimonial-img">
                                        <img src="resources/images/user/say/user1.png" alt="testimonial img">
                                    </div>
                                    <div class="aa-testimonial-info">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate consequuntur ducimus cumque iure modi nesciunt recusandae eligendi vitae voluptatibus, voluptatum tempore, ipsum nisi perspiciatis. Rerum nesciunt fuga ab natus, dolorem?</p>
                                    </div>
                                    <div class="aa-testimonial-bio">
                                        <p>Rifat Rabbi</p>
                                        <span>Web Designer & Developer</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="aa-testimonial-single">
                                    <div class="aa-testimonial-img">
                                        <img src="resources/images/user/say/user1.png" alt="testimonial img">
                                    </div>
                                    <div class="aa-testimonial-info">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate consequuntur ducimus cumque iure modi nesciunt recusandae eligendi vitae voluptatibus, voluptatum tempore, ipsum nisi perspiciatis. Rerum nesciunt fuga ab natus, dolorem?</p>
                                    </div>
                                    <div class="aa-testimonial-bio">
                                        <p>Tanzim Ahmed</p>
                                        <span>Web Designer & Developer</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="aa-testimonial-single">
                                    <div class="aa-testimonial-img">
                                        <img src="resources/images/user/say/user1.png" alt="testimonial img">
                                    </div>
                                    <div class="aa-testimonial-info">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate consequuntur ducimus cumque iure modi nesciunt recusandae eligendi vitae voluptatibus, voluptatum tempore, ipsum nisi perspiciatis. Rerum nesciunt fuga ab natus, dolorem?</p>
                                    </div>
                                    <div class="aa-testimonial-bio">
                                        <p>Irfan Munna</p>
                                        <span>Web Designer</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Client Testimonial -->

<!-- Latest blog -->
<section id="aa-latest-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-latest-blog-area">
                    <div class="aa-title">
                        <h2>News Articles & Tips</h2>
                        <span></span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe magni, est harum repellendus. Accusantium, nostrum!</p>
                    </div>
                    <div class="aa-latest-blog-content">
                        <div class="row">
                            <!-- start single blog -->
                            <?php
                            foreach ($allBlogs as $allBlog) {
                                if ($allBlog->blog_admin_ref == 1){
                                    $publisher = 'Doctor38';
                                }else{
                                    $publisher = $allBlog->doctor_info_name;
                                }

                                echo "
                                <div class=\"col-md-4\">
                                    <article class=\"aa-blog-single\">
                                        <figure class=\"aa-blog-img\">
                                            <a href=\"#\"><img src=\"resources/images/blog/$allBlog->blog_image\" alt=\"img\"></a>
                                            <span class=\"aa-date-tag\">$allBlog->blog_post_datetime</span>
                                        </figure>
                                        <div class=\"aa-blog-single-content\">
                                            <h3><a href=\"#\">$allBlog->blog_title</a></h3>
                                            <p>$allBlog->blog_details</p>
                                            <div class=\"aa-blog-single-bottom\">
                                                <a href=\"#\" class=\"aa-blog-author\"><i class=\"fa fa-user\"></i> $publisher</a>
                                          
                                            </div>
                                        </div>
    
                                    </article>
                                </div>
                                
                                ";
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- / Latest blog -->

<!-- Footer -->
<footer id="aa-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="aa-footer-area">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="aa-footer-left">
                                <p>Designed by <a rel="nofollow" href="#">Doctor38</a></p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="aa-footer-middle">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="aa-footer-right">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- / Footer -->

<!-- jQuery library -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
<script src="resources/js/user/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="resources/bootstrap/js/bootstrap.min.js"></script>
<!-- slick slider -->
<script type="text/javascript" src="resources/js/user/slick.js"></script>
<!-- Price picker slider -->
<script type="text/javascript" src="resources/js/user/nouislider.js"></script>
<!-- mixit slider -->
<script type="text/javascript" src="resources/js/user/jquery.mixitup.js"></script>
<!-- Add fancyBox -->
<script type="text/javascript" src="resources/js/user/jquery.fancybox.pack.js"></script>
<!-- Custom js -->
<script src="resources/js/user/custom.js"></script>

<!--Select2 use-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src='http://eoffice.codeslab.net/assets/plugin/select2/select2.full.min.js'></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<!--Select2 use end-->

<script>
    var x = document.getElementById("geoLocation");

    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }
        else {
            x.value = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        x.value = position.coords.latitude +"," + position.coords.longitude;
    }
</script>

<script>

    $(document).ready(function () {
        var $categorySelect = $('select[name="category"]');
        var $bloodGroupSelect = $('select[name="blood_group"]');
        var $DoctorTypeSelect = $('select[name="doctor_type"]');

        $categorySelect.change(function () {

            // alert($firstSelect.val())
            if ($categorySelect.val() === "2") {
                //$secondSelect.show();
                $bloodGroupSelect.prop("disabled",false);
                $DoctorTypeSelect.prop("disabled",true);
            }
            else if($categorySelect.val() === "1"){
                //alert($categorySelect.val());
                $DoctorTypeSelect.prop("disabled",false);
                $bloodGroupSelect.prop("disabled",true);
            }

            else {
                //$secondSelect.hide();
                $bloodGroupSelect.prop("disabled",true);
                $DoctorTypeSelect.prop("disabled",true);
            }
        });
    });
</script>

</body>
</html>